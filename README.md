dTHSRShuttlerBus
==================

全新版本的公車時刻表，這次的練習對象是WizardPager和TabPager。
更換了新的資料來源，讓程式擁有更多路線可供查詢。
同時保留了前一個版本可離線使用的功能! 希望大家能夠繼續愛用~

● 瀏覽功能 - 瀏覽過的路線會記錄下來，如要查詢新路線，請連接網路

● 我的最愛 - 可以將常用路線儲存下來。如果有儲存的路線，會自動先打開最愛列表。

資料來源：交通部公路總局 http://goo.gl/s6dKet

高鐵快捷公車全數路線2013年度免費搭乘優惠皆至12月31日止。新聞稿請參閱：http://goo.gl/XOsbU

我的網誌連結：http://goo.gl/XYtqh

#高鐵 #台灣高鐵 #高鐵公車 #高鐵接駁

