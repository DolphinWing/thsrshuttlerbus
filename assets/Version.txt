高鐵接駁公車時刻表 by DolphinWing
https://play.google.com/store/apps/details?id=dolphin.apps.ShuttlerBusTHSR

全新版本的公車時刻表，這次的練習對象是WizardPager和TabPager
更換了新的資料來源，讓程式擁有更多路線可供查詢。
同時保留了前一個版本可離線使用的功能! 希望大家能夠繼續愛用~

● 瀏覽功能 - 瀏覽過的路線會記錄下來，如要查詢新路線，建議連接網路。
● 我的最愛 - 可以將常用路線儲存下來。如果有儲存的路線，會自動先打開最愛列表。

注意！第一次查詢的車站或地圖，必須有連上網路，否則無法取得時刻或地圖。
如果是已經查詢過的路線，則會直接讀取離線資料，欲取得最新資料請選擇刷新的功能。
如果您不想使用新版，也可隨時切換到舊版界面唷！

資料來源：交通部公路總局 http://goo.gl/s6dKet
目前行駛路線及班次，可能因各站實際情況而有所調整。

高鐵快捷公車全數路線2013年度免費搭乘優惠皆至12月31日止。新聞稿請參閱：http://goo.gl/XOsbU

我的網誌連結：http://goo.gl/XYtqh

#高鐵 #台灣高鐵 #高鐵公車 #高鐵接駁
--
0.10.0, r60 @ 2014-01-14
add new 2014 wizard

0.9.11, r58 @ 2013-10-27
try to fix Array OutOfBound
    java.lang.IndexOutOfBoundsException: Invalid index 1, size is 0

0.9.10, r57 @ 2013-10-12
Sony Ericsson XPERIA Arc S LT18i
    java.lang.String cannot be cast to java.util.Set
add Google Analytics: Tracking ID UA-4163873-7

Currently we only have old cache data for you due to a new version of MOTC website.
The schedule may not be the same as local transportation. Sorry about the inconvenience.

0.9.9, r56 @ 2013-09-27
高鐵網站改版~ 對parsing資料造成問題，改用cache到google drive的圖檔代替
    將路線改成讀取程式內的陣列，圖檔也是，改為GDrive當作來源
remove ThsrRouteChoiceFragment thread loading
add MOTC website links

1.0.0, r55 @ 2013-09-21
en-rUS

r54 @ 2013-09-17
change the replace order to speed up new data checking

0.9.8, r53 @ 2013-09-10
remove Modal loading dialogs => they are awful.
adjust the tablet layout

0.9.7, r52 @ 2013-09-08
change default language to zh-rTW

0.9.6, r51 @ 2013-09-07
fix sw600dp-port/sw720dp-port map preview too large
add Google Drive support
    config control
    map key update

0.9.5, r50 @ 2013-09-06
quick fix for new route data that DGH, MOTC updated
    更新時間：2013/9/4  維護單位：運輸管理科
change the map image preview for the phone

0.9.3, r49 @ 2013-09-03
add switch back to old version

0.9.1

r46 @ 2013-08-24
add network check
delete cached map if exists

r45 @ 2013-08-23 @ work
fix refresh(download new data) won't show ProgressDialog(caused by r43 patch)
use lighter color for outdated bus

r44 @ 2013-08-22 @ home
add sorting of favorite routes
add version info to about dialog
remove debug logcat

r43 @ 2013-08-22 @ work
check current index before popup ProgressDialog in DownloadStart()
	modify the interface, add a parameter to store current index
delete all cache files(internal/external)
fix Skip will cause next app open still shows tutorial

r42 @ 2013-08-21
add SampleActivity for demo purpose (some already done in r41)
change portrait mode layout_padding for wider view

r41 @ 2013-08-16
change action bar more icon

0.9.0

r40 @ 2013-08-14 @ home
prepare the about dialog and change all themes to light
prepare the version for online test

r39 @ 2013-08-14 @ work
code optimization and tweak
add multiple screen menu support

r38 @ 2013-08-14
Android setSringSet() / getStringSet() with compatibility
    https://gist.github.com/shreeshga/5398506
Fix back compatibility to Froyo/Gingerbread
    ActionBarSherlock List Navigation is so ugly
    and need Light theme for ListView (work around)

r37 @ 2013-08-13
different screen size check
disable no use service and receiver in 0.9.x

r36 @ 2013-08-12
adjust for large screen layout/logic

r35 @ 2013-08-11
fix some first-use bugs

r34 @ 2013-08-11
add refresh function
    also update the favorite items
improve and fix some parsing errors
    fix the missing data when changing rows
    add workaround functions for bot pre and post processing
    add color and more data to some columns
better UX for putting some loading into threads

r33 @ 2013-08-11
change the highlight color

r32 @ 2013-08-10
favorite function: done
stored in SharedPreference, read and show in TabViewPager
fix getLeaveTime sync with today's date

r31 @ 2013-08-10
fix switching page in StripStep and ViewPager

r30 @ 2013-08-08
fix new selection mode in ThsrRouteChoice that cause no data in RouteView
    also modified Model.clear to clean data
add a new method to sync the data between fragments

r29 @ 2013-08-06
asynchronous loading the data from network
add cache to external cache
use interface/listener callback for data download
add loading progress to UI
add new multi-languages
add Map button click and download and display
adjust the flow, maybe need more tweak

r28 @ 2013-08-05
download data and show in ExpandableListView/GridView
    GridView inside Expandable list in android
        http://stackoverflow.com/a/5726131
        http://blog.csdn.net/lilu_leo/article/details/7163457
add listener to MotcDataHelper

TO DO
1. make MotcDataHeler as asynchronous one
2. make download thread not blocking
3. make UI has loading progress indicator

r27 @ 2013-08-05 @ work
將 wizard 第二頁的 Fragment 換成自訂(使用自訂的layout)
    增加儲存選項的 index
use AbsListViewHelper

r26 @ 2013-08-04
將所有地圖圖片上傳到 Google Drive (暫時沒用 SDK 來開發，考慮之後要用)
將下載的資料轉成 JSON 成功。轉換成文字寫入檔案，再讀回來轉成物件，成功。
    need trim() to get rid of unused buffer in file read

r25 @ 2013-08-03
準備 MOTC 資料: download and parsing now ready
修好跑馬燈 http://stackoverflow.com/a/3333855

r24 @ 2013-08-02 @ home
將最後一頁的UI準備好(利用 ReviewItem 的特性)

r23 @ 2013-08-02 @ work
修正一些原本將 WizardPager 放入MyUtilsLibrary 沒考慮到的功能
    目前不太確定為什麼 Page.isRequired 似乎不如預期功能?
    --> 關閉 ButtonBar 功能，並在 StepPagerStrip 裡增加判斷
完成新的 ActionBar ListNavigation 架構及 WizardPager 的切換
    還不確定是否會和 TabPager 產生衝突，需要進一步實作

TO DO:
1. MyFavorite 的 TabPager
2. 第一次的教學引導(設定我的最愛)
3. ActionBarMenu
4. 離線/即時更新資料

r22 @ 2013-08-02 @ home
使用新的icon (較有立體感)
使用 WizardPager 的 library (from MyUtilsLibrary, thanks Roman Nurik)
    移除重複的 class (originate from MyUtilsLibrary)
增加 SplashActivity
建立完整的公車路線(離線陣列資料，還不確定即時資料的更新是否能實作成功)

原本要套用 https://github.com/TechFreak/WizardPager
但是編譯一直不過，感覺是Android Studio的設定還不夠完善(或者我不會用XD)
原本還要使用 ActionBarCompat (v7 support lib from Google)
也是編譯上的問題，換回來使用ActionBarSherlock

===
這是從高鐵網站下載的快捷公車時刻表
原本只是方便自己 可以記得高鐵的接駁車時間
趁著這個機會 順便熟悉ActionBar和Service的功能!
既然做好了 就順便分享給有緣人:)

支援功能(最新: 0.8.3)
● 離線閱讀高鐵快捷公車時刻表(資料由高鐵網站提供)
● 高鐵轉乘資訊地圖(PDF 由高鐵網站提供)(需安裝其他 PDF 閱讀器)
● 高鐵聯外客運班次時刻表及路線圖(連結至交通部公路總局)(須有網路連線)
● 可以將程式移到 SD 卡
--
0.8.3 (2013/06/07)
增加可以移到 SD card 的選項(android:installLocation="auto")
增加了提示正在下載檔案的Toast
修改了proguard.cfg 利用ProGuard 4.7將程式大小減少了100k
    -dontwarn android.support.**
    check http://my.eoe.cn/futurexiong/archive/1595.html

--
0.8.2 (2013/05/12)
移除直向螢幕的需求，以支援更多裝置

--
0.8.1 (2013/05/01)
判斷系統是否支援PDF閱讀器，移除內建的PdfViewer
新增需求寫入SD權限，讓PDF閱讀器可以開啟

--
0.8.0 (2013/04/30)
增加PDF連結，加入PdfViewer.jar
增加交通部網站連結，須連結網路才能使用

--
0.7.0 (2013/04/20)
修正在較低解析度裝置上，圖片顯示不正確
增加對螢幕旋轉的支援

0.6.1 (2013/01/19)
增加連結到高鐵網站
修改小圖示、動畫及更新提示

0.6 (2013/01/10)
修正在Galaxy Note 2上變形的問題
在點擊尚未下載過的圖片時，加上載入中的提示
其他小修正

0.5.2 (2013/01/04)
修正可能造成錯誤關閉程式的程式碼

0.5.1 (2012/12/27)
修正排版，並重新上傳新版螢幕擷取畫面 

0.5   update with latest SDK (API=17)
