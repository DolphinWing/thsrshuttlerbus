package dolphin.android.apps.RailwayTimeHelper.provider;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by dolphin on 2014/4/12.
 */
public class TimeInfo {

    public int Station;

    public String DepartTime;

    public String ArriveTime;

    public int Order;

    public int Route;

    public static TimeInfo readTimeInfo(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        TimeInfo timeInfo = new TimeInfo();
        parser.require(XmlPullParser.START_TAG, null, "TimeInfo");
        //String tag = parser.getName();
        timeInfo.ArriveTime = parser.getAttributeValue(null, "ARRTime");
        timeInfo.DepartTime = parser.getAttributeValue(null, "DEPTime");
        timeInfo.Order = Integer.parseInt(parser.getAttributeValue(null, "Order"));
        //timeInfo.Route = Integer.parseInt(parser.getAttributeValue(null, "Route"));
        timeInfo.Station = Integer.parseInt(parser.getAttributeValue(null, "Station"));
        //parser.require(XmlPullParser.END_TAG, null, "TimeInfo");
        parser.nextTag();
        return timeInfo;
    }

    @Override
    public String toString() {
        return String.format("{Order=%d,Station=%d,arrive=%s,depart=%s}",
                Order, Station, ArriveTime, DepartTime);
    }

//    private boolean before(String src, String dst) {
//        return src.compareTo(dst) <= 0;
//    }
//
//    private boolean after(String src, String dst) {
//        return src.compareTo(dst) >= 0;
//    }
//
//    public boolean canCatch(int hour, int minute) {
//        return after(String.format("%02d:%02d:00", hour, minute), DepartTime);
//    }
//
//    public boolean canVisit(int hour, int minute) {
//        return before(String.format("%02d:%02d:00", hour, minute), ArriveTime);
//    }
}
