/**
 * Created by dolphin on 2014/4/10.
 */
package dolphin.android.apps.RailwayTimeHelper.provider;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import dolphin.android.net.HttpHelper;
import dolphin.android.util.FileUtils;

public class RailwayTimeHelper extends HttpHelper {

    private final static String TAG = "RailwayTimeHelper";

    private final static boolean LOG_V = false;

    private Context mContext;

    public RailwayTimeHelper(Context context) {
        mContext = context;
    }

    private final static String URL_TRA_OPEN_DATA = "http://163.29.3.98/xml/";

    private final static String URL_TRA_OPEN_DATA_ZIP = URL_TRA_OPEN_DATA + "@name.zip";

    public Map<String, Calendar> getAvailableDates() {
        Map<String, Calendar> mapCalendar = new HashMap<String, Calendar>();
        long startTime = System.currentTimeMillis();
        try {
            String html = getUrlContent(URL_TRA_OPEN_DATA);
            //Log.d(TAG, html);
            //<A HREF="/xml/20140402.zip">20140402.zip</A>
            Matcher matcher = Pattern.compile("<A[^>]*>([\\d]{8})").matcher(html);
            while (matcher != null && matcher.find()) {
                String date = matcher.group(1);
                //if (matcher.group(1).contains(".zip")) {
                //Log.d(TAG, date);
                //}
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
                cal.set(Calendar.MONTH, Integer.parseInt(date.substring(4, 6)) - 1);
                cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(6)));
                //Log.v(TAG, String.format("%tF", cal));
                mapCalendar.put(date, cal);
            }
        } catch (Exception e) {
            Log.e(TAG, "checkDate: " + e.getMessage());
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();

        //How to sort HashMap by key and value in Java
        //http://goo.gl/itGgQN
        List<String> keys = new LinkedList<String>(mapCalendar.keySet());
        Collections.sort(keys, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });

        Map<String, Calendar> sortedMap = new LinkedHashMap<String, Calendar>();
        for (String key : keys) {
            sortedMap.put(key, mapCalendar.get(key));
        }

        Log.v(TAG, String.format("wasted %d ms", ((endTime - startTime))));
        return sortedMap;
    }

    public String getXmlData(String name) {
        File zipFile = new File(mContext.getExternalCacheDir(),
                String.format("%s.zip", name));
        File xmlFile = new File(mContext.getExternalCacheDir(),
                String.format("%s.xml", name));
        if (xmlFile.exists()) {//use local cache
            Log.v(TAG, xmlFile.getAbsolutePath());
            return FileUtils.readFileToString(xmlFile);
        }

        long startTime = System.currentTimeMillis();

        boolean r = HttpHelper.getRemoteFile(mContext,
                URL_TRA_OPEN_DATA_ZIP.replace("@name", name),
                zipFile.getAbsolutePath());
        if (r) {
            long endTime = System.currentTimeMillis();
            Log.v(TAG, String.format("download %s wasted %d ms",
                    zipFile.getName(), ((endTime - startTime))));

            ZipInputStream zis = null;
            try {
                zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)));

                ZipEntry ze;
                while ((ze = zis.getNextEntry()) != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int count;
                    while ((count = zis.read(buffer)) != -1) {
                        baos.write(buffer, 0, count);
                    }
                    String filename = ze.getName();
                    //byte[] bytes = baos.toByteArray();
                    // do something with 'filename' and 'bytes'...
                    //Log.v(TAG, filename + " " + bytes.length);
                    if (filename.startsWith(name)) {
                        r = FileUtils.writeStringToFile(xmlFile, baos.toString());
                        Log.v(TAG, String.format("%s write %d bytes %s", filename,
                                baos.toByteArray().length, r ? "success" : "failed"));
                    }
                    return FileUtils.readFileToString(xmlFile);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } //finally {
            //    //zis.close();
            //}
        } else {
            Log.w(TAG, zipFile.getAbsolutePath() + " download failed");
        }
        return null;
    }

    public List<TrainInfo> parseXmlData(String name) {
        File xmlFile = new File(mContext.getExternalCacheDir(),
                String.format("%s.xml", name));
        if (xmlFile.exists()) {
            XmlPullParser parser = Xml.newPullParser();
            try {
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(new FileInputStream(xmlFile), null);
                parser.nextTag();
                return readTrainList(parser);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.w(TAG, xmlFile.getAbsolutePath() + " gg");
        return null;
    }

    public List<TrainInfo> readTrainList(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        List<TrainInfo> list = new ArrayList<TrainInfo>();
        parser.require(XmlPullParser.START_TAG, null, "TaiTrainList");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            //Log.v(TAG, "parser " + name);
            // Starts by looking for the entry tag
            if (name.equals("TrainInfo")) {
                TrainInfo trainInfo = TrainInfo.readTrainInfo(parser);
                if (LOG_V) {
                    //Log.v(TAG, trainInfo.Id + " " + trainInfo.TimeList.size());
                    Log.v(TAG, trainInfo.toString());
                    for (int i = 0; i < trainInfo.TimeList.size(); i++) {
                        int key = trainInfo.TimeList.keyAt(i);
                        TimeInfo timeInfo = trainInfo.TimeList.get(key);
                        Log.v(TAG, "  " + timeInfo.toString());
                    }
                }
                list.add(trainInfo);
            } else {
                //skip(parser);
            }
        }
        return list;
    }

    public List<TrainInfo> getAvailableTrains(List<TrainInfo> list, int car,
            int departStat, int arriveStat,
            int startHour, int startMins, int endHour, int endMins) {
        HashMap<TimeInfo, TrainInfo> availableTrains = null;
        if (list != null) {
            availableTrains = new HashMap<TimeInfo, TrainInfo>();
            //http://goo.gl/DUf03O
            for (TrainInfo trainInfo : list) {
                //check if CarClass OK
                switch (car) {
                    case TrainInfo.CLASS_RESERVED:
                        if (trainInfo.CarClass >= 1130) {
                            continue;
                        }
                        break;
                    case TrainInfo.CLASS_NON_RESERVED:
                        if (trainInfo.CarClass < 1130) {
                            continue;
                        }
                        break;
                    case TrainInfo.CLASS_ALL:
                    default:
                        break;
                }

                //check station that contains depart station
                TimeInfo departTime = trainInfo.TimeList.get(departStat);
                if (departTime == null) {
                    //Log.v(TAG, "depart: " + trainInfo.toString());
                    continue;
                }

                //check station that contains arrive station
                TimeInfo arriveTime = trainInfo.TimeList.get(arriveStat);
                if (arriveTime == null) {
                    //Log.v(TAG, "arrive: " + trainInfo.toString());
                    continue;
                }

                //check if the order correct
                if (departTime.Order >= arriveTime.Order) {
                    //Log.v(TAG, "reverse: " + trainInfo.toString());
                    continue;
                }

                //check if the time in range
                if (departTime.DepartTime.compareTo(String.format("%02d:%02d:00",
                        startHour, startMins)) < 0) {
                    //Log.v(TAG, "  " + departTime.toString());
                    continue;
                }
                if (departTime.DepartTime.compareTo(String.format("%02d:%02d:00",
                        endHour, endMins)) > 0) {
                    //Log.v(TAG, "  " + departTime.toString());
                    continue;
                }

                Log.v(TAG, trainInfo.toString());
                Log.v(TAG, "  " + departTime.toString());
                Log.v(TAG, "  " + arriveTime.toString());
                availableTrains.put(departTime, trainInfo);
            }

            //sort the list
            List<TimeInfo> keys = new LinkedList<TimeInfo>(availableTrains.keySet());
            Collections.sort(keys, new Comparator<TimeInfo>() {

                @Override
                public int compare(TimeInfo timeInfo1, TimeInfo timeInfo2) {
                    return timeInfo1.DepartTime.compareTo(timeInfo2.DepartTime);
                }
            });

            List<TrainInfo> trainList = new ArrayList<TrainInfo>();
            for (TimeInfo info : keys) {
                trainList.add(availableTrains.get(info));
            }
            return trainList;
        }

        return null;
    }
}
