package dolphin.android.apps.RailwayTimeHelper.provider;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.SparseArray;

import java.io.IOException;

/**
 * Created by dolphin on 2014/4/12.
 */
public class TrainInfo {

    public final static int CLASS_ALL = 0;

    public final static int CLASS_RESERVED = 1;

    public final static int CLASS_NON_RESERVED = 2;

    public final static int LINE_NA = 0;

    public final static int LINE_MOUNTIAN = 1;

    public final static int LINE_SEA = 2;

    public final static int TYPE_NORMAL = 0;

    public final static int TYPE_OCCASION = 1;

    public final static int TYPE_GROUP = 2;

    public final static int TYPE_ADDITIONAL = 3;

    public String Id;

    public int CarClass;

    public int Route;

    public int Line;

    public boolean DirectionClockwise;//LineDir

    public int OverNightStation;

    public boolean Cripple;

    public boolean Package;

    public boolean Dinning;

    public int Type;

    public String Note;

    public SparseArray<TimeInfo> TimeList;

    public static TrainInfo readTrainInfo(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        TrainInfo trainInfo = new TrainInfo();
        trainInfo.TimeList = new SparseArray<TimeInfo>();
        parser.require(XmlPullParser.START_TAG, null, "TrainInfo");

        trainInfo.Id = parser.getAttributeValue(null, "Train");
        trainInfo.CarClass = Integer.parseInt(parser.getAttributeValue(null, "CarClass"));
        trainInfo.Line = Integer.parseInt(parser.getAttributeValue(null, "Line"));

        String direction = parser.getAttributeValue(null, "LineDir");
        trainInfo.DirectionClockwise = direction.equals(String.valueOf(0));

        String station = parser.getAttributeValue(null, "OverNightStn");
        trainInfo.OverNightStation = Integer.parseInt(station);

        String cripple = parser.getAttributeValue(null, "Cripple");
        trainInfo.Cripple = cripple.equalsIgnoreCase("Y");

        String dinning = parser.getAttributeValue(null, "Dinning");
        trainInfo.Dinning = dinning.equalsIgnoreCase("Y");

        String packaging = parser.getAttributeValue(null, "Package");
        trainInfo.Package = packaging.equalsIgnoreCase("Y");

        trainInfo.Type = Integer.parseInt(parser.getAttributeValue(null, "Type"));
        trainInfo.Note = parser.getAttributeValue(null, "Note");

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("TimeInfo")) {
                TimeInfo timeInfo = TimeInfo.readTimeInfo(parser);
                if (timeInfo != null) {
                    trainInfo.TimeList.put(timeInfo.Station, timeInfo);
                }
            }
        }
        return trainInfo;
    }

    @Override
    public String toString() {
        String line = "N";
        switch (Line) {
            case LINE_MOUNTIAN:
                line = "M";
                break;
            case LINE_SEA:
                line = "S";
                break;
            case LINE_NA:
            default:
                break;
        }
        String type = "N";
        switch (Type) {
            case TYPE_ADDITIONAL:
                type = "A";
                break;
            case TYPE_GROUP:
                type = "G";
                break;
            case TYPE_OCCASION:
                type = "O";
                break;
            case TYPE_NORMAL:
            default:
                break;
        }
        return String.format("{Id=%s,CarClass=%d,Line=%s,Type=%s,Stations=%d}",
                Id, CarClass, line, type, TimeList.size());
    }
}
