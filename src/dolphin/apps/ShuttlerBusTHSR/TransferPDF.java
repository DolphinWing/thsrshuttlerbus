package dolphin.apps.ShuttlerBusTHSR;

import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRDataHelper;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferPdf;

public class TransferPDF extends Activity
{
	public static final String TAG = "TransferPDF";

	private ArrayList<TransferPdf> mList;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.empty_linear_layout);

		load_pdfs();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		IntentFilter filter =
			new IntentFilter(THSRData.INTENT_DOWNLOAD_COMPLETE);
		registerReceiver(mReceiver, filter);
	}

	@Override
	protected void onPause()
	{
		unregisterReceiver(mReceiver);

		super.onPause();
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			Log.i(TAG, action);
			if (action.equalsIgnoreCase(THSRData.INTENT_DOWNLOAD_COMPLETE)) {
				load_pdfs();
			}
		}
	};

	private void load_pdfs()
	{
		THSRDataHelper dataHelper = new THSRDataHelper(getBaseContext());
		mList = dataHelper.get_transfer_pdf();
		Log.d(TAG, String.format("mList size = %d", mList.size()));

		LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout01);
		layout.removeAllViews();

		if (mList == null || mList.size() <= 0) {//no data
			TextView tv = new TextView(getBaseContext());
			tv.setText(R.string.no_data);
			layout.addView(tv);
		}
		else
			for (int i = 0; i < mList.size(); i++) {
				Button bt = new Button(getBaseContext());
				bt.setText(mList.get(i).getTitle());
				bt.setTag(i);
				bt.setOnClickListener(onOpenFile);
				layout.addView(bt);
			}
	}

	private Button.OnClickListener onOpenFile = new Button.OnClickListener() {

		@Override
		public void onClick(View btn)
		{
			Log.d(TAG, String.format("btn %s", btn.getTag().toString()));

			TransferPdf item =
				mList.get(Integer.parseInt(btn.getTag().toString()));
			Log.d(TAG, String.format("%s", item.getName()));

			//open browser?
			Toast
					.makeText(getBaseContext(), item.getName(),
						Toast.LENGTH_SHORT).show();
		}
	};
}
