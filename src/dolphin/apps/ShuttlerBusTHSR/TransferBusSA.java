package dolphin.apps.ShuttlerBusTHSR;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.text.Html;
import android.text.SpannableString;
import android.text.format.DateUtils;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.google.analytics.tracking.android.EasyTracker;
import com.quanta.pobu.app.utils.QProgressDialog;
import com.quanta.pobu.net.QHttpHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import dolphin.android.util.FileUtils;
import dolphin.android.util.PackageUtils;
import dolphin.apps.ShuttlerBusTHSR.motc.DghMotcActivity;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcDataHelper;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferRoute;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferStation;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRDataHelper;

public class TransferBusSA extends SherlockActivity {
    public static final String TAG = "TransferBus";

    private ArrayList<TransferStation> mList;

    private static final int PAGE_TYPE_UNKNOWN = 0;
    private static final int PAGE_TYPE_STATION = 1;
    private static final int PAGE_TYPE_ROUTE = 2;
    private static final int PAGE_TYPE_MAP = 3;
    private static final int PAGE_TYPE_PDF = 4;//[16]++
    //private static final int PAGE_TYPE_WEB = 5;//[16]++

    private int mPageType = PAGE_TYPE_UNKNOWN;

    private TextView mTextView1, mTextView2;
    //    PhotoViewAttacher mAttacher;//[0.7.0]++ add support to zoom
    //[0.7.0]++ add support to screen rotation
    private final static String EXTRA_PAGE_TYPE = "page_type";
    private final static String EXTRA_STATION = "station";
    private final static String EXTRA_ROUTE = "route";

    //[16]++ add map index array to find PDF url
    private int[] mMapIndex;
    private String[] mMapName;
    //private String mPdfMapUrlBase;
    private String[] mMapUrl;
    //private String mMotcUrlBase;
    private int[] mMotcIndex;
    private Intent mPdfSupport;
    private boolean mGDCache;//[56]++

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        setContentView(R.layout.empty_scrollview_layout);
        setSupportProgressBarIndeterminateVisibility(true);//[12]++

        final Context context = getBaseContext();
        //since API level=9
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // http://goo.gl/cmG1V , solve android.os.NetworkOnMainThreadException
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork() // or .detectAll() for all detectable problems     
                    .penaltyLog()
                    .build());
        }

        //[16]++ load global variable arrays
        mMapIndex = getResources().getIntArray(R.array.list_index);
        mMapName = getResources().getStringArray(R.array.pdf_map_name);
        //mPdfMapUrlBase = getString(R.string.pdf_map_url_base);
        mMapUrl = getResources().getStringArray(R.array.pdf_map_url);
        //mMotcUrlBase =  getString(R.string.motc_url_base);
        mMotcIndex = getResources().getIntArray(R.array.motc_index);
        mGDCache = context.getResources().getBoolean(R.bool.thsr_use_cache);

//        //[17]++ export Version.pdf
//        try {
//            AssetUtils.copyAssets(context, "Version.pdf", getExternalCacheDir());
//            //Uri path = Uri.parse("android.resource://dolphin.apps.ShuttlerBusTHSR/raw/Version.pdf");
//            Uri path = Uri.fromFile(new File(getExternalCacheDir(), "Version.pdf"));
//            mPdfSupport = new Intent(Intent.ACTION_VIEW);
//            mPdfSupport.setDataAndType(path, "application/pdf");
//            //Log.d(TAG, path.toString());
//            if (!PackageUtils.isCallable(context, mPdfSupport))
//                Toast.makeText(context, R.string.pdf_no_available, Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.e(TAG, "copyAssets: " + e.getMessage());
//        }
        mPdfSupport = null;//don't support PDF

        //[0.7.0]++ add support to screen rotation
        if (savedInstanceState != null) {
            mPageType = savedInstanceState.getInt(EXTRA_PAGE_TYPE, PAGE_TYPE_UNKNOWN);
            mCurrentStation = savedInstanceState.getInt(EXTRA_STATION, -1);
            mCurrentRoute = savedInstanceState.getInt(EXTRA_ROUTE, -1);
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                THSRDataHelper dataHelper = new THSRDataHelper(context);
                mList = dataHelper.get_transfer_station(mGDCache);
                //Log.d(TAG, String.format("stations = %d", mList.size()));

                //                Log.v(TAG, "mPageType = " + mPageType);
                //                Log.v(TAG, "mCurrentStation = " + mCurrentStation);
                //                Log.v(TAG, "mCurrentRoute = " + mCurrentRoute);
                switch (mPageType) {//[0.7.0]++ add support to screen rotation
                    case PAGE_TYPE_MAP:
                        prepare_load_gif();
                        break;
                    case PAGE_TYPE_ROUTE:
                        load_route(getBaseContext());
                        break;
                    case PAGE_TYPE_STATION:
                    default:
                        load_station(context);
                        break;
                }
            }
        });

        //        mTextView1 = (TextView) findViewById(android.R.id.text1);
        //        if (THSRDataHelper.getUpdateTime(context).get(Calendar.YEAR) < 2013) {
        //            mTextView1.setVisibility(View.GONE);
        //        }
        //        else {
        //            mTextView1.setText(getString(R.string.update_at,
        //                DateUtils.getRelativeTimeSpanString(
        //                    THSRDataHelper.getUpdateTime(context).getTimeInMillis(),
        //                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS)));
        //        }
        //        final PackageInfo pinfo =
        //            PackageUtils.getPackageInfo(getBaseContext(), TransferBusSA.class);
        //        //String version = String.format(Locale.US, "%s.%d", 
        //        //    pinfo.versionName, pinfo.versionCode);
        //        mTextView2 = (TextView) findViewById(android.R.id.text2);
        //        mTextView2.setText(String.valueOf(pinfo.versionName));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // return super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_item_cache:
                //download_cache();
                //mHandler.sendEmptyMessage(MSG_DOWNLOAD_CACHE);
                showMsgDialog(R.string.start_download_cache);//show loading dialog
                //[21]++ clear the file cache and download again
                if (mPageType == PAGE_TYPE_MAP) {//[21]++
                    TransferRoute route = mList.get(mCurrentStation)
                            .getRoutes().get(mCurrentRoute);
                    File f = getBaseContext().getFileStreamPath(route.getName());
                    if(mGDCache)//[56]++
                        f = new File(getExternalCacheDir(), route.getDstName());
                    boolean r = f.delete();
                    Log.w(TAG, String.format("delete %s %s", f.getAbsolutePath(),
                            r ? " success" : " failed"));//[56]++
                    //download again
                    prepare_load_gif();
                } else {
                    //Log.d(TAG, getBaseContext().getFilesDir().getAbsolutePath());
                    File[] files = mGDCache ? getExternalCacheDir().listFiles()
                            : getFilesDir().listFiles();
                    for (File f : files)
                        f.delete();
                    //Log.d(TAG, String.format("after delete: files = %d",
                    //        getFilesDir().listFiles().length));
                    if(mGDCache)//[56]++
                        load_station(getBaseContext());
                    else
                    sendBroadcast(new Intent(THSRData.INTENT_DOWNLOAD_START));
                }
                break;
            case R.id.menu_item_close://[0.7.0]++ directly finish the activity
                TransferBusSA.this.finish();
                break;
            case R.id.menu_item_preference:
                //send_message(EVENT_MSG_UPDATE_DESCRIPTION, 0, 0, 10);
                break;
            case R.id.menu_item_use_new:
                useNewVersion();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private QProgressDialog mDialog = null;

    private void showMsgDialog(int msgRes) {
        showMsgDialog(getString(msgRes));
    }

    private void showMsgDialog(String msg) {
        setSupportProgressBarIndeterminateVisibility(true);
        if (mDialog != null) {//already has a dialog
            if (msg != null) {//directly show the message
                mDialog.setMessage(msg);
            }
        } else {//create a new dialog
            mDialog =
                    QProgressDialog.show(TransferBusSA.this, msg, true,
                            new Dialog.OnCancelListener() {

                                @Override
                                public void onCancel(DialogInterface arg0) {
                                    if (!mLoadingImage)
                                        TransferBusSA.this.finish();//finish activity
                                    //else//[21]++
                                    //    Toast.makeText(TransferBusSA.this,
                                    //            R.string.wait_download_process,
                                    //            Toast.LENGTH_SHORT).show();
                                }
                            });
        }
    }

    private void hideMsgDialog() {
        if (mDialog != null) {//hide the existing dialog
            mDialog.dismiss();
        }
        mDialog = null;//clear the memory
        setSupportProgressBarIndeterminateVisibility(false);
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(mReceiver, new IntentFilter(THSRData.INTENT_DOWNLOAD_COMPLETE));
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mReceiver);

        super.onPause();
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(TAG, action);
            if (action.equalsIgnoreCase(THSRData.INTENT_DOWNLOAD_COMPLETE)) {
                if (intent.getBooleanExtra(THSRData.EXTRA_RESULT, false)) {
                    Log.i(TAG, "update complete!");
                    THSRDataHelper dataHelper = new THSRDataHelper(getBaseContext());
                    mList = dataHelper.get_transfer_station();
                    //Log.d(TAG, String.format("stations = %d", mList.size()));
                    dataHelper.setUpdateTime(System.currentTimeMillis());//[13]++
                } else {
                    mList = null;
                }

                load_station(context);
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (mLoadingImage) {//[21]++
            Toast.makeText(this, R.string.wait_download_process,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        switch (mPageType) {
            case PAGE_TYPE_ROUTE:
                load_station(getBaseContext());
                break;
            case PAGE_TYPE_MAP:
                load_route(getBaseContext());
                break;
            case PAGE_TYPE_STATION:
            default:
                super.onBackPressed();
                break;
        }
        //mLoadingImage = false;
    }

    private void load_station(Context context) {
        mPageType = PAGE_TYPE_STATION;
        setSupportProgressBarIndeterminateVisibility(true);//[12]++
        setContentView(R.layout.empty_scrollview_layout);
        load_version(context);

        //Context context = getBaseContext();
        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout01);
        layout.removeAllViews();
        if (mList == null || mList.size() <= 0) {//no data
            //layout.addView(getSourceLinkView(context));//[13]++ add link back to THSR website
            layout.addView(getEmptyView(context));
        } else {
            int i = 0;//[16]++ for 2 more extra station
            for (i = 0; i < mList.size(); i++) {
                //Button bt = new Button(getBaseContext());
                //bt.setText(mList.get(i).getName());
                View v = getOptionView(context, mList.get(i).getName());
                //[16]-- View v = getOptionView(context, mMapName[mMapIndex[i]]);
                v.setTag(i);
                v.setOnClickListener(onOpenStation);
                layout.addView(v);
            }
            //[16]++ load extra stations not having the map
            layout.addView(getSeparator(context));

            View v1 = getOptionView(context, mMapName[mMapIndex[i]]);
            v1.setTag(i++);
            v1.setOnClickListener(onOpenStation);
            layout.addView(v1);

            if(!mGDCache) {//[56]++
                View v2 = getOptionView(context, mMapName[mMapIndex[i]]);
                v2.setTag(i++);
                v2.setOnClickListener(onOpenStation);
                layout.addView(v2);
            }
        }
//[56]--
//        if (THSRDataHelper.getUpdateTime(context).get(Calendar.YEAR) >= 2013) {
//            //layout.addView(getUpdateTimeView(context));//[13]++ add update time
//            getUpdateTimeView(context);
//        }
        layout.addView(getSourceLinkView(context));//[13]++ add link back to THSR website

        hideMsgDialog();//[12]++
    }

    private int mCurrentStation = -1;

    private Button.OnClickListener onOpenStation =
            new Button.OnClickListener() {

                @Override
                public void onClick(View btn) {
                    mCurrentStation = Integer.parseInt(btn.getTag().toString());
                    //Log.d(TAG, String.format("btn %s", currentStation));

                    //TransferStation item = mList.get(mCurrentStation);
                    //Log.d(TAG, String.format("%s", item.getName()));

                    load_route(getBaseContext());
                }
            };

    private void load_route(Context context) {
        mPageType = PAGE_TYPE_ROUTE;
        setSupportProgressBarIndeterminateVisibility(true);//[12]++
        setContentView(R.layout.empty_scrollview_layout);
        load_version(context);

        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout01);
        layout.removeAllViews();

        if (mCurrentStation >= mList.size()) {//[16]++ for Taipei and Zuoying
            load_extra_data(context, layout);
        } else {
            ArrayList<TransferRoute> routes =
                    mList.get(mCurrentStation).getRoutes();
            if (routes == null || routes.size() <= 0) {//no data
                layout.addView(getEmptyView(getBaseContext()));
            } else {
                for (int i = 0; i < routes.size(); i++) {
                    //Button bt = new Button(getBaseContext());
                    //bt.setText(routes.get(i).getTitle());
                    View v = getOptionView(context, routes.get(i).getTitle());
                    v.setTag(i);
                    v.setOnClickListener(onOpenRoute);
                    layout.addView(v);
                }
                //[16]++ add PDF path download
                if (routes.size() > 0) //only add separator when route more than 1
                    layout.addView(getSeparator(context));
                load_extra_data(context, layout);
            }
        }
        //layout.addView(getSourceLinkView(context));//[13]++ add link back to THSR website

        hideMsgDialog();//[12]++
    }

    private boolean mLoadingImage = false;
    private int mCurrentRoute = -1;
    private Button.OnClickListener onOpenRoute = new Button.OnClickListener() {

        @Override
        public void onClick(View btn) {
            if (mLoadingImage) {
                Toast.makeText(TransferBusSA.this, R.string.wait_download_process,
                        Toast.LENGTH_SHORT).show();//[21]++
                return;
            }

            mCurrentRoute = Integer.parseInt(btn.getTag().toString());
            //Log.d(TAG, String.format("btn %s", mCurrentRoute));
            prepare_load_gif();
        }
    };

    private void load_extra_data(Context context, ViewGroup parent) {
        View pdfView = getOptionView(context, context.getString(R.string.pdf_map));
        pdfView.setOnClickListener(onOpenPdf);
        pdfView.setEnabled(PackageUtils.isCallable(context, mPdfSupport));
        pdfView.setVisibility(View.GONE);//[56]-- don't use in new version
        parent.addView(pdfView);

        if (mMotcIndex[mMapIndex[mCurrentStation]] > 0) {//check MOTC index
            View motcView = getOptionView(context, context.getString(R.string.dgh_motc_info));
            motcView.setOnClickListener(onOpenMOTC);
            motcView.setEnabled(QHttpHelper.checkNeworkAvailable(context));
            parent.addView(motcView);
        }
    }

    private Button.OnClickListener onOpenPdf = new Button.OnClickListener() {

        @Override
        public void onClick(View btn) {
            if (mLoadingImage) {
                Toast.makeText(TransferBusSA.this, R.string.wait_download_process,
                        Toast.LENGTH_SHORT).show();//[21]++
                return;//avoid multiple reading, slowing down performance
            }
            mLoadingImage = true;
            setSupportProgressBarIndeterminateVisibility(true);

            final Context context = getBaseContext();
            final String fileName = mMapUrl[mMapIndex[mCurrentStation]];
            final String fileUrl = getString(R.string.pdf_map_url_base, fileName);

            new Thread(new Runnable() {

                @Override
                public void run() {
                    File tmpFile = new File(getExternalCacheDir(), fileName);
                    File pdfFile = context.getFileStreamPath(fileName);
                    String pdfPath = "";
                    if (tmpFile.exists()) {//read from external cache
                        pdfPath = tmpFile.getAbsolutePath();
                    } else if (pdfFile.exists()) {
                        //read from cache
                        Log.v(TAG, String.format("exist: %s", fileName));
                        //load_gif(true, item.getName());
                        try {//http://wzhiju.iteye.com/blog/1119037
                            pdfPath = moveToExternalCache(pdfFile, tmpFile);
                        } catch (IOException e) {
                            Log.e(TAG, "PDF: " + e.getMessage());
                            e.printStackTrace();
                            pdfPath = pdfFile.getAbsolutePath();
                        }
                    } else if (THSRDataHelper.getRemoteFile(context, fileUrl, fileName)) {//create cache
                        Log.v(TAG, String.format("create: %s", fileName));
                        try {//http://wzhiju.iteye.com/blog/1119037
                            pdfPath = moveToExternalCache(pdfFile, tmpFile);
                        } catch (IOException e) {
                            Log.e(TAG, "PDF: " + e.getMessage());
                            e.printStackTrace();
                            pdfPath = pdfFile.getAbsolutePath();
                        }
                        //load_gif(true, item.getName());
                    } //else {//use url
                    //    Log.v(TAG, String.format("url: %s", fileUrl));
                    //    //load_gif(false, item.getUrl());
                    //    THSRDataHelper.getRemoteFile(fileUrl);
                    //}
                    //mUiHandler.sendEmptyMessageDelayed(PAGE_TYPE_MAP, 10);
                    mUiHandler.sendMessage(mUiHandler.obtainMessage(PAGE_TYPE_PDF, pdfPath));
                }
            }).start();
        }
    };

    private void openPdf(String fileName) {
        File pdfFile = new File(fileName);
        if (pdfFile.exists()) {
            // File tmpFile = new File(getExternalCacheDir(), pdfFile.getName());
            // Log.d(TAG, tmpFile.getAbsolutePath());
            // try {
            //     //tmpFile = File.createTempFile("map", ".pdf", getExternalCacheDir());
            //
            //     //http://wzhiju.iteye.com/blog/1119037
            //     boolean r = tmpFile.createNewFile();
            //     Log.d(TAG, "createNewFile: " + r);
            //     if (r) {//not exist, copy to new place
            //         r = FileUtils.moveFile(pdfFile, tmpFile);
            //         Log.d(TAG, "moveFile: " + r);
            //     }//we should have the file there now
            //
            //     if (r) {
            Uri path = Uri.fromFile(pdfFile);
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (PackageUtils.isCallable(getBaseContext(), pdfIntent))
                startActivity(pdfIntent);
                //     }
                //  } catch (IOException e) {
                //     e.printStackTrace();
                //  }
            else {
                Toast.makeText(getBaseContext(), R.string.pdf_no_available,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String moveToExternalCache(File src, File dest) throws IOException {
        boolean r = dest.createNewFile();
        Log.d(TAG, "createNewFile: " + r);
        if (r) {//not exist, copy to new place
            r = FileUtils.moveFile(src, dest);
            Log.d(TAG, "moveFile: " + r);
            //we should have the file there now
            if (r)
                return dest.getAbsolutePath();
            else
                throw new IOException("moveFile failed");
        } else {//failed, try use cache file
            throw new IOException("createNewFile failed");
        }
    }

    private Button.OnClickListener onOpenMOTC = new Button.OnClickListener() {

        @Override
        public void onClick(View btn) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(
                    R.string.motc_url_base, mMotcIndex[mMapIndex[mCurrentStation]]))));
        }
    };

    private void prepare_load_gif() {
        mLoadingImage = true;
        //showMsgDialog(R.string.start_download_cache);//show loading dialog
        setSupportProgressBarIndeterminateVisibility(true);

        final Context context = getBaseContext();
        final TransferRoute item =
                mList.get(mCurrentStation).getRoutes().get(mCurrentRoute);

        new Thread(new Runnable() {

            @Override
            public void run() {
                if (mGDCache) {//[56]++
                    File f = new File(getExternalCacheDir(), item.getDstName());
                    if (f.exists())
                        Log.v(TAG, String.format("exist: %s", item.getDstName()));
                    else
                        MotcDataHelper.downloadFromGoogleDrive(context, item.getUrl(), f);
                } else {
                    if (context.getFileStreamPath(item.getName()).exists()) {
                        //read from cache
                        Log.v(TAG, String.format("exist: %s", item.getName()));
                        //load_gif(true, item.getName());
                    } else if (THSRDataHelper.getRemoteImage(context, item.getUrl(),
                            item.getName())) {//create cache
                        Log.v(TAG, String.format("create: %s", item.getName()));
                        //load_gif(true, item.getName());
                    } else {//use url
                        Log.v(TAG, String.format("url: %s", item.getUrl()));
                        //load_gif(false, item.getUrl());
                        THSRDataHelper.getRemoteImage(item.getUrl());
                    }
                }
                //mUiHandler.sendEmptyMessageDelayed(PAGE_TYPE_MAP, 10);
                mUiHandler.sendEmptyMessage(PAGE_TYPE_MAP);
            }
        }).start();
    }

    private void load_gif(boolean bIsCache, final String url)
            throws FileNotFoundException {
        mPageType = PAGE_TYPE_MAP;
        setSupportProgressBarIndeterminateVisibility(true);//[12]++

        setContentView(R.layout.empty_scrollview_layout2);
        //        //setContentView(R.layout.empty_linear_layout);
        //        mTextView1 = (TextView) findViewById(android.R.id.text1);
        //        mTextView1.setTextColor(Color.TRANSPARENT);
        //        mTextView2 = (TextView) findViewById(android.R.id.text2);
        //        mTextView2.setTextColor(Color.TRANSPARENT);

        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout01);
        layout.removeAllViews();

        final Context context = getBaseContext();
        final ImageView image = new ImageView(context);
        //http://blog.sephiroth.it/2011/04/04/imageview-zoom-and-scroll/
        //final ImageViewTouch image = new ImageViewTouch(context, null);
        //https://code.google.com/p/android-pinch/wiki/PinchImageView
        //final PinchImageView image = new PinchImageView(context);
        image.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        image.setScaleType(ImageView.ScaleType.FIT_XY);
        //File cache = new File(url);
        if (bIsCache/* && cache.exists() */) {//use data from cache
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            //try {//decode from cache
            //[12] add size detect http://stackoverflow.com/a/2528718
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            if(mGDCache) {//[56]++
                File f = new File(getExternalCacheDir(), url);
                Log.d(TAG, f.getAbsolutePath());
                BitmapFactory.decodeFile(f.getAbsolutePath(), opts);
            } else
                BitmapFactory.decodeStream(context.openFileInput(url), null, opts);
            //Log.d(TAG, "opts.outHeight = " + opts.outHeight);
            //Log.d(TAG, "opts.outWidth = " + opts.outWidth);
            int h = opts.outHeight, w = opts.outWidth;
            if (metrics.widthPixels > w * 2) {
                h *= 2;
                w *= 2;//not to enlarge too many times
            }
            {//else if (metrics.widthPixels > w) {
                h = Math.round((float) metrics.widthPixels / w * h);
                w = metrics.widthPixels;
            }// if w > metrics.widthPixels
            Log.v(TAG, "opts.outHeight = " + opts.outHeight + " ==> " + h);
            Log.v(TAG, "opts.outWidth = " + opts.outWidth + "==> " + w);
            image.setLayoutParams(new LinearLayout.LayoutParams(w, h));
            if(mGDCache) {//[56]++
                File f = new File(getExternalCacheDir(), url);
                image.setImageBitmap(BitmapFactory.decodeFile(f.getAbsolutePath()));
            } else
                image.setImageBitmap(BitmapFactory.decodeStream(context.openFileInput(url)));
            image.setBackgroundColor(Color.WHITE);//avoid transparent background
            //} catch (FileNotFoundException e) {
            //	Log.e(TAG, "file not found! " + e.getMessage());
            //	e.printStackTrace();
            //}
        } else {//download data from network
            //showMsgDialog(R.string.start_download_cache);//show loading dialog
            Log.w(TAG, "exception: " + url);
            //image.setImageBitmap(THSRDataHelper.getRemoteImage(url));
            TransferBusSA.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    image.setImageBitmap(THSRDataHelper.getRemoteImage(url));
                }

            });
        }
        layout.addView(image);

        //   //[0.7.0]++ add support to zoom
        //https://github.com/chrisbanes/PhotoView
        //   mAttacher = new PhotoViewAttacher(image);
        //   mAttacher.setMaxScale(2.0f);
        //   mAttacher.setOnMatrixChangeListener(new PhotoViewAttacher.OnMatrixChangedListener() {
        //
        //       @Override
        //       public void onMatrixChanged(RectF rect)
        //       {
        //           Log.d(TAG, String.format("w=%f, h=%f", rect.width(), rect.height()));
        //           //image.setLayoutParams(new LinearLayout.LayoutParams(
        //           //    LinearLayout.LayoutParams.MATCH_PARENT,
        //           //    LinearLayout.LayoutParams.WRAP_CONTENT));
        //       }
        //   });
        //   mAttacher.update();

        hideMsgDialog();
        mLoadingImage = false;
    }

    private View getOptionView(Context context, String text) {
        final int padding = getResources().getDimensionPixelSize(R.dimen.padding_medium);
        Button bt = new Button(context);
        bt.setText(text);
        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        getResources().getDimensionPixelSize(R.dimen.button_height));
        //params.bottomMargin = padding;
        params.topMargin = padding;
        //params.leftMargin = params.rightMargin = padding;
        bt.setLayoutParams(params);
        return bt;
    }

    private View getEmptyView(Context context) {
        final int textSize = getResources().getDimensionPixelSize(R.dimen.no_data_text_size);
        final int padding = getResources().getDimensionPixelSize(R.dimen.padding_large);
        TextView tv = new TextView(context);
        tv.setText(Html.fromHtml(getString(R.string.no_data)));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 0);
        //LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1.0f;
        tv.setLayoutParams(params);
        tv.setTextSize(textSize);
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setPadding(0, padding, 0, 0);
        return tv;
    }

    private Handler mUiHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PAGE_TYPE_ROUTE:
                    load_route(getBaseContext());
                    break;
                case PAGE_TYPE_MAP://directly load from cache
                    TransferBusSA.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            TransferRoute item =
                                    mList.get(mCurrentStation).getRoutes().get(mCurrentRoute);
                            try {
                                load_gif(true,
                                        mGDCache ? item.getDstName() : item.getName());
                            } catch (FileNotFoundException e) {
                                Log.e(TAG, "FileNotFoundException: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    });
                    break;
                case PAGE_TYPE_PDF://[16]++
                    //Log.v(TAG, mMapUrl[mMapIndex[mCurrentStation]]);
                {//start a new activity
                    //String name = mMapUrl[mMapIndex[mCurrentStation]];
                    //Log.d(TAG, getFileStreamPath(name).getAbsolutePath());
                    //openPdf(getFileStreamPath(name).getAbsolutePath());
                    Log.d(TAG, msg.obj.toString());
                    openPdf(msg.obj.toString());
                }
                hideMsgDialog();
                mLoadingImage = false;
                break;
                case PAGE_TYPE_STATION:
                default:
                    load_station(getBaseContext());
                    break;
            }
            super.handleMessage(msg);
        }

    };

    private View getSourceLinkView(Context context) {
        final int padding = getResources().getDimensionPixelSize(R.dimen.padding_small);
        TextView tv = new TextView(context);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.LEFT;
        tv.setLayoutParams(params);
        tv.setGravity(Gravity.LEFT);
        SpannableString content = new SpannableString(getString(R.string.title_thsr_transfer));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tv.setText(content);
        tv.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setData(Uri.parse(THSRDataHelper.TRANSFER_DATA_SOURCE));
                startActivity(i);
            }
        });
        tv.setPadding(padding, padding * 4, padding, padding * 8);
        tv.setTextColor(Color.parseColor("#3648ff"));
        //http://stackoverflow.com/a/6599782
        tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.new_tab, 0);
        return tv;
    }

    @SuppressWarnings("unused")
    private View getUpdateTimeView(Context context) {
        final int padding = getResources().getDimensionPixelSize(R.dimen.padding_small);
        TextView tv = new TextView(context);
        tv.setPadding(padding, 0, padding, 0);
        tv.setTextColor(Color.DKGRAY);//a small tip
        tv.setTextSize(10);
        //http://www.senab.co.uk/2013/01/15/snippet-dateutils/
        mTextView1.setText(getString(R.string.update_at, DateUtils.getRelativeTimeSpanString(
                THSRDataHelper.getUpdateTime(context).getTimeInMillis(), System.currentTimeMillis(),
                DateUtils.SECOND_IN_MILLIS)));
        return tv;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //        Log.v(TAG, "mPageType = " + mPageType);
        //        Log.v(TAG, "mCurrentStation = " + mCurrentStation);
        //        Log.v(TAG, "mCurrentRoute = " + mCurrentRoute);
        //[0.7.0]++ add support to screen rotation
        if (outState != null) {
            outState.putInt(EXTRA_PAGE_TYPE, mPageType);
            outState.putInt(EXTRA_STATION, mCurrentStation);
            outState.putInt(EXTRA_ROUTE, mCurrentRoute);
        }
        super.onSaveInstanceState(outState);
    }

    private void load_version(Context context) {
        mTextView1 = (TextView) findViewById(android.R.id.text1);
        if (THSRDataHelper.getUpdateTime(context).get(Calendar.YEAR) < 2013) {
            mTextView1.setVisibility(View.GONE);
        } else {
            mTextView1.setText(getString(R.string.update_at,
                    DateUtils.getRelativeTimeSpanString(
                            THSRDataHelper.getUpdateTime(context).getTimeInMillis(),
                            System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS)));
        }
        final PackageInfo pinfo =
                PackageUtils.getPackageInfo(getBaseContext(), TransferBusSA.class);
        //String version = String.format(Locale.US, "%s.%d", 
        //    pinfo.versionName, pinfo.versionCode);
        mTextView2 = (TextView) findViewById(android.R.id.text2);
        mTextView2.setText(String.valueOf(pinfo.versionName));
    }

    private View getSeparator(Context context) {
        View sep = new View(context);
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 1);
        p.leftMargin = p.rightMargin = 5;
        p.topMargin = 10;
        sep.setLayoutParams(p);
        sep.setBackgroundColor(Color.DKGRAY);
        return sep;
    }

    private void useNewVersion() {//[49]++
        SplashActivity.setUseOldVersion(this, false);

        Intent intent = new Intent();
        intent.setClass(this, DghMotcActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        //[57]++ ... // The rest of your onStart() code.
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //[57]++ ... // The rest of your onStop() code.
        EasyTracker.getInstance(this).activityStop(this);
    }
}
