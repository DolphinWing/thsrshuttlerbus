package dolphin.apps.ShuttlerBusTHSR;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRDataHelper;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferRoute;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferStation;

public class TransferBus extends Activity
{
	public static final String TAG = "TransferBus";

	private ArrayList<TransferStation> mList;

	private static final int PAGE_TYPE_UNKNOWN = 0;
	private static final int PAGE_TYPE_STATION = 1;
	private static final int PAGE_TYPE_ROUTE = 2;
	private static final int PAGE_TYPE_MAP = 3;
	//	private enum PAGE_TYPE {
	//		UNKNOWN, STATION, ROUTE, MAP
	//	};

	private int mPageType = PAGE_TYPE_UNKNOWN;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.empty_linear_layout);

		THSRDataHelper dataHelper = new THSRDataHelper(getBaseContext());
		mList = dataHelper.get_transfer_station();
		//Log.d(TAG, String.format("stations = %d", mList.size()));

		load_station();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		IntentFilter filter =
			new IntentFilter(THSRData.INTENT_DOWNLOAD_COMPLETE);
		registerReceiver(mReceiver, filter);
	}

	@Override
	protected void onPause()
	{
		unregisterReceiver(mReceiver);

		super.onPause();
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			Log.i(TAG, action);
			if (action.equalsIgnoreCase(THSRData.INTENT_DOWNLOAD_COMPLETE)) {
				if (intent.getBooleanExtra(THSRData.EXTRA_RESULT, false)) {
					Log.i(TAG, "update complete!");
					THSRDataHelper dataHelper =
						new THSRDataHelper(getBaseContext());
					mList = dataHelper.get_transfer_station();
					//Log.d(TAG, String.format("stations = %d", mList.size()));
				}
				else {
					mList = null;
				}

				load_station();
			}
		}
	};

	@Override
	public void onBackPressed()
	{
		switch (mPageType) {
			case PAGE_TYPE_ROUTE:
				load_station();
				break;
			case PAGE_TYPE_MAP:
				load_route();
				break;
			case PAGE_TYPE_STATION:
			default:
				super.onBackPressed();
				break;
		}
	}

	private void load_station()
	{
		mPageType = PAGE_TYPE_STATION;

		LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout01);
		layout.removeAllViews();
		if (mList == null || mList.size() <= 0) {//no data
			TextView tv = new TextView(getBaseContext());
			tv.setText(R.string.no_data);
			layout.addView(tv);
		}
		else
			for (int i = 0; i < mList.size(); i++) {
				Button bt = new Button(getBaseContext());
				bt.setText(mList.get(i).getName());
				bt.setTag(i);
				bt.setOnClickListener(onOpenStation);
				layout.addView(bt);
			}
	}

	private int mCurrentStation = -1;

	private Button.OnClickListener onOpenStation =
		new Button.OnClickListener() {

			@Override
			public void onClick(View btn)
			{
				mCurrentStation = Integer.parseInt(btn.getTag().toString());
				//Log.d(TAG, String.format("btn %s", currentStation));

				//TransferStation item = mList.get(mCurrentStation);
				//Log.d(TAG, String.format("%s", item.getName()));

				load_route();
			}
		};

	private void load_route()
	{
		mPageType = PAGE_TYPE_ROUTE;

		LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout01);
		layout.removeAllViews();

		ArrayList<TransferRoute> routes =
			mList.get(mCurrentStation).getRoutes();
		if (routes == null || routes.size() <= 0) {//no data
			TextView tv = new TextView(getBaseContext());
			tv.setText(R.string.no_data);
			layout.addView(tv);
		}
		else
			for (int i = 0; i < routes.size(); i++) {
				Button bt = new Button(getBaseContext());
				bt.setText(routes.get(i).getTitle());
				bt.setTag(i);
				bt.setOnClickListener(onOpenRoute);
				layout.addView(bt);
			}
	}

	private int mCurrentRoute = -1;
	private Button.OnClickListener onOpenRoute =
		new Button.OnClickListener() {

			@Override
			public void onClick(View btn)
			{
				mCurrentRoute = Integer.parseInt(btn.getTag().toString());
				//Log.d(TAG, String.format("btn %s", mCurrentRoute));

				TransferRoute item =
					mList.get(mCurrentStation).getRoutes().get(mCurrentRoute);

				if (getBaseContext().getFileStreamPath(item.getName()).exists()) {
					//read from cache
					Log.v(TAG, String.format("exist: %s", item.getName()));
					load_gif(true, item.getName());
				}
				else if (THSRDataHelper.getRemoteImage(getBaseContext(), item
						.getUrl(), item.getName())) {//create cache
					Log.v(TAG, String.format("create: %s", item.getName()));
					load_gif(true, item.getName());
				}
				else {//use url
					Log.v(TAG, String.format("%s", item.getUrl()));
					load_gif(false, item.getUrl());
				}
			}
		};

	private void load_gif(boolean bIsCache, String url)
	{
		mPageType = PAGE_TYPE_MAP;

		LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout01);
		layout.removeAllViews();

		ImageView image = new ImageView(getBaseContext());
		image.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		image.setScaleType(ImageView.ScaleType.FIT_XY);
		if (bIsCache) {//use data from cache
			try {//decode from cache
				image.setImageBitmap(BitmapFactory
						.decodeStream(getBaseContext().openFileInput(url)));
			}
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {//download data from network
			image.setImageBitmap(THSRDataHelper.getRemoteImage(url));
		}
		layout.addView(image);
	}
}
