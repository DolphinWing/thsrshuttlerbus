package dolphin.apps.ShuttlerBusTHSR.motc;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import dolphin.android.app.ABSFragment;
import dolphin.android.net.HttpHelper;
import dolphin.android.util.FileUtils;
import dolphin.android.util.PackageUtils;
import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcData;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcDataHelper;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.ReviewItem;
import rnurik.android.wizardpager.ui.PageFragmentCallbacks;
import rnurik.android.wizardpager.ui.ReviewFragment;

/**
 * Created by dolphin on 2013/8/2.
 */
public class RouteViewFragment extends ABSFragment implements ModelCallbacks, MotcDataHelper.MotcDataListener {
    private static final String TAG = "RouteViewFragment";
    private static final String ARG_KEY = "key";

    private PageFragmentCallbacks mCallbacks;
    private RouteViewFragment.Callbacks mRouteCallbacks;
    private String mKey;
    private Page mPage = null;

    private AbstractWizardModel mWizardModel;
    //private ArrayList<ReviewItem> reviewItems;
    private HashMap<String, String> mReviewItems;

    private TextView mTitle;
    private ExpandableListView mListView;
    private View mProgress;
    private View mMapButton;
    private ImageButton mImageMapButton;

    private MotcDataHelper mHelper;
    private MotcData motcData = null;
    private String[] mRouteMapId;
    private String[] mRouteMapName;

    private String mRouteJson = null;
    private String mRouteKey = null;

    private boolean mNavigationAutoExpand = false;
    private boolean mRouteMapAsImage = false;
    private int mAvailableMapWidth = 0;
    private int mAvailableMapHeight = 0;
    private boolean mIsPortrait = false;//[51]++

    public static RouteViewFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        RouteViewFragment fragment = new RouteViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static class RouteViewPage extends Page {
        protected RouteViewPage(ModelCallbacks callbacks, String title) {
            super(callbacks, title);
        }

        @Override
        public Fragment createFragment() {
            return RouteViewFragment.create(getKey());
        }

        @Override
        public void getReviewItems(ArrayList<ReviewItem> dest) {
            //dest.add(new ReviewItem(getTitle(),
            //        mData.getString(EXTRA_ITEM_TITLE), getKey()));
        }

        public RouteViewPage setItemTitle(String title) {
            //mData.putString(EXTRA_ITEM_TITLE, title);
            return this;
        }

        @Override
        public boolean isCompleted() {
            //return super.isCompleted();
            return true;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Resources res = getActivity().getResources();

        Bundle args = getArguments();
        if (args != null) {
            mKey = args.containsKey(ARG_KEY) ? args.getString(ARG_KEY) : null;
            mPage = mCallbacks.onGetPage(mKey);
            mRouteJson = args.containsKey(ThsrStationModel.KEY_FAVOR_DATA)
                    ? args.getString(ThsrStationModel.KEY_FAVOR_DATA) : null;
            mRouteKey = args.containsKey(ThsrStationModel.KEY_FAVOR_KEY)
                    ? args.getString(ThsrStationModel.KEY_FAVOR_KEY) : null;
        }

        int seq = -1;
        if (mKey != null) {//Navigation
            seq = mHelper.getStationSeq(mReviewItems.get(ThsrStationModel.KEY_STATION));
            //Log.d(TAG, "station seq = " + seq);
        } else if (mRouteKey != null) {
            //Log.d(TAG, "onCreate favorite");
            seq = Integer.parseInt(mRouteKey.split("-")[0]) - 1;
        }
        if (seq >= 0) {//in the array
            mRouteMapId = res.getStringArray(MotcDataHelper.getRouteMapIdArrayId(seq));
            mRouteMapName = res.getStringArray(MotcDataHelper.getRouteMapNameArrayId(seq));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_review_page, container, false);
        //((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
        mTitle = (TextView) rootView.findViewById(android.R.id.title);
        //add_debug_buttons(rootView);
        mMapButton = rootView.findViewById(R.id.buttonMap);
        if (mMapButton != null)
            mMapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //[50]-- only for download
                    //if (mRouteMapAsImage)//for large screen
                    //    onMapImageClick();
                    //else //for phone
                    onMapButtonClick();
                }
            });
        //[50]++ separate the image view button and download button
        mImageMapButton = (ImageButton) rootView.findViewById(android.R.id.button3);
        if (mImageMapButton != null)
            mImageMapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onMapImageClick();
                }
            });

        mListView = (ExpandableListView) rootView.findViewById(android.R.id.list);
//        mMapButton = rootView.findViewById(android.R.id.button3);
        mProgress = rootView.findViewById(android.R.id.progress);
//        if (mRouteCallbacks!= null)
//            motcData = mRouteCallbacks.getMotcData();//try to get from Activity
//        int pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
//        if (motcData != null && pos >= 0 && mListView != null) {
//            Log.d(TAG, "onCreateView " + pos);
//            mListView.setAdapter(new RouteExpandableListAdapter(getActivity(),
//                    motcData.getRoute(pos)));
//            mProgress.setVisibility(View.GONE);
//        }
        final Resources res = getActivity().getResources();
        mNavigationAutoExpand = res.getBoolean(R.bool.navigation_auto_expand);
        mRouteMapAsImage = res.getBoolean(R.bool.route_map_as_button);
        //Log.d(TAG, "mRouteMapAsImage = " + mRouteMapAsImage);
        if (mRouteMapAsImage && mImageMapButton != null)
            mImageMapButton.setImageResource(android.R.color.transparent);
        mIsPortrait = res.getBoolean(R.bool.portrait_mode);//[51]++
        Log.d(TAG, "mIsPortrait: " + mIsPortrait);
        return rootView;
    }

    void add_debug_buttons(View rootView) {
        final TextView mTimetable = (TextView) rootView.findViewById(android.R.id.text2);
        rootView.findViewById(android.R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String station = mReviewItems.get(ThsrStationModel.KEY_STATION);
                Log.d(TAG, "android.R.id.button1: " + station);
                MotcDataHelper helper = new MotcDataHelper(getActivity());
                MotcData motcData = helper.get_motc_html(helper.getMotcIndex(station));
                if (motcData != null) {
                    Toast.makeText(getActivity(), motcData.getUpdateTime(),
                            Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "routes: " + motcData.getRoutes().size());
                    MotcData.Route route =
                            motcData.findRoute(mReviewItems.get(ThsrStationModel.KEY_ROUTE));
                    if (route != null) {
                        //Log.d(TAG, "  " + route.getName());
                        Log.d(TAG, "  " + route.getSchedules().size());
                        mTimetable.setText(route.getName());
                    }

                    File f = new File(getActivity().getExternalCacheDir(), "test.json");
                    String json = MotcData.toJson(motcData);
                    Log.d(TAG, "string length: " + json.length());
                    FileUtils.writeStringToFile(f, json);
                }
            }
        });
        rootView.findViewById(android.R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File f = new File(getActivity().getExternalCacheDir(), "test.json");
                String json = FileUtils.readFileToString(f);
                Log.d(TAG, "string length: " + json.length());
                MotcData motcData = MotcData.jsonToMotcData(json);
                if (motcData != null) {
                    Toast.makeText(getActivity(), motcData.getUpdateTime(),
                            Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "routes: " + motcData.getRoutes().size());
                    MotcData.Route route =
                            motcData.findRoute(mReviewItems.get(ThsrStationModel.KEY_ROUTE));
                    if (route != null) {
                        //Log.d(TAG, "  " + route.getName());
                        Log.d(TAG, "  schedules = " + route.getSchedules().size());
                        mTimetable.setText(route.getName());
                    }
                }
            }
        });
    }

    private void onMapButtonClick() {
        int pos = -1;
        if (mKey != null) {//Navigation
            pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
        } else if (mRouteKey != null) {//Favorite
            pos = Integer.parseInt(mRouteKey.split("-")[1]) - 1;
        }

        if (pos >= 0) {
            File f = getMapFile(pos);
            if (!f.exists())
                downloadMapFromGoogleDrive(mRouteMapId[pos], f);
            else
                startActivityForMap(f);
        }
    }

    private void onMapImageClick() {
//        int pos = -1;
//        if (mKey != null) {//Navigation
//            pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
//        } else if (mRouteKey != null) {//Favorite
//            pos = Integer.parseInt(mRouteKey.split("-")[1]) - 1;
//        }
//        if (pos >= 0) {
        File f = getMapFile();
        if (f != null && f.exists())
            startActivityForMap(f);
//        }
    }

    private File getMapFile() {
        int pos = -1;
        if (mKey != null) {//Navigation
            pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
        } else if (mRouteKey != null) {//Favorite
            pos = Integer.parseInt(mRouteKey.split("-")[1]) - 1;
        }
        return getMapFile(pos);
    }

    private File getMapFile(int pos) {
        if (pos >= 0 && pos < mRouteMapName.length) {
            return new File(getActivity().getExternalCacheDir(), mRouteMapName[pos]);
        }
        return null;
    }

    private void downloadMapFromGoogleDrive(String id, final File dst) {
        if (!HttpHelper.checkNetworkAvailable(getActivity())) {//[46]++ check network
            Log.e(TAG, "no network to download map file");
            return;
        }
        //http://stackoverflow.com/a/11855448
        //https://drive.google.com/uc?export=download&id={fileId}
//        File f = new File(getActivity().getExternalCacheDir(), "test.jpg");
//        Log.d(TAG, f.getAbsolutePath());
        if (mMapButton != null)
            mMapButton.setEnabled(false);
        if (mImageMapButton != null)
            mImageMapButton.setEnabled(false);
        //0B-oMP4622t0hWk1DZmdCVkR4VFk
        //final String url = "https://drive.google.com/uc?export=download&id=" + id;
        final String driveID = id;
        //Log.d(TAG, url);
        //Log.d(TAG, dst.getAbsolutePath());
        //[53]--
        //final ProgressDialog dialog = ProgressDialog.show(getActivity(),
        //        null, getString(R.string.wait_download_process));
        //dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //dialog.setCancelable(false);
        //dialog.setCanceledOnTouchOutside(false);
        //dialog.show();
        show_loading(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                //final boolean r = HttpHelper.getRemoteFile(getActivity(),
                //        url, dst.getAbsolutePath());
                final boolean r = MotcDataHelper.downloadFromGoogleDrive(getActivity(),
                        driveID, dst);
                Log.v(TAG, "download " + (r ? "success" : "failed"));

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //                dialog.dismiss();//[51]++ do in UI thread
                        if (r) {//download success
                            //[50]-- always decode
                            //if (mRouteMapAsImage) {//decode as possible
                            decodeMapFile(dst.getAbsolutePath());
                            //} else
                            //if (!mRouteMapAsImage) //[50]-- don't open directly
                            //    startActivityForMap(dst);
                            if (mMapButton != null && mImageMapButton != null) {//[50]++
                                mMapButton.setVisibility(View.GONE);
                                mImageMapButton.setVisibility(View.VISIBLE);
                            }
                        } else
                            Toast.makeText(getActivity(),
                                    R.string.download_failed,
                                    Toast.LENGTH_SHORT).show();

                        if (mMapButton != null)
                            mMapButton.setEnabled(true);
                        if (mImageMapButton != null)
                            mImageMapButton.setEnabled(true);
                        show_loading(false);
                    }
                });

            }
        }).start();
    }

    private void show_loading(boolean bShown) {
        getSherlockActivity().setSupportProgressBarIndeterminateVisibility(bShown);
    }

    private void startActivityForMap(File dst) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(dst), "image/*");
        if (PackageUtils.isCallable(getActivity(), intent))
            startActivity(intent);
    }

    private void decodeMapFile(String file) {
        //decode as possible
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;
        if (mRouteMapAsImage && !mIsPortrait) {//width first for tablet
            float width = options.outWidth;
            //Log.d(TAG, "width = " + width);
            while (width > mAvailableMapWidth) {
                width /= 2;
                options.inSampleSize *= 2;
            }
        } else {//[50]++ for phone, check height
            float height = options.outHeight;
            //Log.d(TAG, "width = " + width);
            while (height > mAvailableMapHeight) {
                height /= 2;
                options.inSampleSize *= 2;
            }
        }
        //Log.v(TAG, "options.inSampleSize " + options.inSampleSize);
        Bitmap bmp = BitmapFactory.decodeFile(file, options);
        if (mImageMapButton != null)
            mImageMapButton.setImageBitmap(bmp);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException(
                    "Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;

        if (!(activity instanceof ReviewFragment.Callbacks)) {
            throw new ClassCastException(
                    "Activity must implement ReviewFragment.Callbacks");
        }

        mReviewItems = new HashMap<String, String>();

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        //Log.d(TAG, "widthPixels = " + dm.widthPixels);
        //Log.d(TAG, "heightPixels = " + dm.heightPixels);
        //Log.d(TAG, "density = " + dm.density);
        int padding = getActivity().getResources().getDimensionPixelSize(R.dimen.layout_padding);
        //Log.d(TAG, "layout_padding = " + padding);
        //[50]-- mAvailableMapWidth = (int) (dm.widthPixels / 2 - padding);
        mAvailableMapWidth = (dm.widthPixels - padding * 2);//[50]++
        //Log.d(TAG, "mAvailableMapWidth = " + mAvailableMapWidth);
        mAvailableMapHeight = (int) (dm.heightPixels / 2);//[50]++
        //Log.d(TAG, "mAvailableMapHeight = " + mAvailableMapHeight);

        mRouteCallbacks = (RouteViewFragment.Callbacks) activity;
        mWizardModel = mRouteCallbacks.onGetModel();
        mWizardModel.registerListener(this);
        mHelper = mRouteCallbacks.getMotcDataHelper();
        mHelper.registerDataListener(this);
        motcData = mRouteCallbacks.getMotcData();//try to get from Activity at onAttach
        onPageTreeChanged();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mWizardModel.unregisterListener(this);
        mRouteCallbacks = null;
        mHelper.unregisterDataListener(this);

        mReviewItems.clear();
        mReviewItems = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        onPageTreeChanged();
    }

    @Override
    public void onPageDataChanged(Page changedPage) {
        //if (mRouteCallbacks != null)
        //    motcData = mRouteCallbacks.getMotcData();//try to get from Activity
        mReviewItems.clear();
        ArrayList<ReviewItem> reviewItems = new ArrayList<ReviewItem>();
        for (Page page : mWizardModel.getCurrentPageSequence()) {
            page.getReviewItems(reviewItems);
        }
        for (ReviewItem item : reviewItems) {
            String str = item.getDisplayValue();
            mReviewItems.put(item.getPageKey(), str);
            //Log.d(TAG, item.getPageKey() + ": " + str);
            if (item.getPageKey().equalsIgnoreCase(ThsrStationModel.KEY_ROUTE)
                    && mTitle != null && str != null) {
                mTitle.setText(str);
                //mReviewItems.put("TITLE", str);
            }
        }

        if (mKey != null) {//Navigation
            int pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
            //Log.d(TAG, "onPageDataChanged " + pos);
            if (motcData != null && pos >= 0 && pos < motcData.getRoutes().size()) {
                MotcData.Route route = motcData.getRoute(pos);
                updateRouteView(pos, route, mNavigationAutoExpand);
            } else {
                if (mTitle != null)
                    mTitle.setText("");
                if (mMapButton != null)
                    mMapButton.setEnabled(false);
            }
        } else if (mRouteKey != null) {//Favorite
            //Log.d(TAG, "onPageDataChanged favorite " + mRouteJson);
            //motcData = new MotcData();
            final int pos = Integer.parseInt(mRouteKey.split("-")[1]) - 1;
            if (mImageMapButton != null && mRouteMapAsImage)
                mImageMapButton.setImageResource(android.R.color.transparent);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final MotcData.Route route = MotcData.jsonToRoute(mRouteJson);
                    //motcData.addRoute(MotcData.jsonToRoute(mRouteJson));
                    final String title = MotcDataHelper.formatTitle(route.getName());
                    //Log.d(TAG, route.getName());

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateRouteView(pos, route, true);
                            if (mTitle != null)
                                mTitle.setText(title);
                        }
                    });
                }
            }).start();
        }
    }

    private void updateRouteView(int pos, MotcData.Route route, boolean expand) {
        if (mListView != null) {
            mListView.setAdapter(new RouteExpandableListAdapter(getActivity(), route));
            if (expand)
                for (int i = 0; i < route.getSchedules().size(); i++)
                    mListView.expandGroup(i);
        }
        if (mProgress != null)
            mProgress.setVisibility(View.GONE);
        if (mMapButton != null) {
            mMapButton.setEnabled(true);
        }
        File f = getMapFile(pos);
        if (mRouteMapAsImage) {
            if (!f.exists())
                downloadMapFromGoogleDrive(mRouteMapId[pos], f);
            else
                decodeMapFile(f.getAbsolutePath());
        } else {//if already downloaded, show in the screen
            if (f != null && f.exists()) {
                decodeMapFile(f.getAbsolutePath());
                if (mMapButton != null && mImageMapButton != null) {
                    mMapButton.setVisibility(View.GONE);
                    mImageMapButton.setVisibility(View.VISIBLE);
                }
            } else {//no file, need to download
                if (mMapButton != null)
                    mMapButton.setVisibility(View.VISIBLE);
                if (mImageMapButton != null)
                    mImageMapButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onPageTreeChanged() {
        onPageDataChanged(null);
    }

    public interface Callbacks extends ReviewFragment.Callbacks {
        public MotcDataHelper getMotcDataHelper();

        public MotcData getMotcData();
    }

    @Override
    public void onDownloadStart(int motcIndex) {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDownloadComplete(int motcIndex, MotcData data) {
        int pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
        //Log.d(TAG, "onDownloadComplete " + motcIndex + " " + pos);
        motcData = data;
        onPageTreeChanged();//update the data
    }

    @Override
    public void onError() {
        mProgress.setVisibility(View.GONE);
    }
}
