package dolphin.apps.ShuttlerBusTHSR.motc;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.Calendar;

import dolphin.android.widget.AbsListViewHelper;
import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcData;

/**
 * Created by dolphin on 2013/8/5.
 */
public class RouteExpandableListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private MotcData.Route mRoute;
    private LayoutInflater mInflater;
    private float mDensity;

    public RouteExpandableListAdapter(Context context, MotcData.Route group) {
        mContext = context;
        mRoute = group;
        mInflater = LayoutInflater.from(mContext);
        mDensity = mContext.getResources().getDisplayMetrics().density;
    }

    @Override
    public int getGroupCount() {
        return mRoute.getSchedules().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;//mRoute.getSchedule(groupPosition).getLeaveTime().size();
    }

    @Override
    public MotcData.Schedule getGroup(int groupPosition) {
        return mRoute.getSchedule(groupPosition);
    }

    @Override
    public Calendar getChild(int groupPosition, int childPosition) {
        return mRoute.getSchedule(groupPosition).getLeaveTime().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        MotcData.Schedule schedule = getGroup(groupPosition);
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.listview_route_item, null, false);
        TextView text = (TextView) convertView.findViewById(android.R.id.text1);
        text.setText(schedule.getTerminal());
        text.setPadding((int) (mDensity * 32), 0, 0, 0);
        text = (TextView) convertView.findViewById(android.R.id.text2);
        text.setVisibility(View.GONE);
        convertView.findViewById(android.R.id.checkbox).setVisibility(View.GONE);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild,
                             View convertView, ViewGroup parent) {
        //Log.d("dolphin", "groupPosition: " + groupPosition);
        //Log.d("dolphin", "childPosition: " + childPosition);
        MotcData.Schedule schedule = getGroup(groupPosition);
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.listview_schedule_item, null, false);

        ExpandableListChildView grid =
                (ExpandableListChildView) convertView.findViewById(android.R.id.list);
        int ts = schedule.getLeaveTime().size();
        if (ts > 0) {
            String[] time = new String[schedule.getLeaveTime().size()];
            int selection = -1;
            for (int i = 0; i < ts; i++) {
                Calendar c = schedule.getLeaveTime().get(i);
                time[i] = String.format("%02d:%02d", c.get(Calendar.HOUR_OF_DAY),
                        c.get(Calendar.MINUTE));
                if (c.after(Calendar.getInstance()) && selection < 0) {
                    selection = i;
                    //time[i] = "* " + time[i];
                }
                if (schedule.getLeaveTimeExtra(i) != null)
                    time[i] = schedule.getLeaveTimeExtra(i);
//                if (schedule.isLeaveTimeRed(i))
//                    time[i] = "<span style=\"color: #ff0000\">" + time[i] + "</span>";
            }
            //Log.d("dolphin", "time = " + time[selection]);
            //grid.setItemChecked(selection, true);
            ExpandableListChildView.ChildAdapter adapter = new ExpandableListChildView.ChildAdapter(mContext,
                    android.R.layout.simple_list_item_1, time);
            if (selection > -1) {
                //View v = grid.getChildAt(selection);
                //if (v != null)
                //    v.setBackgroundResource(R.drawable.abs__list_focused_holo);
                adapter.setSelection(selection);
            } else {
                adapter.setSelection(ts + 10);
            }

            adapter.setColorMap(schedule.getLeaveTimeRed());
            if (schedule.hasLeaveTimeExtra()) {
                grid.setColumnWidth((int) (140 * mDensity));
            }
            grid.setAdapter(adapter);

        } else
            grid.setVisibility(View.GONE);

        TextView text = (TextView) convertView.findViewById(android.R.id.text1);
        if (schedule.getExtras() != null) {
            //Log.d("dolphin", "getExtras() len: " + schedule.getExtras().length());
            text.setText(Html.fromHtml(schedule.getExtras()));
        } else
            text.setVisibility(View.GONE);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
