package dolphin.apps.ShuttlerBusTHSR.motc;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import dolphin.android.libs.R;
import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.SingleFixedChoicePage;
import rnurik.android.wizardpager.ui.SingleChoiceFragment;

/**
 * Created by 97011424 on 2013/8/2.
 */
public class SingleChoiceTwoLinesFragment extends SingleChoiceFragment {
    private static final String ARG_KEY = "key";

    public static SingleChoiceTwoLinesFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        SingleChoiceTwoLinesFragment fragment = new SingleChoiceTwoLinesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public SingleChoiceTwoLinesFragment() {
        super();
    }

    public static class MyPage extends SingleFixedChoicePage {
        private String mKey;

        public MyPage(ModelCallbacks callbacks, String title) {
            super(callbacks, title);
        }

        public MyPage(ModelCallbacks callbacks, String title, String key) {
            super(callbacks, title);
            mKey = key;
        }

        @Override
        public String getKey() {
            return mKey != null ? mKey : super.getKey();
        }

        @Override
        public Fragment createFragment() {
            return SingleChoiceTwoLinesFragment.create(getKey());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wizard_listview_page, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(getPage().getTitle());

        final ListView listView = (ListView) rootView.findViewById(android.R.id.list);

        //http://blog.changyy.org/2012/05/android-listview-listactivity.html
        final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
        ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();
//        String[] titles = new String[]{ "Title1" , "Title2", "Title3" };
//        String[] subtitles = new String[]{ "SubTitle1" , "SubTitle2", "SubTitle3" };

        for (int i = 0; i < getChoices().size(); ++i) {
            String choice = getChoices().get(i);
            HashMap<String, String> item = new HashMap<String, String>();
            int s = choice.contains("】") ? choice.indexOf("】") + 1 : 0;
            int c = choice.contains("(") ? choice.indexOf("(") : choice.length();
            item.put(ID_TITLE, choice.substring(s, c));
            item.put(ID_SUBTITLE, choice.substring(0, s) + choice.substring(c));
            myListData.add(item);
        }

        setListAdapter(new SimpleAdapter(
                getActivity(),
                myListData,
                android.R.layout.simple_list_item_2,
                new String[]{ID_TITLE, ID_SUBTITLE},
                new int[]{android.R.id.text1, android.R.id.text2})
        );
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setSelector(R.drawable.abs__list_focused_holo);

        // Pre-select currently selected item.
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                String selection = getPage().getData().getString(Page.SIMPLE_DATA_KEY);
                for (int i = 0; i < getChoices().size(); i++) {
                    if (getChoices().get(i).equals(selection)) {
                        listView.setItemChecked(i, true);
                        break;
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.d("dolphin", "onListItemClick: " + position);
        getPage().getData().putString(Page.SIMPLE_DATA_KEY,
                getListAdapter().getItem(position).toString());
        getPage().notifyDataChanged();
    }
}
