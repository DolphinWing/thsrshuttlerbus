package dolphin.apps.ShuttlerBusTHSR.motc;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dolphin.android.widget.AbsListViewHelper;
import dolphin.apps.ShuttlerBusTHSR.R;
import rnurik.android.wizardpager.model.BranchPage;
import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.ReviewItem;

/**
 * Created by 97011424 on 2013/8/5.
 */
public class ThsrRouteChoiceFragment extends ThsrChoiceFragment {
    private final static String TAG = "ThsrRouteChoiceFragment";
    private static final String ARG_KEY = "key";
    private final String ID_TITLE = "TITLE";
    private final String ID_SUBTITLE = "SUBTITLE";
    ArrayList<HashMap<String, String>> myListData;


    public static ThsrRouteChoiceFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        ThsrRouteChoiceFragment fragment = new ThsrRouteChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ThsrRouteChoiceFragment() {
        super();
    }

    public static class MyPage extends ThsrChoiceFragment.MyPage {
        public MyPage(ModelCallbacks callbacks, String title) {
            super(callbacks, title);
        }

        @Override
        public Fragment createFragment() {
            return ThsrRouteChoiceFragment.create(getKey());
        }

        @Override
        public void getReviewItems(ArrayList<ReviewItem> dest) {
            //super.getReviewItems(dest);

            int pos = mData.getInt(ThsrStationModel.KEY_ROUTE_POS, -1);
            //Log.d(TAG, "getReviewItems pos = " + pos);
            dest.add(new ReviewItem(getTitle(),
                    Integer.toString(pos), ThsrStationModel.KEY_ROUTE_POS));
            dest.add(new ReviewItem(getTitle(),
                    mData.getString(SIMPLE_DATA_KEY), ThsrStationModel.KEY_ROUTE));
        }

        public MyPage setPosition(int selection) {
            mData.putInt(ThsrStationModel.KEY_ROUTE_POS, selection);
            return this;
        }

        @Override
        public boolean isCompleted() {
            //return super.isCompleted();
            int pos = mData.getInt(ThsrStationModel.KEY_ROUTE_POS, -1);
            return (pos > -1);
        }
    }

    public static class MyBranchPage extends BranchPage {
        public MyBranchPage(ModelCallbacks callbacks, String title) {
            super(callbacks, title);
        }

        @Override
        public void getReviewItems(ArrayList<ReviewItem> dest) {
            dest.add(new ReviewItem(getTitle(), mData.getString(SIMPLE_DATA_KEY),
                    ThsrStationModel.KEY_STATION));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wizard_listview_page_with_loading,
                container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(getPage().getTitle());

        final ListView listView = (ListView) rootView.findViewById(android.R.id.list);
        final AbsListViewHelper listViewHelper = new AbsListViewHelper(listView);
        listViewHelper.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        final View progress = rootView.findViewById(android.R.id.progress);

//        // Pre-select currently selected item.
//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
//                int selection = getPage().getData().getInt(ThsrStationModel.KEY_ROUTE_POS);
//                listViewHelper.setItemChecked(selection, true);
//            }
//        });
        //[56]-- remove thread
        //new Thread(new Runnable() {
        //    @Override
        //    public void run() {
        myListData = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < getChoices().size(); ++i) {
            String choice = getChoices().get(i);
            HashMap<String, String> item = new HashMap<String, String>();
            int s = choice.contains("】") ? choice.indexOf("】") + 1 : 0;
            int c = choice.contains("(") ? choice.indexOf("(") : choice.length();
            item.put(ID_TITLE, choice.substring(s, c));
            item.put(ID_SUBTITLE, choice.substring(0, s) + choice.substring(c));
            myListData.add(item);
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listViewHelper.setAdapter(new RouteAdapter(getActivity(), myListData));

                int selection = getPage().getData().getInt(ThsrStationModel.KEY_ROUTE_POS, -1);
                //Log.d(TAG, "KEY_ROUTE_POS = " + selection);
                if (selection > -1 && selection < listViewHelper.getCount()) {
                    listViewHelper.setItemChecked(selection, true);
                }

                progress.setVisibility(View.GONE);
            }
        });
        //    }
        //}).start();

        return rootView;
    }

    private class RouteAdapter extends ArrayAdapter<HashMap<String, String>> {
        private Context mContext;

        public RouteAdapter(Context context, List<HashMap<String, String>> objects) {
            super(context, android.R.id.text1, objects);
            mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = null;
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.listview_route_item, null);

            HashMap<String, String> item = getItem(position);
            TextView tv1 = (TextView) v.findViewById(android.R.id.text1);
            tv1.setText(item.get(ID_TITLE));
            TextView tv2 = (TextView) v.findViewById(android.R.id.text2);
            tv2.setText(item.get(ID_SUBTITLE));
            if (getPage().getData().getInt(ThsrStationModel.KEY_ROUTE_POS, -1) == position) {
                //Log.d("dolphin", "ThsrStationModel.KEY_ROUTE_POS = " + position);
                RadioButton checkbox = (RadioButton) v.findViewById(android.R.id.checkbox);
                checkbox.setChecked(true);
            }
            return v;
        }

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //super.onListItemClick(l, v, position, id);
        Bundle bundle = getPage().getData();
        int lastPos = bundle.getInt(ThsrStationModel.KEY_ROUTE_POS, -1);
        //Log.d(TAG, "KEY_ROUTE_POS = " + lastPos + ", position = " + position);
        View checkbox;
        if (lastPos > -1) {
            int fp = getListView().getFirstVisiblePosition();
            View box = getListView().getChildAt(lastPos - fp);
            if (box != null) {
                checkbox = box.findViewById(android.R.id.checkbox);
                if (checkbox != null && checkbox instanceof RadioButton) {
                    ((RadioButton) checkbox).setChecked(false);
                }
            }
        }
        checkbox = v.findViewById(android.R.id.checkbox);
        if (checkbox != null && checkbox instanceof RadioButton) {
            ((RadioButton) checkbox).setChecked(true);
        }
        bundle.putString(Page.SIMPLE_DATA_KEY, getChoices().get(position));
        //        myListData.get(position).get(ID_TITLE));
        bundle.putInt(ThsrStationModel.KEY_ROUTE_POS, position);
        getPage().notifyDataChanged();
    }

}
