package dolphin.apps.ShuttlerBusTHSR.motc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.ReviewItem;
import rnurik.android.wizardpager.model.SingleFixedChoicePage;
import rnurik.android.wizardpager.ui.SingleChoiceFragment;

/**
 * Created by 97011424 on 2013/8/5.
 */
public class ThsrChoiceFragment extends SingleChoiceFragment {
    private static final String ARG_KEY = "key";

    public final static String POS_DATA_KEY = "_pos";

    public static ThsrChoiceFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        ThsrChoiceFragment fragment = new ThsrChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ThsrChoiceFragment() {
        super();
    }

    public static class MyPage extends SingleFixedChoicePage {
        public MyPage(ModelCallbacks callbacks, String title) {
            super(callbacks, title);
        }

        @Override
        public Fragment createFragment() {
            return ThsrChoiceFragment.create(getKey());
        }

        @Override
        public void getReviewItems(ArrayList<ReviewItem> dest) {
            super.getReviewItems(dest);

            int pos = mData.getInt(POS_DATA_KEY);
            dest.add(new ReviewItem(POS_DATA_KEY, String.valueOf(pos), getKey()));
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //Log.d("dolphin", "onListItemClick: " + position);
        getPage().getData().putString(Page.SIMPLE_DATA_KEY,
                getListAdapter().getItem(position).toString());
        getPage().getData().putInt(POS_DATA_KEY, position);
        getPage().notifyDataChanged();
    }
}
