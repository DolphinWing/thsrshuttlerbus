package dolphin.apps.ShuttlerBusTHSR.motc;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;

import java.io.File;
import java.util.List;

import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.SplashActivity;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.WizardPagerAdapter;
import rnurik.android.wizardpager.ui.PageFragmentCallbacks;
import rnurik.android.wizardpager.ui.StepPagerStrip;

/**
 * Created by dolphin on 2013/8/16.
 */
public class SampleActivity extends SherlockFragmentActivity
        implements ModelCallbacks, PageFragmentCallbacks {
    private final static String TAG = "SampleActivity";

    private AbstractWizardModel mWizardModel;
    private ViewPager mPager;
    private WizardPagerAdapter mPagerAdapter;
    private StepPagerStrip mStepPagerStrip;
    private List<Page> mCurrentPageSequence;

    private Dialog overlayInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mWizardModel = new SampleModel(this);
        //mWizardModel.setReviewEnabled(false);
        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }
        mWizardModel.registerListener(this);//unregister in onDestory

        SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.action_bar_nav_name,
                android.R.layout.simple_spinner_dropdown_item);

        getSupportActionBar().setListNavigationCallbacks(mSpinnerAdapter, null);
        setContentView(R.layout.activity_fragment_wizard_pager);

        mPagerAdapter = new WizardPagerAdapter(getSupportFragmentManager(),
                mWizardModel);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.wizardpager_strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                int currPos = mPager.getCurrentItem();
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (currPos != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });
        mStepPagerStrip.setReviewEnabled(false);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);
            }
        });
        findViewById(R.id.wizardpager_buttonbar).setVisibility(View.GONE);

        showDemoOverlay();

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                getSupportActionBar().setSelectedNavigationItem(1);
                onPageTreeChanged();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mWizardModel.unregisterListener(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (mCurrentStep == 1) {
            this.finish();
        } else {
            View button2 = overlayInfo.findViewById(android.R.id.button2);
            if (button2 != null) {
                button2.performClick();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mWizardModel != null)
            outState.putBundle("model", mWizardModel.save());
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    @Override
    public void onPageDataChanged(Page page) {

    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        //mPagerAdapter.notifyDataSetChanged();
        //Log.d(TAG, "size() = " + mCurrentPageSequence.size());
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size()
                + (mWizardModel.isReviewEnabled() ? 1 : 0)); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.fragment_navigation, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //How to put overlay View over action bar Sherlock
    //http://stackoverflow.com/a/15638126
    private void showDemoOverlay() {
//        DisplayMetrics dm = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        Log.d(TAG, "dm.widthPixels = " + dm.widthPixels);
//        Log.d(TAG, "dm.heightPixels = " + dm.heightPixels);

        overlayInfo = new Dialog(SampleActivity.this);
        // Making sure there's no title.
        overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Setting the content using prepared XML layout file.
        overlayInfo.setContentView(R.layout.activity_sample_demo_step1);

        Window overlayWin = overlayInfo.getWindow();
        // Making dialog content transparent.
        overlayWin.setBackgroundDrawable(new ColorDrawable(Color.argb(160, 0, 0, 0)));
        // Removing window dim normally visible when dialog are shown.
        overlayWin.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        //overlayWin.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        // Setting position of content, relative to window.
//        WindowManager.LayoutParams params = overlayWin.getAttributes();
//        //params.gravity = Gravity.TOP | Gravity.LEFT;
//        params.x = 0;
//        params.y = 0;
//        params.width = dm.widthPixels;//WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = dm.heightPixels;//WindowManager.LayoutParams.MATCH_PARENT;
//        overlayWin.setAttributes(params);
        overlayWin.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        // If user taps anywhere on the screen, dialog will be cancelled.
        overlayInfo.setCancelable(false);
        overlayInfo.show();

        //loadStep(1);
        loadStep(SplashActivity.getTutorStep(this));
    }

    private int mCurrentStep = 0;

    private void loadStep(int step) {
        if (overlayInfo == null) {
            throw new NullPointerException("no tutorial dialog");
        }

        mCurrentStep = step;
        switch (step) {//load content
            case 1://step 1: welcome page
                mPager.setCurrentItem(0);
                overlayInfo.setContentView(R.layout.activity_sample_demo_step1);
                break;
            case 2://step 2: action bar tour
                mPager.setCurrentItem(0);
                overlayInfo.setContentView(R.layout.activity_sample_demo_step2);
                break;
            case 3://step 3: station tour
                mPager.setCurrentItem(0);
                overlayInfo.setContentView(R.layout.activity_sample_demo_step3);
                break;
            case 4://step 4: route tour
                mPager.setCurrentItem(1);
                overlayInfo.setContentView(R.layout.activity_sample_demo_step4);
                break;
            case 5://step 5: switch page tour
                mPager.setCurrentItem(1);
                overlayInfo.setContentView(R.layout.activity_sample_demo_step5);
                break;
            case 6://step 6: complete page
                mPager.setCurrentItem(1);
                overlayInfo.setContentView(R.layout.activity_sample_demo_step6);
                break;

        }
        SplashActivity.setTutorTo(this, mCurrentStep);

        View button1 = overlayInfo.findViewById(android.R.id.button1);
        if (button1 != null) {
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Log.d(TAG, "button1: " + mCurrentStep);
                    //overlayInfo.setContentView(R.layout.fragment_review_page);
                    if (mCurrentStep <= 1) {
                        finishTutorial();
                    } else {
                        loadStep(--mCurrentStep);
                    }
                }
            });
        }

        View button2 = overlayInfo.findViewById(android.R.id.button2);
        if (button2 != null) {
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Log.d(TAG, "button2: " + mCurrentStep);
                    //overlayInfo.dismiss();
                    if (mCurrentStep >= 6) {
                        finishTutorial();
                    } else {
                        loadStep(++mCurrentStep);
                    }
                }
            });
        }
    }

    private void finishTutorial() {
        //[43] delete old version caches
        Log.w(TAG, "clear old caches");
        File[] file1 = getFilesDir().listFiles();//internal
        for (File f : file1) f.delete();
        File[] file2 = getExternalCacheDir().listFiles();//external
        for (File f : file2) f.delete();

        //write preference
        SplashActivity.setTutorTo(this, 7);//[43] make sure it is done

        Intent intent = new Intent();
        intent.setClass(this, DghMotcActivity.class);
        startActivity(intent);

        this.finish();//go to main activity
    }
}

