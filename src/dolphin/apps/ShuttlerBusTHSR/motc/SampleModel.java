package dolphin.apps.ShuttlerBusTHSR.motc;

import android.content.Context;

import dolphin.apps.ShuttlerBusTHSR.R;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.PageList;
import rnurik.android.wizardpager.model.SingleFixedChoicePage;

/**
 * Created by dolphin on 2013/8/16.
 */
public class SampleModel extends AbstractWizardModel {
    private String[] mStations;
    private String[] mRoutes;

    public SampleModel(Context context) {
        super(context);
        this.setReviewEnabled(false);
    }

    @Override
    protected PageList onNewRootPageList() {
        mStations = mContext.getResources().getStringArray(R.array.motc_dgh_stations);
        mRoutes = mContext.getResources().getStringArray(R.array.motc_dgh_station_177);
        return new PageList(
                new SingleFixedChoicePage(this, mContext.getString(R.string.title_stations))
                        .setChoices(mStations),
                new ThsrRouteChoiceFragment.MyPage(this, mStations[4])
                        .setChoices(mRoutes),
                new SingleFixedChoicePage(this, mContext.getString(R.string.title_stations))
                        .setChoices(mStations));
    }
}
