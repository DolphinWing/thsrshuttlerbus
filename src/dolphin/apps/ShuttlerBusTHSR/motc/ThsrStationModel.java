package dolphin.apps.ShuttlerBusTHSR.motc;

//import co.juliansuarez.libwizardpager.wizard.model.AbstractWizardModel;

import android.content.Context;
import android.os.Bundle;

import dolphin.apps.ShuttlerBusTHSR.R;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.BranchPage;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.PageList;
import rnurik.android.wizardpager.model.SingleFixedChoicePage;

/**
 * Created by dolphin on 2013/8/2.
 */
public class ThsrStationModel extends AbstractWizardModel {
    public final static String KEY_STATION = "Stations";
    public final static String KEY_ROUTE = "ROUTE";
    public final static String KEY_ROUTE_POS = "ROUTE_POS";
    public final static String KEY_FAVOR_KEY = "favor_key";
    public final static String KEY_FAVOR_DATA = "favorite";

    //private Context mContext;
    private String[] mStations;
    private final static int[] mStationRouteArray = new int[]{
            R.array.motc_dgh_station_173,
            R.array.motc_dgh_station_174,
            R.array.motc_dgh_station_175,
            R.array.motc_dgh_station_176,
            R.array.motc_dgh_station_177,
            R.array.motc_dgh_station_178,
    };

    public static int getStationRouteArrayId(int index) {
        return mStationRouteArray[index];
    }

    public ThsrStationModel(Context context) {
        super(context);
        //mContext = context;
        this.setReviewEnabled(false);
    }

    @Override
    protected PageList onNewRootPageList() {
        PageList list = new PageList();
        //if (mContext != null) {
        mStations = mContext.getResources().getStringArray(R.array.motc_dgh_stations);
        BranchPage branchStation = new ThsrRouteChoiceFragment.MyBranchPage(this,
                mContext.getString(R.string.title_stations));
        for (int i = 0; i < mStations.length; i++) {
            String station = mStations[i];
            String[] routes = mContext.getResources().getStringArray(mStationRouteArray[i]);
            SingleFixedChoicePage stationPage =
                    new ThsrRouteChoiceFragment.MyPage(this, station);
            stationPage.setChoices(routes);
            //stationPage.setRequired(false);

            RouteViewFragment.RouteViewPage page =
                    new RouteViewFragment.RouteViewPage(this, KEY_ROUTE);
            //page.setRequired(true);
            branchStation.addBranch(station, stationPage, page);
        }
        list.add(branchStation);
        //} else {
        //    list = null;
        //    Log.w("dolphin", "no context");
        //}
        return list;
    }

    @Override
    public void clear() {
        super.clear();

        for (Page page : getCurrentPageSequence()) {
            Bundle bundle = page.getData();
            if (bundle.containsKey(ThsrStationModel.KEY_ROUTE_POS)) {
                //Log.d("dolphin", String.format("clear pos = %d",
                //        bundle.getInt(ThsrStationModel.KEY_ROUTE_POS, -1)));
                bundle.remove(ThsrStationModel.KEY_ROUTE_POS);
            }
        }
    }
}
