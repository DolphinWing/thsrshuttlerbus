package dolphin.apps.ShuttlerBusTHSR.motc;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import dolphin.apps.ShuttlerBusTHSR.R;

/**
 * Created by dolphin on 2013/8/5.
 * <p/>
 * http://blog.csdn.net/lilu_leo/article/details/7163457
 */
public class ExpandableListChildView extends GridView {

    public ExpandableListChildView(Context context) {
        super(context);
    }

    public ExpandableListChildView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandableListChildView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * no scroll
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

    public static class ChildAdapter extends ArrayAdapter {
        private int mSelection = 0;
        //private int mPrimaryColor = Color.BLACK;
        private int mSecondaryColor = Color.DKGRAY;;
        private int mItemHeight = 0;
        private SparseBooleanArray mLeaveTimeRed;

        public ChildAdapter(Context context, int resource, Object[] objects) {
            super(context, resource, objects);
            //mPrimaryColor =
            //    context.getResources().getColor(android.R.color.primary_text_dark);
            mSecondaryColor = Color.parseColor("#888888");//[45] use lighter color (than DKGRAY)
            //context.getResources().getColor(android.R.color.secondary_text_light);
            mItemHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    28.0f, context.getResources().getDisplayMetrics());
            mLeaveTimeRed = new SparseBooleanArray();//in case that we don't provide the map
        }

        public void setSelection(int selection) {
            mSelection = selection;
        }

        public void setColorMap(SparseBooleanArray map) {
            mLeaveTimeRed = (map != null) ? map : new SparseBooleanArray();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);
            if (v != null) {
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                if (tv != null) {
                    if (position < mSelection)
                        tv.setTextColor(mSecondaryColor);
                    //else
                    //    tv.setTextColor(mPrimaryColor);
                    tv.setGravity(TEXT_ALIGNMENT_CENTER);
                    //tv.setText(Html.fromHtml(tv.getText().toString()));
                    if (mLeaveTimeRed.get(position))
                        tv.setTextColor(Color.RED);
                }
                //if (mSelection == position)
                //    v.setBackgroundResource(R.drawable.bus);
                v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                        mItemHeight));
            }
            return v;
        }
    }
}
