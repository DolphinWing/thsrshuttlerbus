package dolphin.apps.ShuttlerBusTHSR.motc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SpinnerAdapter;
import android.widget.TabHost;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.google.analytics.tracking.android.EasyTracker;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dolphin.android.net.HttpHelper;
import dolphin.android.util.PackageUtils;
import dolphin.android.widget.FragmentTabsAdapter;
import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.SplashActivity;
import dolphin.apps.ShuttlerBusTHSR.TransferBusSA;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcData;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcDataHelper;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.BranchPage;
import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.ReviewItem;
import rnurik.android.wizardpager.model.WizardPagerAdapter;
import rnurik.android.wizardpager.ui.PageFragmentCallbacks;
import rnurik.android.wizardpager.ui.StepPagerStrip;

/**
 * Created by dolphin on 2013/8/1.
 */
public class DghMotcActivity extends SherlockFragmentActivity //ActionBarActivity
        implements ModelCallbacks, PageFragmentCallbacks,
        RouteViewFragment.Callbacks, MotcDataHelper.MotcDataListener {
    private final static String TAG = "DghMotcActivity";

    private ViewPager mPager;
    private WizardPagerAdapter mPagerAdapter;
    private StepPagerStrip mStepPagerStrip;
    private AbstractWizardModel mWizardModel;
    private HashMap<String, String> mReviewItems;

    private List<Page> mCurrentPageSequence;
    private boolean mConsumePageSelectedEvent;
    private boolean mEditingAfterReview;
    private Button mNextButton;
    private Button mPrevButton;

    private MotcDataHelper mHelper;
    private MotcData motcData;
    private int mCurrentMotcIndex = 0;

    TabHost mTabHost;
    FragmentTabsAdapter mTabsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        //[53]++ don't use popup dialog
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setSupportProgressBarIndeterminateVisibility(false);

        //since API level=9
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // http://goo.gl/cmG1V , solve android.os.NetworkOnMainThreadException
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork() // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
        }

        mReviewItems = new HashMap<String, String>();
        mHelper = new MotcDataHelper(this);

        mWizardModel = new ThsrStationModel(this);
        //mWizardModel.setReviewEnabled(false);
        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }
        mWizardModel.registerListener(this);//unregister in onDestory
        mHelper.registerDataListener(this);

        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.action_bar_nav_name,
                android.R.layout.simple_spinner_dropdown_item);

        getSupportActionBar().setListNavigationCallbacks(mSpinnerAdapter,
                mOnNavigationListener);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (mHelper.hasFavoriteRoutes())
                    getSupportActionBar().setSelectedNavigationItem(0);
                else
                    getSupportActionBar().setSelectedNavigationItem(1);
            }
        });
    }

    private void loadFavoriteLayout() {
        Map<String, MotcData.Route> list = mHelper.getFavoriteRoutes();
        if (list != null && list.size() > 0) {
            setContentView(R.layout.activity_fragment_tabs_pager);

            mTabHost = (TabHost) findViewById(android.R.id.tabhost);
            mTabHost.setup();
            //mTabHost.clearAllTabs();

            mPager = (ViewPager) findViewById(R.id.pager);
            mTabsAdapter = new FragmentTabsAdapter(this, mTabHost, mPager);

            Object[] keys = list.keySet().toArray();
            for (int i = 0; i < keys.length; i++) {
                Bundle bundle = new Bundle();
                MotcData.Route r = list.get(keys[i]);
                bundle.putString(ThsrStationModel.KEY_FAVOR_DATA, MotcData.toJson(r));
                bundle.putString(ThsrStationModel.KEY_FAVOR_KEY, keys[i].toString());
                mTabsAdapter.addTab(mTabHost.newTabSpec(String.format("route%d", i))
                        .setIndicator(MotcDataHelper.formatTitle(r.getName())),
                        RouteViewFragment.class, bundle);
            }
        } else {
            setContentView(R.layout.fragment_empty_view);
        }
    }

    private void clearTabFragments() {
        //How to destroy Fragment?
        //http://stackoverflow.com/a/10046716
        FragmentManager fmgr = getSupportFragmentManager();
        if (fmgr != null) {
            List<Fragment> list = fmgr.getFragments();
            if (list != null && list.size() > 0) {
                //FragmentManager BackStackRecord.run throwing NullPointerException
                //http://stackoverflow.com/a/13395157
                FragmentTransaction trans = fmgr.beginTransaction();
                for (Fragment f : list) if (f != null) trans.remove(f);
                trans.commit();
            }
        }
    }

    private void loadWizardLayout() {
        //setContentView(R.layout.activity_fragment_wizard_pager_thsr);
        setContentView(R.layout.activity_fragment_wizard_pager);

        if (mTabHost != null) {
            clearTabFragments();
        }
        mWizardModel.clear();
        mCurrentMotcIndex = 0;//no select any station
        //mWizardModel.setReviewEnabled(false);
        mPagerAdapter = new WizardPagerAdapter(getSupportFragmentManager(),
                mWizardModel);
        //Log.d(TAG, String.format("mPagerAdapter %d", mPagerAdapter.getCount()));

        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.wizardpager_strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                int currPos = mPager.getCurrentItem();
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                //Log.d(TAG, "onPageStripSelected position = " + position);
                //Log.d(TAG, "  CurrentItem = " + mPager.getCurrentItem());
                if (currPos < position
                        && !mCurrentPageSequence.get(currPos).isCompleted())
                    return;//don't do anything unless you have selected something
                else if (currPos != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });
        mStepPagerStrip.setReviewEnabled(false);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //Log.d(TAG, "onPageSelected position = " + position);
                String station = mReviewItems.get(ThsrStationModel.KEY_STATION);
                //Log.d(TAG, "current station = " + station);
                if (position == 1) {
                    int motcIndex = mHelper.getMotcIndex(station);
                    //motcData = mHelper.get_motc_html(motcIndex);
                    mHelper.asyGetMotcData(DghMotcActivity.this, motcIndex);
                }
                //lock the screen if download is from web

                mStepPagerStrip.setCurrentPage(position);

                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                //updateBottomBar();
                invalidateOptionsMenu();//creates call to onPrepareOptionsMenu()
            }
        });

        findViewById(R.id.wizardpager_buttonbar).setVisibility(View.GONE);
        mNextButton = (Button) findViewById(R.id.wizardpager_next_button);
        if (mNextButton != null)
            mNextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Log.d(TAG, "mEditingAfterReview = " + mEditingAfterReview);
                    //Log.d(TAG, "getCount() = " + mPagerAdapter.getCount());
                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        //Log.d(TAG, "getChildCount() = " + mPager.getChildCount());
                        //Log.d(TAG, "CurrentItem = " + mPager.getCurrentItem());
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            });

        mPrevButton = (Button) findViewById(R.id.wizardpager_prev_button);
        if (mPrevButton != null)
            mPrevButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
            });

        onPageTreeChanged();
        setSupportProgressBarIndeterminateVisibility(false);//[55]++
    }

    @Override
    public void onBackPressed() {
        int navType = getSupportActionBar().getSelectedNavigationIndex();
        switch (navType) {
//            case 0://favorite
//                break;
            case 1://navigation
                if (mPager.getCurrentItem() > 0) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                    return;
                }
                break;
        }
//        if (mPager.getCurrentItem() > 0)
//            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
//        else
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
        mHelper.unregisterDataListener(this);

        mReviewItems.clear();
        mReviewItems = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        if (mWizardModel != null)
            outState.putBundle("model", mWizardModel.save());
        if (mTabHost != null)
            outState.putString("tab", mTabHost.getCurrentTabTag());
    }

    ActionBar.OnNavigationListener mOnNavigationListener =
            new ActionBar.OnNavigationListener() {
                // Get the same strings provided for the drop-down's ArrayAdapter
                //String[] strings = getResources().getStringArray(R.array.action_bar_nav_name);

                @Override
                public boolean onNavigationItemSelected(int position, long itemId) {
                    //Log.d(TAG, "mOnNavigationListener: " + position);
                    switch (position) {
                        case 0://favorite
                            loadFavoriteLayout();
                            break;
                        case 1://navigator
                            loadWizardLayout();
                            break;
                    }
                    invalidateOptionsMenu();
                    return true;
                }
            };

    @Override
    public void onPageDataChanged(Page changedPage) {
        //Log.d(TAG, "page.getKey() = " + page.getKey());
        //Log.d(TAG, "page.getTitle() = " + page.getTitle());
        //Log.d(TAG, "page.isRequired() = " + page.isRequired());

//		//clear the useless data by starting a new branch
//		Bundle bundle = changedPage.getData();
//		if (bundle.containsKey(ThsrStationModel.KEY_ROUTE_POS)) {//only remove page 2
//            Log.d(TAG, String.format("page pos %d gg",
//                    bundle.getInt(ThsrStationModel.KEY_ROUTE_POS, -1)));
//            bundle.remove(ThsrStationModel.KEY_ROUTE_POS);
//        }

        mReviewItems.clear();//reconstruct the ReviewItem
        ArrayList<ReviewItem> reviewItems = new ArrayList<ReviewItem>();
        for (Page page : mWizardModel.getCurrentPageSequence()) {
            if (changedPage instanceof BranchPage
                    && page.getData().containsKey(ThsrStationModel.KEY_ROUTE_POS)) {
                //Log.d(TAG, String.format("page pos %d gg",
                //        page.getData().getInt(ThsrStationModel.KEY_ROUTE_POS, -1)));
                page.getData().remove(ThsrStationModel.KEY_ROUTE_POS);
            }
            page.getReviewItems(reviewItems);
        }
        for (ReviewItem item : reviewItems) {
            //Log.d(TAG, item.getPageKey() + ": " + item.getDisplayValue());
            mReviewItems.put(item.getPageKey(), item.getDisplayValue());
        }
//        if (page.isRequired()) {
//            if (recalculateCutOffPage()) {
//                mPagerAdapter.notifyDataSetChanged();
//                //updateBottomBar();
//            }
//        }

        if (changedPage.isCompleted() && mNextButton != null)
            mNextButton.performClick();
    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        //mPagerAdapter.notifyDataSetChanged();
        //Log.d(TAG, "size() = " + mCurrentPageSequence.size());
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size()
                + (mWizardModel.isReviewEnabled() ? 1 : 0)); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();

        //updateBottomBar();
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        //Log.d(TAG, "cutOffPage = " + cutOffPage);
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            //Log.d(TAG, String.format("isRequired = %s", page.isRequired()));
            //Log.d(TAG, String.format("isCompleted = %s", page.isCompleted()));
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String pageKey) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(pageKey)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                //updateBottomBar();
                break;
            }
        }
    }

    @Override
    public MotcDataHelper getMotcDataHelper() {
        return mHelper;
    }

    @Override
    public MotcData getMotcData() {
        return motcData;
    }

    ProgressDialog dialog;

    @Override
    public void onDownloadStart(int motcIndex) {
//        Toast.makeText(this, "onDownloadStart", Toast.LENGTH_SHORT).show();
        if (mCurrentMotcIndex == motcIndex)
            return;//don't show dialog
        //Log.d(TAG, "onDownloadStart " + motcIndex);

        setSupportProgressBarIndeterminateVisibility(true);//[53]++
        //dialog = ProgressDialog.show(this, null, getString(R.string.processing_html));
        //dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //dialog.setCancelable(false);
        //dialog.setCanceledOnTouchOutside(false);
        //dialog.show();
    }

    @Override
    public void onDownloadComplete(int motcIndex, MotcData data) {
//        Toast.makeText(this, "onDownloadComplete", Toast.LENGTH_SHORT).show();
        mCurrentMotcIndex = motcIndex;
        //Log.d(TAG, "onDownloadComplete " + motcIndex);
        motcData = data;

        int seq = mHelper.getStationSeq(motcIndex);
        for (int i = 0; i < motcData.getRoutes().size(); i++) {
            String key = String.format("%d-%d", seq + 1, i + 1);
            if (mHelper.isFavorite(key)) {
                //Log.d(TAG, "update " + key);
                mHelper.updateFavorite(key, motcData.getRoute(i));
            }
        }

        if (dialog != null) dialog.dismiss();
        dialog = null;
        setSupportProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void onError() {
//        Toast.makeText(this, "onError", Toast.LENGTH_SHORT).show();
        //Log.e(TAG, "onError");
        if (dialog != null) dialog.dismiss();
        dialog = null;
        setSupportProgressBarIndeterminateVisibility(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        int navType = getSupportActionBar().getSelectedNavigationIndex();
//        switch (navType) {
//            case 0://favorite
////                break;
//            case 1://navigation
        getSupportMenuInflater().inflate(R.menu.fragment_navigation, menu);
//                break;
//        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int navType = getSupportActionBar().getSelectedNavigationIndex();
        switch (item.getItemId()) {
            case R.id.menu_item_favorite_add:
                if (navType == 1) {//navigation: add to favorite
                    int seq = mHelper.getStationSeq(mReviewItems.get(ThsrStationModel.KEY_STATION));
                    int pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
                    //Log.d(TAG, String.format("%d-%d", seq + 1, pos + 1));
                    if (motcData != null) {
                        MotcData.Route route = motcData.getRoute(pos);
                        if (route != null) {
                            mHelper.addFavorite(String.format("%d-%d", seq + 1, pos + 1),
                                    route);
                            Toast.makeText(this, R.string.add_to_favorite_ok,
                                    Toast.LENGTH_SHORT).show();
                            item.setEnabled(false);
                            return true;
                        }
                    }
                }
                break;
            case R.id.menu_item_favorite_remove:
                if (navType == 0) {//favorite: remove from favorite
                    int index = mPager.getCurrentItem();
                    //Log.d(TAG, "current index: " + index);
                    Map<String, MotcData.Route> list = mHelper.getFavoriteRoutes();
                    if (list != null && list.size() > 0) {
                        Object[] keys = list.keySet().toArray();
                        //Log.d(TAG, "key = " + keys[index]);
                        mHelper.removeFavorite(keys[index].toString());
//                    clearTabFragments();
//                    loadFavoriteLayout();
//                    FragmentManager fmgr = getSupportFragmentManager();
//                    FragmentTransaction trans = fmgr.beginTransaction();
//                    String tag = FragmentTabsAdapter.getFragmentTag(R.id.pager, index);
//                    trans.remove(fmgr.findFragmentByTag(tag));
//                    trans.commit();
//                    mTabsAdapter.removeTab(index);
                        Toast.makeText(this, R.string.remove_favorite_ok,
                                Toast.LENGTH_SHORT).show();
                        //[50]-- item.setEnabled(false);
                        return true;
                    }
                }
                break;
            case R.id.menu_item_refresh:
                if (navType == 1) {
                    int index = mPager.getCurrentItem();
                    String station = mReviewItems.get(ThsrStationModel.KEY_STATION);
                    int seq = mHelper.getStationSeq(station);
                    if (seq > 0 && index >= 1) {
                        int motcIndex = mCurrentMotcIndex;//[45]-- mHelper.getMotcIndex(station);
                        mCurrentMotcIndex = -1;//[45]++ to show the dialog on download start
                        motcData = null;
                        mHelper.clear_motc_cache(motcIndex);

                        if (index >= 2) {//[46]++ delete map
                            int pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
                            String key = String.format("%d-%d", seq + 1, pos + 1);
                            File f = new File(getExternalCacheDir(), key + ".png");
                            if (!f.exists())//try if PNG or JPG
                                f = new File(getExternalCacheDir(), key + ".jpg");
                            if (f.exists()) {//if exist, try to delete it
                                Log.v(TAG, f.getAbsolutePath());
                                f.delete();
                            }
                        } else {//[50]++ delete all files @ pos = 1
                            for (int i = 1; i < 30; i++) {//delete all files
                                String key = String.format("%d-%d", seq + 1, i);
                                File f = new File(getExternalCacheDir(), key + ".png");
                                if (!f.exists())//try if PNG or JPG
                                    f = new File(getExternalCacheDir(), key + ".jpg");
                                if (f.exists()) {//if exist, try to delete it
                                    Log.v(TAG, f.getAbsolutePath());
                                    f.delete();
                                }
                            }
                        }

                        mHelper.asyGetMotcData(DghMotcActivity.this, motcIndex);
                        return true;
                    }
                }
                break;
            case R.id.menu_item_about:
                showAboutDialog();
                return true;

            case R.id.menu_item_use_old:
                useOldVersion();
                return true;

            case R.id.menu_item_motc_web://[56]++
                if (mPager != null) {//navigation
                    int currIndex = mPager.getCurrentItem();
                    int motcIndex = mHelper.getStationSeq(
                            mReviewItems.get(ThsrStationModel.KEY_STATION)) + 173;
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(getString(R.string.motc_url_base, motcIndex))));
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        int navType = getSupportActionBar().getSelectedNavigationIndex();
        MenuItem item = null;
        switch (navType) {
            case 0: {//favorite
                item = menu.findItem(R.id.menu_item_favorite_add);
                if (item != null)
                    item.setVisible(false);
                item = menu.findItem(R.id.menu_item_favorite_remove);
                if (item != null) {
                    item.setVisible(true);
                    item.setEnabled(mHelper.hasFavoriteRoutes());
                }
                item = menu.findItem(R.id.menu_item_refresh);
                if (item != null)
                    item.setVisible(false);

                item = menu.findItem(R.id.menu_item_motc_web);//[56]++
                if (item != null)
                    item.setVisible(false);
            }
            break;
            case 1:
                if (mPager != null) {//navigation
                    int currIndex = mPager.getCurrentItem();

                    item = menu.findItem(R.id.menu_item_favorite_add);
                    if (item != null) {
                        item.setVisible((currIndex > 1));
                        if (currIndex > 1) {//only in RouteViewFragment
                            int seq = mHelper.getStationSeq(mReviewItems.get(ThsrStationModel.KEY_STATION));
                            int pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
                            String key = String.format("%d-%d", seq + 1, pos + 1);
                            item.setEnabled(!mHelper.isFavorite(key));
                        }
                    }
                    item = menu.findItem(R.id.menu_item_favorite_remove);
                    if (item != null)
                        item.setVisible(false);
                    item = menu.findItem(R.id.menu_item_refresh);
                    if (item != null)
                        item.setVisible((currIndex > 0) //[46]++ also check network
                                && HttpHelper.checkNetworkAvailable(this));

                    item = menu.findItem(R.id.menu_item_use_old);//[50]++
                    if (item != null)
                        item.setVisible((currIndex == 0));
                    //[56]++ navigate to web page directly
                    item = menu.findItem(R.id.menu_item_motc_web);
                    if (item != null)
                        item.setVisible((currIndex > 0) //also check network
                                && HttpHelper.checkNetworkAvailable(this));
                }
                break;
        }

        return super.onPrepareOptionsMenu(menu);
    }

    private void showAboutDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle(R.string.action_about);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
//        dialog.setButton(DialogInterface.BUTTON_NEUTRAL,
//                getString(R.string.action_send_mail),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                    }
//                });
        PackageInfo info = PackageUtils.getPackageInfo(this, DghMotcActivity.class);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage(Html.fromHtml(getString(R.string.about_dialog)
                + "<small style='color: #999999;'>"
                + info.versionName + " r" + info.versionCode + "</small>"));
        dialog.show();
    }

    private void useOldVersion() {
        SplashActivity.setUseOldVersion(this, true);

        Intent intent = new Intent();
        intent.setClass(this, TransferBusSA.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        //[57]++ ... // The rest of your onStart() code.
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //[57]++ ... // The rest of your onStop() code.
        EasyTracker.getInstance(this).activityStop(this);
    }
}
