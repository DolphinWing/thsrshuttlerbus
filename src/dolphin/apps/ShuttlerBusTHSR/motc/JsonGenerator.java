package dolphin.apps.ShuttlerBusTHSR.motc;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import dolphin.android.apps.RailwayTimeHelper.provider.RailwayTimeHelper;
import dolphin.android.apps.RailwayTimeHelper.provider.TrainInfo;
import dolphin.android.util.FileUtils;
import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcData;
import dolphin.apps.ShuttlerBusTHSR.provider.MotcDataHelper;

/**
 * Created by dolphin on 2013/9/7.
 */
public class JsonGenerator extends Activity {

    final static String TAG = "JsonGenerator";

    String[] mMapId;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //since API level=9
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // http://goo.gl/cmG1V , solve android.os.NetworkOnMainThreadException
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .permitDiskWrites()
                    .permitDiskReads()
                    .permitNetwork()
                    .penaltyLog()
                    .build());
        }

        setContentView(R.layout.json_generator);

        mMapId = getResources().getStringArray(R.array.json_src);

        Button btGenerate = (Button) findViewById(android.R.id.button1);
        btGenerate.setOnClickListener(onGenerate);
        Button btReview = (Button) findViewById(android.R.id.button2);
        btReview.setOnClickListener(onReview);
        Button btDownload = (Button) findViewById(android.R.id.button3);
        btDownload.setOnClickListener(onDownload);
        btDownload.setOnClickListener(onTraTest);
    }

    Button.OnClickListener onGenerate = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            ArrayList<MotcData.Map> maps = new ArrayList<MotcData.Map>();
            final Resources res = getResources();
            for (int i = 0; i < 6; i++) {
                String[] keys = res.getStringArray(MotcDataHelper.getRouteMapNameArrayId(i));
                String[] ids = res.getStringArray(MotcDataHelper.getRouteMapIdArrayId(i));
                for (int j = 0; j < keys.length; j++) {
                    maps.add(new MotcData.Map(keys[j], ids[j]));
                }
            }
            String[] mapKeys = res.getStringArray(R.array.json_dst);
            String[] mapIds = res.getStringArray(R.array.json_src);
            for (int k = 0; k < mapKeys.length; k++) {
                maps.add(new MotcData.Map(mapKeys[k], mapIds[k]));
            }

            File f = new File(getExternalCacheDir(), "map.json");
            FileUtils.writeStringToFile(f, MotcDataHelper.toJson(maps));

            Toast.makeText(getBaseContext(), R.string.action_done,
                    Toast.LENGTH_SHORT).show();
        }
    };

    Button.OnClickListener onReview = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            File f = new File(getExternalCacheDir(), "map.json");
            if (f.exists()) {
                String json = FileUtils.readFileToString(f);
                Log.d(TAG, "json size: " + json.length());
                HashMap<String, String> maps = MotcDataHelper.jsonToMap(json);
                Toast.makeText(getBaseContext(), maps.get("5-2.png"),
                        Toast.LENGTH_SHORT).show();
            }

        }
    };

    Button.OnClickListener onDownload = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Spinner spinner = (Spinner) findViewById(android.R.id.list);
            String driveId = mMapId[spinner.getSelectedItemPosition()];
            Log.d(TAG, "driveId: " + driveId);
            File f = new File(getExternalCacheDir(), "tmp.json");
            boolean r = MotcDataHelper.downloadFromGoogleDrive(getBaseContext(),
                    driveId, f);
            Log.d(TAG, "download " + (r ? "success" : "failed"));
            if (r) {
                TextView tv = (TextView) findViewById(android.R.id.text2);
                tv.setText(FileUtils.readFileToString(f));
            }
            Toast.makeText(getBaseContext(), (r ? "success" : "failed"),
                    Toast.LENGTH_SHORT).show();
        }
    };

    Button.OnClickListener onTraTest = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            RailwayTimeHelper helper = new RailwayTimeHelper(getBaseContext());
            helper.getAvailableDates();

            Calendar now = Calendar.getInstance();
            now.add(Calendar.DAY_OF_YEAR, 1);//tomorrow
            String date = String.format("%04d%02d%02d",
                    now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1,
                    now.get(Calendar.DAY_OF_MONTH));
            Toast.makeText(JsonGenerator.this, date, Toast.LENGTH_SHORT).show();
            helper.getXmlData(date);
            List<TrainInfo> list = helper.parseXmlData(date);
            if (list != null) {
                String result = "";
                Log.v(TAG, "assume 1008 -> 1228");
                //assume 1008 -> 1228
                List<TrainInfo> trains = helper
                        .getAvailableTrains(list, 0, 1008, 1228, 12, 0, 14, 0);
                for (TrainInfo info : trains) {
                    Log.v(TAG, info.toString());
                    result += info.toString() + "\n";
                }
                TextView tv = (TextView) findViewById(android.R.id.text2);
                tv.setText(result);
            }
        }
    };
}