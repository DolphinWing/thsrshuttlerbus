package dolphin.apps.ShuttlerBusTHSR.provider;

import java.util.ArrayList;

public class THSRData
{
	public static final String INTENT_DOWNLOAD_START =
		"dolphin.apps.ShuttlerBusTHSR.provider.INTENT_DOWNLOAD_START";
	public static final String INTENT_DOWNLOAD_COMPLETE =
		"dolphin.apps.ShuttlerBusTHSR.provider.INTENT_DOWNLOAD_COMPLETE";
	public static final String EXTRA_RESULT = "RESULT";

	public static class BaseTransferItem
	{
		protected String url;
		protected String title;

		public BaseTransferItem(String title, String url)
		{
			this.title = title;
			this.url = url;
		}

		public String getTitle()
		{
			return title;
		}

		public String getName()
		{
			if (url != null && url.contains("/")) {
				//Log.d(THSRDataHelper.TAG, url);
				return url.substring(url.lastIndexOf("/") + 1);
			}
			return url;
		}
	}

	public static class TransferPdf extends BaseTransferItem
	{
		public TransferPdf(String title, String url)
		{
			super(title, url);
			// TODO Auto-generated constructor stub
		}
	}

	public static class TransferRoute extends BaseTransferItem
	{
		public TransferRoute(String title, String url)
		{
			super(title, url);
			// TODO Auto-generated constructor stub
		}

        private String dstName;

        public TransferRoute(String title, String url, String dst) {
            super(title, url);
            dstName = dst;
        }

		public String getUrl()
		{
			return url;
		}

        public String getDstName() {
            return dstName;
        }
	}

	public static class TransferStation
	{
		private String name;
		private ArrayList<TransferRoute> list;

		public TransferStation(String name, ArrayList<TransferRoute> list)
		{
			this.name = name;
			this.list = list;
		}

		public String getName()
		{
			return name;
		}

		public ArrayList<TransferRoute> getRoutes()
		{
			return list;
		}
	}
}
