package dolphin.apps.ShuttlerBusTHSR.provider;

import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by dolphin on 2013/8/3.
 */
public class MotcData {
    private String mUpdateTime;
    private ArrayList<Route> mRoute = new ArrayList<Route>();

    public MotcData() {
        setUpdateTime(Calendar.getInstance().toString());
    }

    public MotcData(String time) {
        setUpdateTime(time);
    }

    /**
     * store the update time
     *
     * @param time
     */
    public void setUpdateTime(String time) {
        mUpdateTime = time;
    }

    /**
     * get update time
     *
     * @return
     */
    public String getUpdateTime() {
        return mUpdateTime;
    }

    /**
     * add route object
     *
     * @param route
     */
    public void addRoute(Route route) {
        mRoute.add(route);
    }

    /**
     * get a route data
     *
     * @param index
     * @return
     */
    public Route getRoute(int index) {
        if (index < 0 || index >= mRoute.size())
            return null;
        return mRoute.get(index);
    }

    /**
     * get last route in the list
     *
     * @return
     */
    public Route getLastRoute() {
        return getRoute(getRoutes().size() - 1);
    }

    /**
     * get all routes
     *
     * @return
     */
    public ArrayList<Route> getRoutes() {
        return mRoute;
    }

    /**
     * find a special route in the list
     *
     * @param name
     * @return
     */
    public Route findRoute(String name) {
        for (Route route : mRoute) {
            if (route.getName().contains(name))
                return route;
        }

        return null;//can't find the route
    }

    /**
     * A route may have many schedules and different terminals
     * (at least 2 end station, maybe for regular days/holidays)
     */
    public static class Route {
        public String mMapUrl;
        private String mName;
        private ArrayList<Schedule> mSchedule;

        public Route(String name) {
            mName = name;
            mSchedule = new ArrayList<Schedule>();
        }

        /**
         * get route name
         *
         * @return
         */
        public String getName() {
            return mName;
        }

        /**
         * set map url
         *
         * @param url
         */
        public void setMapUrl(String url) {
            mMapUrl = url;
        }

        /**
         * get map url
         *
         * @return
         */
        public String getMapUrl() {
            return mMapUrl;
        }

        /**
         * add a schedule object
         *
         * @param schedule
         */
        public void addSchedule(Schedule schedule) {
            mSchedule.add(schedule);
        }

        /**
         * get a special schedule
         *
         * @param index
         * @return
         */
        public Schedule getSchedule(int index) {
            return mSchedule.get(index);
        }

        /**
         * get the last schedule in the list
         *
         * @return
         */
        public Schedule getLastSchedule() {
            return getSchedule(getSchedules().size() - 1);
        }

        /**
         * get all schedules
         *
         * @return
         */
        public ArrayList<Schedule> getSchedules() {
            return mSchedule;
        }
    }

    /**
     * store a route schedule including terminals and bus leave time
     */
    public static class Schedule {
        private String mTerminal;
        private ArrayList<Calendar> mLeaveTime;
        private SparseArray<String> mLeaveTimeExt;
        private SparseBooleanArray mLeaveTimeRed;
        private String mExtras;

        public Schedule(String terminal) {
            mTerminal = terminal;
            mLeaveTime = new ArrayList<Calendar>();
            mLeaveTimeExt = new SparseArray<String>();
            mLeaveTimeRed = new SparseBooleanArray();
            mExtras = null;
        }

        /**
         * add a bus leave time
         *
         * @param time
         */
        public void addTime(String time) {
            try {
                if (time.contains("(")) {//Chiayi
                    mLeaveTimeExt.put(mLeaveTime.size(), time);
                    time = time.substring(0, time.indexOf("("));
                }
                if (time.contains("font")) {//for Taichung/Chiayi/Zuoying
                    mLeaveTimeRed.put(mLeaveTime.size(), time.contains("color"));
                    time = time.substring(time.indexOf(">") + 1);
                    time = time.substring(0, time.indexOf("</font"));
                    if (!time.contains(":"))
                        time = String.format("%s:%s",
                                time.substring(0, 2), time.substring(2));
                }
                if (time.contains(":")) {
                    String[] t = time.split(":");
                    Calendar c = Calendar.getInstance();
                    c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(t[0]));
                    c.set(Calendar.MINUTE, Integer.parseInt(t[1]));
                    mLeaveTime.add(c);
                }
            } catch (Exception e) {
                Log.e(MotcDataHelper.TAG, time + ": " + e.getMessage());
            }
        }

        /**
         * get terminal name
         *
         * @return
         */
        public String getTerminal() {
            return mTerminal;
        }

        /**
         * set terminal name
         *
         * @param name
         */
        public void setTerminal(String name) {
            mTerminal = name;
        }

        /**
         * get bus leave time list
         *
         * @return
         */
        public ArrayList<Calendar> getLeaveTime() {
            ArrayList<Calendar> calendars = new ArrayList<Calendar>();
            for (Calendar cal : mLeaveTime) {
                Calendar c = Calendar.getInstance();//change to today
                c.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
                c.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                calendars.add(c);
            }
            return calendars;
        }

        /**
         * check if has some special info for leave time
         *
         * @return
         */
        public boolean hasLeaveTimeExtra() {
            if (mLeaveTimeExt != null)
                return (mLeaveTimeExt.size() > 0);
            return false;
        }

        /**
         * get extra data for leave time
         *
         * @param index
         * @return
         */
        public String getLeaveTimeExtra(int index) {
            if (mLeaveTimeExt != null)
                return mLeaveTimeExt.get(index);
            return null;
        }

        /**
         * check if the leave time is special marked
         *
         * @param index
         * @return
         */
        public boolean isLeaveTimeRed(int index) {
            if (mLeaveTimeRed != null)
                return mLeaveTimeRed.get(index);
            return false;
        }

        /**
         * get special marked leave time list
         *
         * @return
         */
        public SparseBooleanArray getLeaveTimeRed() {
            if (mLeaveTimeRed != null)
                return mLeaveTimeRed;
            return new SparseBooleanArray();//for some old data
        }

        /**
         * set extra messages for schedule
         *
         * @param msg
         */
        public void setExtras(String msg) {
            mExtras = msg;
        }

        /**
         * get extra messages for schedule
         *
         * @return
         */
        public String getExtras() {
            return mExtras;
        }
    }

    /**
     * convert to json string
     *
     * @param data
     * @return
     */
    public static String toJson(MotcData data) {
        Gson gson = new Gson();
        return gson.toJson(data);
    }

    /**
     * convert json string to MotcData object
     *
     * @param json
     * @return
     */
    public static MotcData jsonToMotcData(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<MotcData>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    /**
     * convert to json string
     *
     * @param data
     * @return
     */
    public static String toJson(Route data) {
        Gson gson = new Gson();
        return gson.toJson(data);
    }

    /**
     * convert json string to MotcData.Route object
     *
     * @param json
     * @return
     */
    public static Route jsonToRoute(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<Route>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static class Map {
        private String Key;
        private String DriveId;

        public Map(String key, String id) {
            Key = key;
            DriveId = id;
        }

        public String getKey() {
            return Key;
        }

        public String getId() {
            return DriveId;
        }
    }
}
