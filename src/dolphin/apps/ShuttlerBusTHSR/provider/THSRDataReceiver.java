package dolphin.apps.ShuttlerBusTHSR.provider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class THSRDataReceiver extends BroadcastReceiver
{
	public static final String TAG = "THSRDataReceiver";

	@Override
	public void onReceive(Context context, Intent intent)
	{
		String action = intent.getAction();
		Log.i(TAG, action);
		if (action.contentEquals(THSRData.INTENT_DOWNLOAD_COMPLETE)) {
			//finish the download
		}
		else if (action.contentEquals(THSRData.INTENT_DOWNLOAD_START)) {
			//start download
			start_download_cache(context);
		}
	}

	private void start_download_cache(Context context)
	{
		Intent intent = new Intent(context, THSRDataService.class);
		context.startService(intent);
	}
}
