package dolphin.apps.ShuttlerBusTHSR.provider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;

import com.quanta.pobu.net.QHttpHelper;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.motc.ThsrStationModel;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferPdf;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferRoute;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRData.TransferStation;

public class THSRDataHelper extends QHttpHelper {
    //DATA SOURCE: http://www.thsrc.com.tw/tc/transfer/transfer.asp
    public static final String TAG = "THSRDataHelper";
    protected Context mContext;

    public static final String BASE_URL = "http://www.thsrc.com.tw";
    public static final String TRANSFER_DATA_SOURCE =
            //[56]-- BASE_URL + "/tc/transfer/transfer.asp";
            BASE_URL + "/tw/Article/ArticleContent/c2a8fd9d-ced6-43b1-9297-9eab4127b5ff";
    public static final String CACHE_FILE_NAME = "transfer.html";

    public static final String KEY_INIT = "INIT";

    public THSRDataHelper(Context context) {
        mContext = context;
    }

    public String get_transfer_html() {
        return get_transfer_html(false);
    }

    @SuppressLint("WorldWriteableFiles")
    @SuppressWarnings("deprecation")
    public String get_transfer_html(boolean bForceUpdate) {
        String response = null;
        if (!bForceUpdate) {//read from cache
            FileInputStream fis = null;
            try {
                fis = mContext.openFileInput(CACHE_FILE_NAME);
                InputStreamReader isr = new InputStreamReader(fis);
                StringBuffer sb = new StringBuffer();
                int len = 0;
                //				while ((len = fis.read()) != -1) {
                //					sb.append((char) len);
                //				}
                while (true) {//read from buffer
                    char[] buffer = new char[512];
                    //					len = fis.read(buffer);//, size, 512);
                    len = isr.read(buffer);
                    //Log.d(TAG, String.format("%d", len));
                    if (len > 0) {
                        sb.append(buffer);
                    } else {
                        break;
                    }
                }
                fis.close();
                response = sb.toString();
                Log.d(TAG, String
                        .format("cache length = %s", response.length()));
            } catch (Exception e) {
                e.printStackTrace();
                response = null;
            }
        }

        if (response == null) //no data from cache
            try {//read from web
                Log.i(TAG, TRANSFER_DATA_SOURCE);
                response = super.getUrlContent(TRANSFER_DATA_SOURCE);// , ENCODE_BIG5);
                //Log.d(TAG, String.format("%d %s", response.length(), response));
                //Log.d(TAG, String.format("length = %s", response.length()));
                //write the data to cache
                FileOutputStream fos = null;

                try {//add write file
                    fos =
                            mContext.openFileOutput(CACHE_FILE_NAME,
                                    Context.MODE_WORLD_WRITEABLE);
                    if (fos != null) {
                        OutputStreamWriter osw =
                                new OutputStreamWriter(fos, "utf-8");
                        osw.write(response);
                        //fos.write(response.getBytes());
                        //fos.close();
                        osw.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "write the data to cache: " + e.getMessage());
                }

            } catch (Exception e) {
                Log.e(TAG, String.format("get_transfer_html: %s, %s", this
                        .getClass().getName(), e.getMessage()));
            }

        return response;
    }

    public ArrayList<TransferPdf> get_transfer_pdf() {
        ArrayList<TransferPdf> list = new ArrayList<TransferPdf>();

        String html = get_transfer_html();
        //PATTERN: download/transfer
        String pattern = "(download/transfer/[^\"]*)\">([^<]*)";
        if (html != null && html.contains("download/transfer")) {
            Matcher matcher = Pattern.compile(pattern).matcher(html);
            while (matcher.find()) {
                String title = matcher.group(2).trim();
                title =
                        title.replace("\t", "").replace("\n", "").replace("\r", "");
                String url = BASE_URL + "/" + matcher.group(1);
                //Log.i(TAG, String.format("%s %s", title, url));
                list.add(new TransferPdf(title, url));
            }
        }

        return list;
    }

    public ArrayList<TransferStation> get_transfer_station() {
        ArrayList<TransferStation> list = new ArrayList<TransferStation>();

        String html = get_transfer_html();
        if (html != null && html.contains("qbus_mid")) {
            html = html.substring(html.indexOf("qbus_mid"));
            //                    123456789
            while (html.contains("Start -->")) {//station by station
                //int start = html.indexOf("Start -->");
                int end = html.indexOf("End -->");
                String html_station = html.substring(0, end);
                String station_title =
                        html_station.substring(html_station.indexOf("<!--") + 4,
                                html_station.indexOf("-->") - 6).trim();
                //Log.i(TAG, String.format("station_title = %s", station_title));

                ArrayList<TransferRoute> routes =
                        new ArrayList<TransferRoute>();
                //PATTERN: 
                Matcher matcherTitle, matcherGif;
                //<td class="dtz_b" width=380><strong>title...
                matcherTitle = //[7]2012-03-02 THSR updated new routes
                        //Pattern.compile("<td class=\"dtz_b[^>]*><strong>([^<]*)")
                        Pattern.compile("><strong>([^<]*)").matcher(html_station);
                //onClick="MM_openBrWindow('../images/transfer/q_bus/tao_01.gif'
                matcherGif =
                        Pattern.compile("(images/transfer/q_bus/[^']*)").matcher(
                                html_station);
                while (matcherTitle.find()) {
                    if (!matcherGif.find()) //if title can find, the gif should be find
                        break;
                    String title = matcherTitle.group(1).trim();
                    //http://www.thsrc.com.tw/tc/images/transfer/q_bus/tao_01.gif
                    String url = BASE_URL + "/tc/" + matcherGif.group(1);
                    //Log.i(TAG, String.format("%s %s", title, url));
                    routes.add(new TransferRoute(title, url));
                }
                list.add(new TransferStation(station_title, routes));

                //check next station
                html = html.substring(end + 5);
            }
        }

        return list;
    }

    public ArrayList<TransferStation> get_transfer_station(boolean builtIn) {
        if (builtIn) {//[56]++
            final Resources res = mContext.getResources();
            ArrayList<TransferStation> list = new ArrayList<TransferStation>();
            String[] mMapping = res.getStringArray(R.array.thsr_map);
            String[] mStations = res.getStringArray(R.array.thsr_stations);
            for (int i = 0; i < mStations.length; i++) {
                //int motcIndex = 173 + i;
                ArrayList<TransferRoute> routes = new ArrayList<TransferRoute>();
                //Log.d(TAG, String.format("%d %s", motcIndex, mMapping[i]));
                int nameID = ThsrStationModel.getStationRouteArrayId(i);
                String[] names = res.getStringArray(nameID);
                int srcId = MotcDataHelper.getRouteMapIdArrayId(i);
                String[] srcGDriveIds = res.getStringArray(srcId);
                int dstId = MotcDataHelper.getRouteMapNameArrayId(i);
                String[] dstNames = res.getStringArray(dstId);
                String[] keys = mMapping[i].split(";");
                //Log.d(TAG, " length: " + keys.length);
                for (String key : keys) {
                    //Log.d(TAG, String.format(" %d-%s", i + 1, key));
                    int k = Integer.parseInt(key) - 1;
                    String name = names[k];
                    String src = srcGDriveIds[k];
                    String dst = dstNames[k];
                    //Log.d(TAG, src + " " + dst);
                    routes.add(new TransferRoute(name, src, dst));
                }
                list.add(new TransferStation(mStations[i], routes));
            }
            return list;
        }
        return get_transfer_station();
    }

    public Calendar getUpdateTime() {
        // SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        // Calendar cal = Calendar.getInstance();
        // cal.setTimeInMillis(pref.getLong("update_time", 0));
        return getUpdateTime(mContext);
    }

    public static Calendar getUpdateTime(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(pref.getLong("update_time", 0));
        return cal;
    }

    public void setUpdateTime(long millis) {
        // SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        // Editor editor = pref.edit();
        // editor.putLong("update_time", millis);
        // editor.commit();
        setUpdateTime(mContext, millis);
    }

    public static void setUpdateTime(Context context, long millis) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = pref.edit();
        editor.putLong("update_time", millis);
        editor.commit();
    }
}
