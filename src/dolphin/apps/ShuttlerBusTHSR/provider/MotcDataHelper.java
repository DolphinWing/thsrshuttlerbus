package dolphin.apps.ShuttlerBusTHSR.provider;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dolphin.android.net.HttpHelper;
import dolphin.android.preference.PreferenceUtils;
import dolphin.android.util.FileUtils;
import dolphin.apps.ShuttlerBusTHSR.R;

/**
 * Created by dolphin on 2013/8/2.
 */
public class MotcDataHelper extends HttpHelper {
    public static final String TAG = "THSRDataHelper";
    protected Context mContext;

    private int[] mMotcIndex;
    private String[] mMotcStationName;
    private String[] mMapId;

    private List<MotcDataListener> mListeners;
    private SparseArray<MotcData> mMotcData;
    private SparseBooleanArray mMotcDataDownloading;
    private HashMap<String, String> mGDriveIdMap = null;

    private boolean mUseDriveOnly = false;
    private boolean mDriveReplace = false;
    private boolean mEngineerMode = false;

    public MotcDataHelper(Context context) {
        mContext = context;
        mListeners = new ArrayList<MotcDataListener>();

        mMotcIndex = mContext.getResources().getIntArray(R.array.motc_index);
        mMotcStationName = mContext.getResources().getStringArray(R.array.motc_dgh_stations);
        //for data sync
        mMotcData = new SparseArray<MotcData>();
        mMotcDataDownloading = new SparseBooleanArray();

        mMapId = mContext.getResources().getStringArray(R.array.json_src);
        mEngineerMode = mContext.getResources().getBoolean(R.bool.config_eng_mode);
    }

    /**
     * convert station name to MOTC index (173~178)
     *
     * @param stationName
     * @return
     */
    public int getMotcIndex(String stationName) {
        if (mMotcIndex.length > 0)
            for (int i = 1; i < mMotcIndex.length; i++) {
                if (mMotcStationName[i - 1].equalsIgnoreCase(stationName))
                    return mMotcIndex[i];
            }
        return mMotcIndex[0];
    }

    /**
     * convert station name to array index
     *
     * @param stationName
     * @return
     */
    public int getStationSeq(String stationName) {
        if (mMotcIndex.length > 0 && mMotcStationName.length > 1)
            for (int i = 1; i < mMotcIndex.length; i++) {
                if (mMotcStationName[i - 1].equalsIgnoreCase(stationName))
                    return i - 1;
            }
        return -1;
    }

    /**
     * convert MOTC index to array index
     *
     * @param motcIndex
     * @return
     */
    public int getStationSeq(int motcIndex) {
        if (mMotcIndex.length > 0)
            for (int i = 1; i < mMotcIndex.length; i++) {
                if (mMotcIndex[i] == motcIndex)
                    return i - 1;
            }
        return -1;
    }

    /**
     * download HTML from MOTC website and parse to MotcData object
     *
     * @param motcIndex
     * @return
     */
    public MotcData get_motc_html(int motcIndex) {
        String response = null;
        if (!checkNetworkAvailable(mContext)) {
            Log.e(TAG, "no network to download data from MOTC");
            return null;
        }

        if (mGDriveIdMap == null)
            mGDriveIdMap = getGDriveIdMap();
//        for (int l = 0; l < mListeners.size(); l++)
//            mListeners.get(l).onDownloadStart();
        try {
            //read config from my google drive
            read_drive_config();//[51]++
            MotcData motcData = null;
            if (mDriveReplace) {//replace possibility
                //Log.d(TAG, "mUseDriveOnly = " + mUseDriveOnly);
                motcData = get_drive_cache(motcIndex);
                if (mUseDriveOnly) {
                    Log.w(TAG, "data from google drive " + motcIndex);
                    return motcData;
                }
            }

            response = super.getUrlContent(mContext.getString(R.string.motc_url_base, motcIndex));
            //Log.d(TAG, String.format("response.length() = %d", response.length()));
            String data_html = response;
            String updateTime = "";
            if (data_html.contains("更新時間")) {
                updateTime = data_html;
                updateTime = updateTime.substring(updateTime.indexOf("更新時間"));
                updateTime = updateTime.substring(0, updateTime.indexOf("</div>"));
                updateTime = updateTime.replace("&nbsp;", " ");
                //Log.d(TAG, updateTime);
            }

            //check and compare the cached data from Google Drive of assets
            //directly return the JSON data without do the parsing of html
            //Log.d(TAG, "mDriveReplace = " + mDriveReplace);
            if (motcData != null && motcData.getUpdateTime().equalsIgnoreCase(updateTime)) {
                Log.w(TAG, "data from google drive " + motcIndex);
                return motcData;//no need to parse
            }

            motcData = new MotcData(updateTime);
            if (data_html.contains("<div class=\"part\">")) {
                //Log.d(TAG, "data_html.length() = " + data_html.length());
                data_html = data_html.substring(data_html.indexOf("<div class=\"part"));
                data_html = data_html.substring(0, data_html.indexOf("</form"));
                //Log.d(TAG, "data_html.length() = " + data_html.length());
                data_html = data_html.substring(data_html.indexOf("tabletitle"));
                data_html = doPreWorkaround(motcIndex, data_html);

                ArrayList<String> mRawRowData = new ArrayList<String>();
                int i = 1;//Log.d(TAG, data_html.substring(0, 50));
                while (data_html.contains("<tr>") || data_html.contains("<tr class")) {
                    String row = data_html.substring(data_html.indexOf("<tr"));
                    row = row.substring(0, row.indexOf("</tr>") + 5);
                    //Log.d(TAG, "row " + (i++) + ": " + row.length());
                    //if (mRawRowData.size() > 0 &&
                    //        mRawRowData.get(mRawRowData.size() - 1).equals(row))
                    //    Log.w(TAG, "row duplicated? why?");
                    //else
                    mRawRowData.add(row);
                    data_html = data_html.substring(data_html.indexOf("<tr"));
                    data_html = data_html.substring(data_html.indexOf("</tr>") + 5);
                }
                //Log.d(TAG, String.format("row count = %d", mRawRowData.size()));

                String attr2Pattern = "<td class=[^ ]* rowspan=\"([^\"]+)\"";
                String attr1Pattern = "<td rowspan=\"([^\"]+)\"";
                //String valuePattern = "<td[^>]*>([^<]+)";
                //int busCount = 0;
                //int routeCount = 0;

                MotcData.Route route = null;
                MotcData.Schedule bus = null;
                boolean bNeedAddBus = false;
                int routeRows = 0, scheduleRows = 0;
                for (i = 0; i < mRawRowData.size(); i++) {
                    String rowData = mRawRowData.get(i);
                    Matcher matcher = Pattern.compile(attr2Pattern).matcher(rowData);
                    if (matcher != null && matcher.find()) {
                        //check the row data
                        scheduleRows = Integer.parseInt(matcher.group(1).trim());
                        String name = extract1stTdVale(rowData);
                        //bypass the route name or terminal name
                        rowData = rowData.substring(rowData.indexOf("</td") + 5);

                        if (matcher.find() && matcher.find()) {
                            //Log.d(TAG, "route name = " + name);
                            motcData.addRoute(new MotcData.Route(name));
                            route = motcData.getLastRoute();

                            //routeCount++;
                            //busCount = 0;//new route
                            routeRows = scheduleRows;
                            if (routeRows == 2) {//for Taoyuan/Zuoying
                                scheduleRows = 1;
                                //need make sure the next row can get the bus route
                            } else {
                                scheduleRows = Integer.parseInt(matcher.group(1).trim());
                            }
                            //Log.d(TAG, "routeRows = " + routeRows);

                            //bypass the map url and terminal name
                            rowData = rowData.substring(rowData.indexOf("</td") + 5);
                            String terName = extract1stTdVale(rowData);
                            //terName = terName.replace("&nbsp;", "");
                            terName = formatTerminal(terName);
                            //Log.d(TAG, "> terminal name = " + terName);
                            rowData = rowData.substring(rowData.indexOf("</td") + 5);

                            route.addSchedule(new MotcData.Schedule(terName));
                            bus = route.getLastSchedule();
                            //busCount++;

                            //bypass the last row for schedule row count
                            rowData = rowData.substring(rowData.indexOf("</td") + 5);
                        } else {
                            //change a terminal
                            String terName = name.replace("&nbsp;", "");
                            terName = formatTerminal(terName);
                            //Log.d(TAG, "terminal name = " + name);
                            //route = motcData.getRoute(routeCount - 1);
                            route.addSchedule(new MotcData.Schedule(terName));
                            bus = route.getLastSchedule();
                            //busCount++;
                        }
                    } else {//make sure get another row
                        matcher = Pattern.compile(attr1Pattern).matcher(rowData);
                        if (matcher != null && matcher.find()) {//for Zuoying
                            scheduleRows = Integer.parseInt(matcher.group(1).trim());
                            String terName = extract1stTdVale(rowData);
                            //terName = terName.replace("&nbsp;", "");
                            terName = formatTerminal(terName);
                            //bypass the terminal name
                            rowData = rowData.substring(rowData.indexOf("</td") + 5);

                            //Log.d(TAG, "terminal name = " + terName);
                            //route = motcData.getRoute(routeCount - 1);
                            route.addSchedule(new MotcData.Schedule(terName));
                            bus = route.getLastSchedule();
                        } else {//for one schedule row bus
                            scheduleRows--;
                            String terName = extract1stTdVale(rowData);
                            //terName = terName.replace("&nbsp;", "");
                            terName = formatTerminal(terName);
                            //Log.d(TAG, ">> terminal name = " + terName);
                            if (scheduleRows <= 0 || bNeedAddBus) {
                                route.addSchedule(new MotcData.Schedule(terName));
                                //bypass the terminal name
                                rowData = rowData.substring(rowData.indexOf("</td") + 5);
                            }
                            bus = route.getLastSchedule();
                        }
                    }
                    //Log.d(TAG, " scheduleRows = " + scheduleRows + " " + bNeedAddBus);
                    bNeedAddBus = false;//reset and check if next row should add bus

                    //Log.d(TAG, rowData);
                    while (rowData.contains("<td")) {
                        String t = rowData.substring(rowData.indexOf("<td"));
                        t = t.substring(t.indexOf(">") + 1);
                        t = t.substring(0, t.indexOf("</td>"));
                        t = t.replace("\r\n", "").replaceAll("[ ]+", " ").trim();//2013-09-05
                        //Log.d(TAG, "raw data: " + t);
                        if (t.equalsIgnoreCase("&nbsp;")) {
                            bNeedAddBus = true;//next row should be add a new bus
                            //break;//bypass &nbsp;
                        } else {
                            //for Zuoying
                            t = t.replace("&nbsp;", "");
                            //for Taoyuan/Taichung/Chiayi/Zuoying
                            t = t.contains("span") ? t.substring(t.indexOf("</span") + 7) : t;
                            if (t.contains("<a")) {//for Taoyuan/Taichung/Chiayi
                                t = t.substring(t.indexOf("<a")).substring(t.indexOf(">") + 1);
                                t = t.replace("</a>", "").trim();
                            }
                            //for Taoyuan, bypass those <br /> and put to extra
                            if (t.contains("<br")) {
                                bus.setExtras(t);
                                break;//some extra texts
                            }
                            if (t.contains("font")) {//for Taichung/Chiayi/Zuoying
                                String tmp = t.substring(t.indexOf(">") + 1);
                                tmp = tmp.substring(0, tmp.indexOf("</font"));
                                //t = t.substring(t.indexOf(">") + 1);
                                //t = t.substring(0, t.indexOf("</font"));
                                if (tmp.length() > 5)
                                    bus.setExtras(tmp);
                                else
                                    bus.addTime(t);
                            } else {
                                //remove all other tags
                                t = t.contains("<") ? t.substring(0, t.indexOf("<")) : t;
                                if (!t.contains(":"))
                                    t = String.format("%s:%s", t.substring(0, 2), t.substring(2));
                                if (t.contains("(") && motcIndex == 176) {//for Chiayi
                                    //t = t.contains("(") ? t.substring(0, t.indexOf("(")) : t;
                                    bus.addTime(t);
                                } else if (t.length() > 5) {
                                    if (motcIndex == 175)//[50]++ for Taichung
                                        bus.setExtras(t);
                                    else
                                        bus.setExtras(t.replaceFirst("[:]", ""));
                                    break;//some extra texts
                                } else if (t.trim().length() >= 4)
                                    bus.addTime(t);
                            }
                            //Log.d(TAG, "bus time: " + t);
                        }
                        rowData = rowData.substring(rowData.indexOf("</td>") + 5);
                    }
                }
//                for (int l = 0; l < mListeners.size(); l++)
//                    mListeners.get(l).onDownloadComplete(motcIndex, motcData);

                return doPostWorkaround(motcIndex, motcData);
            }
        } catch (Exception e) {
//            for (int l = 0; l < mListeners.size(); l++)
//                mListeners.get(l).onError();
            e.printStackTrace();
            Log.e(TAG, "e.getMessage: " + e.getMessage());
        }
        return null;
    }

    /**
     * pre process workaround for specific station
     *
     * @param motcIndex
     * @param data_html
     * @return
     */
    public String doPreWorkaround(int motcIndex, String data_html) {
        switch (motcIndex) {
            case 173://Taoyuan
                Log.w(TAG, "pre workaround for Taoyuan");
                data_html = data_html.replaceAll("<td class=\"white\" rowspan=\"14\">" +
                        "[^<]*</td>", "");
                data_html = data_html.replaceAll("<td class=\"gray\" rowspan=\"26\">" +
                        "[^<]*</td>", "");
                break;
            case 175://Taichung
                Log.w(TAG, "pre workaround for Taichung");
                //<td class="white" rowspan="6">
                //<a href="/userfiles/image/entry38/3-12A.doc" target="_blank">3-12A</a><br />
                //    台灣好行-日月潭線</td>
                data_html = data_html.replaceAll("<td class=\"white\" rowspan=\"6\">" +
                        "[^<]*<a href=\"/userfiles/image/entry38/3-12A.doc\"[^<]*</a>" +
                        "<br />[^<]*</td>", "");
                if (data_html.contains("<a id=\"bus82\" name=\"bus82\"></a>")) {
//                    String d1 = data_html.substring(0, data_html.indexOf("<a id=\"bus82\""));
//                    d1 = d1.substring(0, d1.lastIndexOf("colspan=\"10\"")) + ">";
//                    Log.d(TAG, "d1: " + d1.substring(d1.length() - 50));
//                    String d2 = data_html.substring(data_html.indexOf("<a id=\"bus82\""));
//                    d2.replaceFirst("colspan=\"10\"", "");
//                    Log.d(TAG, "d2: " + d2.substring(0, 200));
//                    data_html = d1 + d2;
                    data_html = data_html.replace("<a id=\"bus82\" name=\"bus82\"></a>", "");
                }
                data_html = data_html.replace("<a id=\"bus6936\" name=\"bus6936\"></a>", "");
                break;
        }
        return data_html;
    }

    /**
     * post process workaround for specific station
     *
     * @param motcIndex
     * @param motcData
     * @return
     */
    public MotcData doPostWorkaround(int motcIndex, MotcData motcData) {
        switch (motcIndex) {
            case 174://Hsinchu
                Log.w(TAG, "post workaround for Hsinchu");
                if (motcData.getRoutes().size() >= 3) {//for route 3
                    final String[] extras = {" 平日", " 平日", " 假日", " 假日"};
                    MotcData.Route route = motcData.getRoute(2);
                    if (route.getSchedules().size() >= 4) {
                        for (int i = 0; i < route.getSchedules().size(); i++) {
                            MotcData.Schedule schedule = route.getSchedule(i);
                            schedule.setTerminal(schedule.getTerminal() + extras[i]);
                        }
                    }
                }
                break;
            case 175://Taichung
                //[50]++
                Log.w(TAG, "post workaround for Taichung");
                if (motcData.getRoutes().size() >= 12) {//for route 12
                    MotcData.Route route = motcData.getRoute(11);
                    if (route.getSchedules().size() > 4) {
                        for (int i = 4; i < route.getSchedules().size(); i++) {
                            MotcData.Schedule schedule = route.getSchedule(i);
                            schedule.setTerminal(schedule.getTerminal() + "(台灣好行-日月潭線)");
                        }
                    }
                }
                break;
            case 176://Chiayi
                Log.w(TAG, "post workaround for Chiayi");
                if (motcData.getRoutes().size() >= 4) {//for route 4
                    MotcData.Route route = motcData.getRoute(3);
                    if (route.getSchedules().size() >= 2) {
                        route.getSchedule(1).setTerminal("朴子開");
                    }
                }
                break;
            case 178://Zuoying
                Log.w(TAG, "post workaround for Zuoying");
                if (motcData.getRoutes().size() >= 5) {//for route 5
                    MotcData.Route route = motcData.getRoute(4);
                    if (route.getSchedules().size() > 2) {
                        for (int i = 2; i < route.getSchedules().size(); i++) {
                            MotcData.Schedule schedule = route.getSchedule(i);
                            schedule.setTerminal(schedule.getTerminal() + "(連續假日)");
                        }
                    }
                }
                break;
        }
        return motcData;
    }

    /**
     * get MOTC JSON data from local storage
     *
     * @param motcIndex
     * @return
     */
    public MotcData get_motc_cache(int motcIndex) {
        String fileName = String.format("%d.json", motcIndex);
        File f = new File(mContext.getExternalCacheDir(), fileName);
        if (f.exists()) {
            String json = FileUtils.readFileToString(f);
            //Log.d(TAG, "get_motc_cache string length: " + json.length());
            MotcData motcData = MotcData.jsonToMotcData(json);
            return motcData;
        }
        return null;
    }

    /**
     * store MOTC data to local storage as JSON
     *
     * @param motcIndex
     * @param motcData
     */
    public void set_motc_cache(int motcIndex, MotcData motcData) {
        String fileName = String.format("%d.json", motcIndex);
        File f = new File(mContext.getExternalCacheDir(), fileName);
        String json = MotcData.toJson(motcData);
        //Log.d(TAG, "set_motc_cache string length: " + json.length());
        FileUtils.writeStringToFile(f, json);
    }

    /**
     * clear MOTC data in local storage
     *
     * @param motcIndex
     */
    public void clear_motc_cache(int motcIndex) {
        String fileName = String.format("%d.json", motcIndex);
        File f = new File(mContext.getExternalCacheDir(), fileName);
        if (f.exists()) {
            boolean b = f.delete();
            Log.w(TAG, f.getName() + " delete " + (b ? "success" : "failed"));
        }
        mMotcData.remove(motcIndex);//clear from memory
    }

    /**
     * get MOTC data in asynchronous way
     *
     * @param activity
     * @param motcIndex
     */
    public void asyGetMotcData(final Activity activity, final int motcIndex) {
        //Log.d(TAG, "asyGetMotcData: " + motcIndex);
        for (int l = 0; l < mListeners.size(); l++)
            mListeners.get(l).onDownloadStart(motcIndex);

        MotcData motcData = mMotcData.get(motcIndex);
        if (motcData != null) {//load from memory
            for (int l = 0; l < mListeners.size(); l++)
                mListeners.get(l).onDownloadComplete(motcIndex, motcData);
            return;
        }

        //not in the memory, try to load from cache
        if (isMotcDataDownloading(motcIndex)) {//already downloading, treat as error
            Log.e(TAG, "already downloading: " + motcIndex);
            for (int l = 0; l < mListeners.size(); l++)
                mListeners.get(l).onError();
            return;
        }

        mMotcDataDownloading.put(motcIndex, true);//mark as starting the thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                MotcData data = get_motc_cache(motcIndex);
                if (data == null) {//not in the cache, download from web
                    data = get_motc_html(motcIndex);
                    if (data != null) {//got data, save to cache
                        set_motc_cache(motcIndex, data);
                    }
                }

                final MotcData motcData = data;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (motcData != null) {//make sure the data exists
                            mMotcData.put(motcIndex, motcData);//replace the old one if exist
                            for (int l = 0; l < mListeners.size(); l++)
                                mListeners.get(l).onDownloadComplete(motcIndex, motcData);

                        } else {
                            Log.e(TAG, "no data: " + motcIndex);
                            for (int l = 0; l < mListeners.size(); l++)
                                mListeners.get(l).onError();
                        }

                        mMotcDataDownloading.delete(motcIndex);//mark as complete
                        //Log.d(TAG, "asyGetMotcData complete");
                    }
                });

            }
        }).start();
    }

    /**
     * check if MOTC data is downloading
     *
     * @param motcIndex
     * @return
     */
    public boolean isMotcDataDownloading(int motcIndex) {
        return mMotcDataDownloading.get(motcIndex);
    }

    /**
     * get next column data
     *
     * @param str
     * @return
     */
    private String extract1stTdVale(String str) {
        String value = str.substring(str.indexOf("<td"));
        value = value.substring(value.indexOf(">") + 1);
        return value.substring(0, value.indexOf("</td"));
    }

    public interface MotcDataListener {
        public void onDownloadStart(int motcIndex);

        public void onDownloadComplete(int motcIndex, MotcData data);

        public void onError();
    }

    /**
     * register a listener
     *
     * @param listener
     */
    public void registerDataListener(MotcDataListener listener) {
        mListeners.add(listener);
    }

    /**
     * unregister a listener
     *
     * @param listener
     */
    public void unregisterDataListener(MotcDataListener listener) {
        mListeners.remove(listener);
    }

    //string id
    private final static int[] mStationRouteMapIdArray = new int[]{
            R.array.motc_dgh_station_173_map_src,
            R.array.motc_dgh_station_174_map_src,
            R.array.motc_dgh_station_175_map_src,
            R.array.motc_dgh_station_176_map_src,
            R.array.motc_dgh_station_177_map_src,
            R.array.motc_dgh_station_178_map_src,
    };

    /**
     * get route map id on Google Drive
     *
     * @param index
     * @return
     */
    public static int getRouteMapIdArrayId(int index) {
        return mStationRouteMapIdArray[index];
    }

    private final static int[] mStationRouteMapNameArray = new int[]{
            R.array.motc_dgh_station_173_map_dst,
            R.array.motc_dgh_station_174_map_dst,
            R.array.motc_dgh_station_175_map_dst,
            R.array.motc_dgh_station_176_map_dst,
            R.array.motc_dgh_station_177_map_dst,
            R.array.motc_dgh_station_178_map_dst,
    };

    /**
     * get route map file name
     *
     * @param index
     * @return
     */
    public static int getRouteMapNameArrayId(int index) {
        return mStationRouteMapNameArray[index];
    }

    private final static String KEY_FAVORITE = "favorite_key";

    /**
     * get favorite list
     *
     * @return
     */
    public Map<String, MotcData.Route> getFavoriteRoutes() {
        HashMap<String, MotcData.Route> list = null;
        PreferenceUtils pHelper = new PreferenceUtils(mContext);
        Set<String> routeSet = pHelper.getStringSet(KEY_FAVORITE);
        if (routeSet != null) {
            list = new HashMap<String, MotcData.Route>();
            Iterator<String> iterator = routeSet.iterator();
            while (iterator.hasNext()) {
                String key = iterator.next().toString();
                String json = pHelper.getString(getFavBundleKey(key));
                if (json != null) {
                    list.put(key, MotcData.jsonToRoute(json));
                } else {
                    Log.e(TAG, "? wtf " + key);
                }
            }
        }

        if (list == null)
            return null;

        //How to sort HashMap by key and value in Java
        //http://goo.gl/itGgQN
        List<String> keys = new LinkedList<String>(list.keySet());
        Collections.sort(keys, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                //Log.d(TAG, s1 + " " + s2);
                String[] str1 = s1.split("-");
                String[] str2 = s2.split("-");
                if (str1[0].compareTo(str2[0]) == 0) {
                    return Integer.decode(str1[1]).compareTo(Integer.decode(str2[1]));
                }
                return Integer.decode(str1[0]).compareTo(Integer.decode(str2[0]));
            }
        });

        Map<String, MotcData.Route> sortedMap = new LinkedHashMap<String, MotcData.Route>();
        for (String key : keys) {
            sortedMap.put(key, list.get(key));
        }

        return sortedMap;
    }

    /**
     * if has favorite routes
     *
     * @return
     */
    public boolean hasFavoriteRoutes() {
        Map<String, MotcData.Route> list = getFavoriteRoutes();
        return (list != null && list.size() > 0);
    }

    /**
     * check if the route is in favorite list
     *
     * @param key
     * @return
     */
    public boolean isFavorite(String key) {
        PreferenceUtils pHelper = new PreferenceUtils(mContext);
        Set<String> routeSet = pHelper.getStringSet(KEY_FAVORITE);
        if (routeSet != null) {
            return routeSet.contains(key);
        }
        return false;
    }

    /**
     * remove from favorite list
     *
     * @param key
     */
    public void removeFavorite(String key) {
        PreferenceUtils pHelper = new PreferenceUtils(mContext);
        Set<String> routeSet = pHelper.getStringSet(KEY_FAVORITE);
        if (routeSet != null && isFavorite(key)) {
            //SharedPreferences.Editor editor = pHelper.edit();
            routeSet.remove(key);
            pHelper.putStringSet(KEY_FAVORITE, routeSet);
            pHelper.remove(getFavBundleKey(key));
            //editor.commit();
        }
    }

    /**
     * add to favorite list
     *
     * @param key
     * @param route
     */
    public void addFavorite(String key, MotcData.Route route) {
        PreferenceUtils pHelper = new PreferenceUtils(mContext);
        //SharedPreferences.Editor editor = pHelper.edit();
        Set<String> routeSet = pHelper.getStringSet(KEY_FAVORITE);
        if (routeSet != null) {
            if (routeSet.contains(key)) {
                Log.e(TAG, "already in favorite");
                return;
            }
        } else {
            routeSet = new HashSet<String>();
        }
        routeSet.add(key);

        pHelper.putStringSet(KEY_FAVORITE, routeSet);
        pHelper.putString(getFavBundleKey(key), MotcData.toJson(route));
        //editor.commit();
    }

    /**
     * update the route data in favorite list
     *
     * @param key
     * @param route
     */
    public void updateFavorite(String key, MotcData.Route route) {
        if (!isFavorite(key)) {
            Log.e(TAG, "updateFavorite failed. " + key);
            return;
        }
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(getFavBundleKey(key), MotcData.toJson(route));
        editor.commit();
    }

    private String getFavBundleKey(String key) {
        return String.format("favorite_route_%s", key);
    }

    /**
     * format the title text for better reading
     *
     * @param title
     * @return
     */
    public static String formatTitle(String title) {
        //Log.d(TAG, "formatTitle: " + title);
        String ft = title.trim();
        //if (ft.contains("<br"))
        //    ft = ft.substring(0, ft.lastIndexOf("<"));
        if (ft.contains("nbsp"))
            ft = ft.replace("&nbsp;", "");
        ft = ft.replace("\r\n", "").replace("\n", "");
        ft = ft.replace("\t", "");//[50]++
        ft = ft.replaceAll("[ ]+", " ").trim();//[50]++
        return ft.replaceAll("<br />[ ]*", "").trim();
    }

    /**
     * format the terminal name for better reading
     *
     * @param terminal
     * @return
     */
    public static String formatTerminal(String terminal) {
        //Log.d(TAG, "formatTerminal: " + terminal);
        String ft = terminal.trim();
        if (ft.contains("nbsp"))
            ft = ft.replace("&nbsp;", "");
        ft = ft.replace("\r\n", "").replace("\n", "");
        ft = ft.replace("\t", "");//[50]++
        ft = ft.replaceAll("[ ]+", " ").trim();//[50]++
        return ft.replaceAll("<br />[ ]*", "").trim();
    }

    public static String toJson(ArrayList<MotcData.Map> data) {
        Gson gson = new Gson();
        return gson.toJson(data);
    }

    public static HashMap<String, String> jsonToMap(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<MotcData.Map>>() {
        }.getType();
        ArrayList<MotcData.Map> gmap = gson.fromJson(json, type);
        if (gmap != null) {
            HashMap<String, String> map = new HashMap<String, String>();
            for (MotcData.Map m : gmap)
                map.put(m.getKey(), m.getId());
            return map;
        }
        return null;
    }

    public static String getGoogleDriveUrl(String id) {
        return "https://drive.google.com/uc?export=download&id=" + id;
    }

    /**
     * download file from Google Drive
     *
     * @param context
     * @param id
     * @param dst
     */
    public static boolean downloadFromGoogleDrive(Context context, String id, File dst) {
        if (!HttpHelper.checkNetworkAvailable(context))
            return false;
        //http://stackoverflow.com/a/11855448
        //https://drive.google.com/uc?export=download&id={fileId}
        String url = getGoogleDriveUrl(id);
        boolean r = HttpHelper.getRemoteFile(context, url, dst.getAbsolutePath());
        //Log.v(TAG, "download " + (r ? "success" : "failed"));
        return r;
    }

    private HashMap<String, String> getGDriveIdMap() {
        File f = new File(mContext.getExternalCacheDir(), "dmap.json");
        //if (!f.exists()) {
        try {
            if (MotcDataHelper.downloadFromGoogleDrive(mContext, mMapId[0], f))
                return jsonToMap(FileUtils.readFileToString(f));
        } catch (Exception e) {
            Log.e(TAG, "getGDriveIdMap: " + e.getMessage());
        }
        //}
        return null;
    }

    private MotcData get_drive_cache(int motcIndex) {
        File f = new File(mContext.getExternalCacheDir(),
                String.format("%d.g.json", motcIndex));
        try {
            String driveId = (mGDriveIdMap != null)
                    ? mGDriveIdMap.get(String.format("%d.json", motcIndex)) : null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD)
                driveId = (driveId != null && !driveId.trim().isEmpty())
                        ? driveId : mMapId[getStationSeq(motcIndex)];
            else
                driveId = (driveId != null && driveId.trim().equalsIgnoreCase(""))
                        ? driveId : mMapId[getStationSeq(motcIndex)];
            if (MotcDataHelper.downloadFromGoogleDrive(mContext, driveId, f))
                return MotcData.jsonToMotcData(FileUtils.readFileToString(f));
        } catch (Exception e) {
            Log.e(TAG, "getGDriveIdMap: " + e.getMessage());
        }
        //}
        return null;
    }

    private void read_drive_config() {
        String configId = "0B-oMP4622t0hdjIwX3FhR01xNm8";
        File f = new File(mContext.getExternalCacheDir(), "_config");
        if (MotcDataHelper.downloadFromGoogleDrive(mContext, configId, f)) {
            String content = FileUtils.readFileToString(f);
            //Log.d(TAG, content);
            String[] configs = content.split("\n");
            for (String c : configs) {
                //Log.d(TAG, "c: " + c);
                if (c.startsWith("drive_replace"))//drive_replace=true
                    mDriveReplace = Boolean.parseBoolean(c.split("=")[1]);
                else if (c.startsWith("only_drive"))//only_drive=false
                    mUseDriveOnly = Boolean.parseBoolean(c.split("=")[1]);
            }
        }
    }
}
