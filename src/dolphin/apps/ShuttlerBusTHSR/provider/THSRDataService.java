package dolphin.apps.ShuttlerBusTHSR.provider;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;

public class THSRDataService extends Service implements Runnable
{
	public static final String TAG = "THSRDataService";

	private volatile Looper mServiceLooper;
	private volatile ServiceHandler mServiceHandler;

	private static boolean bIsWorking = false;

	public static boolean isWorking()
	{
		return bIsWorking;
	}

	@Override
	public IBinder onBind(Intent arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();

		// Start up the thread running the service.  Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block.
		Thread thr = new Thread(null, this, TAG);
		thr.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

			@Override
			public void uncaughtException(Thread thread, Throwable ex)
			{
				// TODO Auto-generated method stub
				Log.e(TAG, thread.getName() + " " + ex.getMessage());
			}
		});
		thr.start();
	}

	@Override
	public void onDestroy()
	{
		// Make sure thread has started before telling it to quit.
		while (mServiceLooper == null) {
			synchronized (this) {
				try {
					wait(100);
				}
				catch (InterruptedException e) {
				}
			}
		}
		mServiceLooper.quit();
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		//return super.onStartCommand(intent, flags, startId);
		while (mServiceHandler == null) {
			synchronized (this) {
				try {
					wait(100);
				}
				catch (InterruptedException e) {
				}
			}
		}

		if (intent == null) {
			Log.e(TAG, "Intent is null in onStartCommand: ",
				new NullPointerException());
			return Service.START_NOT_STICKY;
		}

		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		msg.obj = intent.getExtras();
		mServiceHandler.sendMessage(msg);

		// Try again later if we are killed before we can finish scanning.
		return Service.START_REDELIVER_INTENT;
	}

	@Override
	public void run()
	{
		// reduce priority below other background threads to avoid interfering
		// with other services at boot time.
		Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND +
				Process.THREAD_PRIORITY_LESS_FAVORABLE);
		Looper.prepare();

		mServiceLooper = Looper.myLooper();
		mServiceHandler = new ServiceHandler();

		Looper.loop();
	}

	private final class ServiceHandler extends Handler
	{
		@Override
		public void handleMessage(Message msg)
		{
			try {
				//do the work
				download_cache();
			}
			catch (Exception e) {
				Log.e(TAG, "Exception in handleMessage", e);
			}
		}
	}

	private void download_cache()
	{
		bIsWorking = true;
		Log.i(TAG, THSRData.INTENT_DOWNLOAD_START);

		THSRDataHelper helper = new THSRDataHelper(THSRDataService.this);
		helper.get_transfer_html(true);//download from web

		//check download result
		boolean r = (helper.get_transfer_station().size() > 0);
		SharedPreferences settings =
			PreferenceManager.getDefaultSharedPreferences(THSRDataService.this);
		Editor editor = settings.edit();
		editor.putBoolean(THSRDataHelper.KEY_INIT, r);//set as data got
		editor.commit();//commit to storage

		Intent i = new Intent(THSRData.INTENT_DOWNLOAD_COMPLETE);
		i.putExtra(THSRData.EXTRA_RESULT, r);//add extra indicates the result
		sendBroadcast(i);//send broadcast to notify data updates
		Log.i(TAG, THSRData.INTENT_DOWNLOAD_COMPLETE + String.format(" %s", r));
		bIsWorking = false;
	}
}
