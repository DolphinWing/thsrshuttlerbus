package dolphin.apps.ShuttlerBusTHSR.thsr;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import dolphin.android.app.ABSFragment;
import dolphin.android.net.HttpHelper;
import dolphin.android.util.PackageUtils;
import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.motc.ThsrStationModel;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.ReviewItem;
import rnurik.android.wizardpager.ui.ReviewFragment;

/**
 * Created by 97011424 on 2014/1/14.
 */
public class RouteFragment extends ABSFragment
        implements ModelCallbacks, DriveHelper.HashMapCreator {
    private static final String TAG = "RouteFragment";
    private static final String ARG_KEY = "key";

    private String mKey;
    private Page mPage = null;
    private ReviewFragment.Callbacks mCallbacks;
    private AbstractWizardModel mWizardModel;
    private HashMap<String, String> mReviewItems;

    private TextView mTitle;
    private ImageButton mImageMapButton;
    private View mProgress;
    private ArrayList<String> mImageMap;

    private int mAvailableMapWidth = 0;
    private int mAvailableMapHeight = 0;
    private boolean mIsPortrait = false;

    public static RouteFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        RouteFragment fragment = new RouteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static class RouteViewPage extends Page {

        protected RouteViewPage(ModelCallbacks callbacks, String title) {
            super(callbacks, title);
        }

        @Override
        public Fragment createFragment() {
            return RouteFragment.create(getKey());
        }

        @Override
        public void getReviewItems(ArrayList<ReviewItem> dest) {
            //super.getReviewItems(dest);
        }

        @Override
        public boolean isCompleted() {
            return false;//never complete
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Resources res = getActivity().getResources();
        mIsPortrait = res.getBoolean(R.bool.portrait_mode);
        //Log.d(TAG, "mIsPortrait: " + mIsPortrait);

        Bundle args = getArguments();
        if (args != null) {
            mKey = args.containsKey(ARG_KEY) ? args.getString(ARG_KEY) : null;
        }

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int padding = res.getDimensionPixelSize(R.dimen.layout_padding);
        //Log.d(TAG, "layout_padding = " + padding);
        mAvailableMapWidth = (dm.widthPixels - padding * 2);
        mAvailableMapHeight = (int) (dm.heightPixels / 2);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof ReviewFragment.Callbacks)) {
            throw new ClassCastException(
                    "Activity must implement fragment's callbacks");
        }

        mReviewItems = new HashMap<String, String>();
        mCallbacks = (ReviewFragment.Callbacks) activity;

        mWizardModel = mCallbacks.onGetModel();
        mWizardModel.registerListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mWizardModel.unregisterListener(this);
        mCallbacks = null;

        mReviewItems.clear();
        mReviewItems = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_route_page, container, false);
        //((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
        mTitle = (TextView) rootView.findViewById(android.R.id.title);
        mTitle.setText(R.string.action_select_a_route);
        mProgress = rootView.findViewById(android.R.id.progress);

        mImageMapButton = (ImageButton) rootView.findViewById(android.R.id.button3);
        if (mImageMapButton != null)
            mImageMapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File f = new File(view.getTag().toString());
                    if (f != null && f.exists())
                        startActivityForMap(f);
                }
            });
        mImageMapButton.setEnabled(false);

        show_loading(true);
        DriveHelper.getImageMapper(getActivity(), this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DriveHelper.unregisterMapper(this);
    }

    private void show_loading(boolean bShown) {
        getSherlockActivity().setSupportProgressBarIndeterminateVisibility(bShown);
        mProgress.setVisibility(bShown ? View.VISIBLE : View.GONE);
    }

    private void startActivityForMap(File dst) {
        if (!dst.exists()) return;
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(dst), "image/*");
        if (PackageUtils.isCallable(getActivity(), intent))
            startActivity(intent);
        else Log.e(TAG, "no image viewer?");
    }

    private void downloadMapFromGoogleDrive(String id, final File dst) {
        if (!HttpHelper.checkNetworkAvailable(getActivity())) {//[46]++ check network
            Log.e(TAG, "no network to download map file");
            Toast.makeText(getActivity(), R.string.download_failed,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        //http://stackoverflow.com/a/11855448
        //https://drive.google.com/uc?export=download&id={fileId}
        if (mImageMapButton != null)
            mImageMapButton.setEnabled(false);

        final String driveID = id;

        show_loading(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                final boolean r = DriveHelper.download(getActivity(), driveID, dst);
                Log.v(TAG, "download " + (r ? "success" : "failed"));

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (r) {//download success
                            decodeMapFile(dst.getAbsolutePath());
                            if (mImageMapButton != null)
                                mImageMapButton.setVisibility(View.VISIBLE);

                        } else
                            Toast.makeText(getActivity(),
                                    R.string.download_failed,
                                    Toast.LENGTH_SHORT).show();

//                        if (mImageMapButton != null)
//                            mImageMapButton.setEnabled(true);
                        show_loading(false);
                    }
                });

            }
        }).start();
    }

    private void decodeMapFile(String file) {
        //decode as possible
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;
        if (!mIsPortrait) {//width first for tablet
            float width = options.outWidth;
            //Log.d(TAG, "width = " + width);
            while (width > mAvailableMapWidth) {
                width /= 2;
                options.inSampleSize *= 2;
            }
        } else {//[50]++ for phone, check height
            float height = options.outHeight;
            //Log.d(TAG, "width = " + width);
            while (height > mAvailableMapHeight) {
                height /= 2;
                options.inSampleSize *= 2;
            }
        }
        //Log.v(TAG, "options.inSampleSize " + options.inSampleSize);
        Bitmap bmp = BitmapFactory.decodeFile(file, options);
        if (mImageMapButton != null) {
            mImageMapButton.setImageBitmap(bmp);
            mImageMapButton.setTag(file);
            mImageMapButton.setEnabled(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mImageMap == null) {
            show_loading(true);
            DriveHelper.getImageMapper(getActivity(), this);
        }
        onPageTreeChanged();
    }

    @Override
    public void onPageDataChanged(Page page) {
        onPageTreeChanged();
    }

    @Override
    public void onPageTreeChanged() {
        refreshReviewItems();

        //update UI views
        if (mReviewItems.containsKey(ThsrStationModel.KEY_ROUTE)) {
            mTitle.setText(mReviewItems.get(ThsrStationModel.KEY_ROUTE));
        } else {
            mTitle.setText(R.string.action_select_a_route);
            mImageMapButton.setImageResource(android.R.color.transparent);
        }
        if (mReviewItems.containsKey(ThsrStationModel.KEY_ROUTE_POS)) {
            int pos = Integer.parseInt(mReviewItems.get(ThsrStationModel.KEY_ROUTE_POS));
            if (mImageMap == null || pos >= mImageMap.size() || pos < 0) {
                //Log.e(TAG, "onPageTreeChanged: no map " + pos);
                return;
            }
            //Log.d(TAG, mImageMap.get(pos));
            String[] img = mImageMap.get(pos).split(":");
            File image = new File(getActivity().getExternalCacheDir(), img[0]);
            if (image.exists())
                decodeMapFile(image.getAbsolutePath());
            else
                downloadMapFromGoogleDrive(img[1], image);
        }
    }

    private void refreshReviewItems() {
        mReviewItems.clear();
        //collect review items page by page
        ArrayList<ReviewItem> reviewItems = new ArrayList<ReviewItem>();
        for (Page page : mWizardModel.getCurrentPageSequence()) {
            page.getReviewItems(reviewItems);
        }
        //check the review items
        for (ReviewItem item : reviewItems) {
            String str = item.getDisplayValue();
            //Log.d(TAG, "ReviewItem: " + item.getPageKey() + "/" + str);
            if (str != null && !str.equalsIgnoreCase(""))
                mReviewItems.put(item.getPageKey(), str);
        }
    }

    public Page getPage() {
        return mPage;
    }

    @Override
    public void OnListStart() {
        //Log.d(TAG, "OnListStart");
        show_loading(true);
    }

    @Override
    public void OnListComplete(ArrayList<String> map) {
        //Log.d(TAG, "OnListComplete");
        mImageMap = map;
        onPageTreeChanged();
        show_loading(false);
    }

}
