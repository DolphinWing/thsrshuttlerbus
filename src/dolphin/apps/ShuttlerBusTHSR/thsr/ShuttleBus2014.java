package dolphin.apps.ShuttlerBusTHSR.thsr;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.google.analytics.tracking.android.EasyTracker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import dolphin.apps.ShuttlerBusTHSR.R;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.ModelCallbacks;
import rnurik.android.wizardpager.model.Page;
import rnurik.android.wizardpager.model.WizardPagerAdapter;
import rnurik.android.wizardpager.ui.PageFragmentCallbacks;
import rnurik.android.wizardpager.ui.ReviewFragment;
import rnurik.android.wizardpager.ui.StepPagerStrip;

/**
 * Created by 97011424 on 2014/1/14.
 */
public class ShuttleBus2014 extends SherlockFragmentActivity
        implements ModelCallbacks, PageFragmentCallbacks, ReviewFragment.Callbacks,
        DriveHelper.HashMapCreator {
    private final static String TAG = "ShuttleBus";
    private boolean mIsUserMode = true;

    private ViewPager mPager;
    private WizardPagerAdapter mPagerAdapter;
    private StepPagerStrip mStepPagerStrip;
    private AbstractWizardModel mWizardModel = null;

    private List<Page> mCurrentPageSequence;
    //    private boolean mConsumePageSelectedEvent;
    //    private boolean mEditingAfterReview;
    private Button mNextButton;
    private Button mPrevButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setSupportProgressBarIndeterminateVisibility(false);
        mIsUserMode = !getResources().getBoolean(R.bool.config_eng_mode);

        //since API level=9
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // http://goo.gl/cmG1V , solve android.os.NetworkOnMainThreadException
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork() // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
        }

        //if (mWizardModel == null)
        mWizardModel = new ThsrBusModel(this);
        if (savedInstanceState != null)
            mWizardModel.load(savedInstanceState.getBundle("model"));
        mWizardModel.registerListener(this);//unregister in onDestory

        setContentView(R.layout.activity_fragment_wizard_pager);

        //ViewPager
        mPagerAdapter = new WizardPagerAdapter(getSupportFragmentManager(),
                mWizardModel);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);
            }
        });

        //PagerStrip
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.wizardpager_strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                int currPos = mPager.getCurrentItem();
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (currPos != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });
        mStepPagerStrip.setReviewEnabled(false);

        findViewById(R.id.wizardpager_buttonbar).setVisibility(View.GONE);
        mNextButton = (Button) findViewById(R.id.wizardpager_next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
            }
        });
        mPrevButton = (Button) findViewById(R.id.wizardpager_prev_button);
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });
        //mPrevButton.setEnabled(false);

        DriveHelper.getImageMapper(this, this);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                onPageTreeChanged();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
        DriveHelper.unregisterMapper(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        if (mWizardModel != null)
            outState.putBundle("model", mWizardModel.save());
    }

    @Override
    public void onPageDataChanged(Page changedPage) {
        if (changedPage.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                //updateBottomBar();
            }
        }
        if (changedPage.isCompleted() && mNextButton != null)
            mNextButton.performClick();
    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size()
                + (mWizardModel.isReviewEnabled() ? 1 : 0)); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mIsUserMode)//[57]++ ... // The rest of your onStart() code.
            EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mIsUserMode)//[57]++ ... // The rest of your onStop() code.
            EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String pageKey) {

    }

    @Override
    public void onBackPressed() {
        int currPos = mPager.getCurrentItem();
        if (currPos > 0)
            mPrevButton.performClick();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.menu_for_2014, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_item_refresh);
        if (item != null) {
            item.setVisible(!mIsWorking);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_refresh:
                DriveHelper.unregisterMapper(this);
                clear_cache();
                mPager.setCurrentItem(0);//forced to first page
                DriveHelper.getImageMapper(this, this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void clear_cache() {
        //clear internal cache
        File map = new File(getCacheDir(), DriveHelper.IMAGE_MAPPER);
        if (map.exists())
            map.delete();

        //clear external cache
        for (File f : getExternalCacheDir().listFiles()) {
            Log.v(TAG, "rm " + f.getAbsolutePath());
            f.delete();
        }
    }

    boolean mIsWorking = false;

    @Override
    public void OnListStart() {
        //Log.d(TAG, "OnListStart");
        mIsWorking = true;
        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
        setSupportProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void OnListComplete(ArrayList<String> map) {
        //Log.d(TAG, "OnListComplete");
        setSupportProgressBarIndeterminateVisibility(false);
        mIsWorking = false;
        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mIsWorking) return true;
        return super.onTouchEvent(event);
    }
}
