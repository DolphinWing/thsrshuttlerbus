package dolphin.apps.ShuttlerBusTHSR.thsr;

import android.app.Activity;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import dolphin.android.net.GoogleDriveHelper;
import dolphin.android.util.AssetUtils;
import dolphin.android.util.FileUtils;

/**
 * Created by 97011424 on 2014/1/14.
 */
public class DriveHelper extends GoogleDriveHelper {
    private final static String TAG = "DriveHelper";
    public final static String IMAGE_MAPPER = "2014.map";

    public interface HashMapCreator {
        public void OnListStart();

        public void OnListComplete(ArrayList<String> map);
    }

    private static List<HashMapCreator> mListener = new ArrayList<HashMapCreator>();
    private static boolean mIsWorking = false;

    public static void getImageMapper(final Activity activity, HashMapCreator listener) {
        if (listener != null) {//store the listener
            unregisterMapper(listener);//remove old listener
            mListener.add(listener);//register in the callback list
            if (mIsWorking) {//if started, only call self start callback
                Log.w(TAG, "downloading list");
                listener.OnListStart();
                return;
            }
        }
        for (HashMapCreator l : mListener)
            l.OnListStart();//call each start callback

        //check local cache (internal cache)
        final File f = new File(activity.getCacheDir(), IMAGE_MAPPER);
        if (f.exists()) {
            for (HashMapCreator l : mListener)
                l.OnListComplete(parseMap(f));
            return;
        }

        mIsWorking = true;
        Log.v(TAG, "start download the list");
        //download from google drive
        new Thread(new Runnable() {
            @Override
            public void run() {
                String mapId = "0B-oMP4622t0hMzYzTkFidlJXTW8";
                boolean r = download(activity, mapId, f);
                if (!r) {//use default data as backup plan
                    Log.w(TAG, "use default data as backup plan");
                    String asset = AssetUtils.read_asset_text(activity,
                            "_2014.map", "UTF-8");
                    r = FileUtils.writeStringToFile(f, asset);
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.v(TAG, "list downloaded");
                        for (HashMapCreator l : mListener)
                            l.OnListComplete(parseMap(f));
                        mIsWorking = false;//no working
                    }
                });
            }
        }).start();
    }

    public static void unregisterMapper(HashMapCreator listener) {
        if (mListener.contains(listener))
            mListener.remove(listener);
    }

    private static ArrayList<String> parseMap(File src) {
        if (src == null || !src.exists())
            return null;

        ArrayList<String> map = new ArrayList<String>();
        String content = FileUtils.readFileToString(src);
        String[] configs = content.split("\n");
        for (String c : configs) {
            map.add(c);
        }
        return map;
    }
}
