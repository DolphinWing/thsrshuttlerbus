package dolphin.apps.ShuttlerBusTHSR.thsr;

import android.content.Context;
import android.content.res.Resources;

import dolphin.apps.ShuttlerBusTHSR.R;
import dolphin.apps.ShuttlerBusTHSR.motc.ThsrRouteChoiceFragment;
import rnurik.android.wizardpager.model.AbstractWizardModel;
import rnurik.android.wizardpager.model.PageList;
import rnurik.android.wizardpager.model.SingleFixedChoicePage;

/**
 * Created by 97011424 on 2014/1/14.
 */
public class ThsrBusModel extends AbstractWizardModel {

    public ThsrBusModel(Context context) {
        super(context);

        this.setReviewEnabled(false);
    }

    @Override
    protected PageList onNewRootPageList() {
        PageList list = new PageList();

        Resources res = mContext.getResources();
        SingleFixedChoicePage stationPage =
                new ThsrRouteChoiceFragment.MyPage(this,
                        res.getString(R.string.title_stations));
        //stationPage.setRequired(true);
        stationPage.setChoices(res.getStringArray(R.array.thsr_bus_2014));
        list.add(stationPage);

        RouteFragment.RouteViewPage routePage =
                new RouteFragment.RouteViewPage(this, "ROUTE");
        list.add(routePage);

        return list;
    }
}
