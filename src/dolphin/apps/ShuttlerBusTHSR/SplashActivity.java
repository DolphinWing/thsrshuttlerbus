package dolphin.apps.ShuttlerBusTHSR;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import dolphin.android.preference.PreferenceUtils;
import dolphin.apps.ShuttlerBusTHSR.motc.JsonGenerator;
import dolphin.apps.ShuttlerBusTHSR.thsr.ShuttleBus2014;

/**
 * Created by dolphin on 2013/8/1.
 */
public class SplashActivity extends Activity {
    public final static String KEY_TUTORIAL = "_tour";
    private final static String KEY_USE_OLD = "_old";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent();
//        if (useOldVersion(this))//[49]++
//            intent.setClass(this, TransferBusSA.class);
//        else if (isTutored(this))
//            intent.setClass(this, DghMotcActivity.class);
//        else
//            intent.setClass(this, SampleActivity.class);
        if (getResources().getBoolean(R.bool.config_eng_mode))
            intent.setClass(this, JsonGenerator.class);
        else
            intent.setClass(this, ShuttleBus2014.class);
        startActivity(intent);
        this.finish();
    }

    public static boolean isTutored(Context context) {
        return (getTutorStep(context) > 6);
    }

    public static int getTutorStep(Context context) {
        PreferenceUtils pref = new PreferenceUtils(context);
        return pref.getInt(KEY_TUTORIAL, 1);
    }

    public static void setTutorTo(Context context, int step) {
        PreferenceUtils pref = new PreferenceUtils(context);
        pref.putInt(KEY_TUTORIAL, step);
    }

    public static boolean useOldVersion(Context context) {
        PreferenceUtils pref = new PreferenceUtils(context);
        return pref.getBoolean(KEY_USE_OLD);
    }

    public static void setUseOldVersion(Context context, boolean value) {
        PreferenceUtils pref = new PreferenceUtils(context);
        pref.putBoolean(KEY_USE_OLD, value);
    }
}
