package dolphin.apps.ShuttlerBusTHSR;

import android.app.Dialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;

import com.quanta.pobu.app.utils.QProgressDialog;

import dolphin.apps.ShuttlerBusTHSR.provider.THSRData;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRDataHelper;
import dolphin.apps.ShuttlerBusTHSR.provider.THSRDataService;

@SuppressWarnings("deprecation")
public class ShuttlerBus extends TabActivity
{
	private static final String TAG = "ShuttlerBus";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		//prepare layout
		boolean bInit = false;
		SharedPreferences settings =
			PreferenceManager.getDefaultSharedPreferences(this);
		if (settings != null) {
			bInit = settings.getBoolean(THSRDataHelper.KEY_INIT, bInit);
		}
		if (!bInit) { //download the data to cache
			Log.i(THSRDataHelper.TAG, "download the data to cache");
			//download_cache();
			sendBroadcast(new Intent(THSRData.INTENT_DOWNLOAD_START));
		}

		// http://jimmy319.blogspot.com/2011/08/android-tab-view-content_04.html
		// Initialize a TabSpec for each tab and add it to the TabHost
		TabHost.TabSpec specBus = getTabHost().newTabSpec("TransferBus");
		specBus.setIndicator("TransferBus", getResources().getDrawable(
			R.drawable.tran_sbus));
		specBus.setContent(new Intent().setClass(this, TransferBus.class));
		getTabHost().addTab(specBus);

		TabHost.TabSpec specPdf = getTabHost().newTabSpec("TransferPDF");
		specPdf = getTabHost().newTabSpec("TransferPDF");
		specPdf.setIndicator("TransferPDF", getResources().getDrawable(
			android.R.drawable.ic_menu_info_details));
		specPdf.setContent(new Intent().setClass(this, TransferPDF.class));
		getTabHost().addTab(specPdf);

		if (THSRDataService.isWorking()) {//service still working
			showMsgDialog(R.string.processing_html);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// return super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch (item.getItemId()) {
			case R.id.menu_item_cache:
				//download_cache();
				//mHandler.sendEmptyMessage(MSG_DOWNLOAD_CACHE);
				showMsgDialog(R.string.start_download_cache);//show loading dialog
				sendBroadcast(new Intent(THSRData.INTENT_DOWNLOAD_START));
				break;
			case R.id.menu_item_preference:
				//send_message(EVENT_MSG_UPDATE_DESCRIPTION, 0, 0, 10);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	//move to service
	//	private void download_cache()
	//	{
	//		THSRDataHelper helper = new THSRDataHelper(getBaseContext());
	//		helper.get_transfer_html(true);//download from web
	//
	//		//check download result
	//		SharedPreferences settings =
	//			PreferenceManager.getDefaultSharedPreferences(this);
	//		Editor editor = settings.edit();
	//		editor.putBoolean(THSRDataHelper.KEY_INIT, (helper
	//				.get_transfer_station().size() > 0));//get data
	//		editor.commit();//set as already downloaded
	//	}

	private QProgressDialog mDialog = null;

	private void showMsgDialog(int msgRes)
	{
		showMsgDialog(getString(msgRes));
	}

	private void showMsgDialog(String msg)
	{
		if (mDialog != null) {//already has a dialog
			if (msg != null) {//directly show the message
				mDialog.setMessage(msg);
			}
		}
		else {//create a new dialog
			mDialog =
				QProgressDialog.show(ShuttlerBus.this, msg, true,
					new Dialog.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface arg0)
						{
							ShuttlerBus.this.finish();//finish activity
						}
					});
		}
	}

	private void hideMsgDialog()
	{
		if (mDialog != null) {//hide the existing dialog
			mDialog.dismiss();
		}
		mDialog = null;//clear the memory
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		IntentFilter filter =
			new IntentFilter(THSRData.INTENT_DOWNLOAD_COMPLETE);
		registerReceiver(mReceiver, filter);
	}

	@Override
	protected void onPause()
	{
		unregisterReceiver(mReceiver);

		super.onPause();
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			Log.i(TAG, action);
			if (action.equalsIgnoreCase(THSRData.INTENT_DOWNLOAD_COMPLETE)) {
				hideMsgDialog();//hide loading dialog
			}
		}
	};
}
