package com.quanta.pobu.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import dolphin.apps.ShuttlerBusTHSR.R;

public class QProgressDialog extends Dialog // AlertDialog
{
    private final static String TAG = "QProgressDialog";

    private TextView mMessageView = null;
    //    private ImageView mLoadingIcon = null;
    private Context mContext = null;

    public QProgressDialog(Context context)
    {
        // super(context, android.R.style.Theme_Translucent);
        super(context, R.style.Theme_QProgressDialog);

        init(context);
    }

    public QProgressDialog(Context context, int theme)
    {
        super(context, theme);

        init(context);
    }

    // public QProgressDialog(Context context, boolean cancelable,
    // OnCancelListener cancelListener)
    // {
    // super(context, cancelable, cancelListener);
    //
    // init(context);
    // }

    private void init(Context context)
    {
        mContext = context;

        // no title of this dialog
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.dialog_qprogress, null);
        //        mLoadingIcon = (ImageView) view.findViewById(R.id.progress_image);
        mMessageView = (TextView) view.findViewById(R.id.progress_message);
        // setView(view);//copy from ProgressDialog from android source
        setContentView(view);

        // Log.d(TAG, TAG);
        super.onCreate(savedInstanceState);

        // mLoadingIcon.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.spin));
    }

    // @Override
    public void setMessage(CharSequence message)
    {
        if (mMessageView != null) {
            if (message != null) {
                mMessageView.setText(message);
                mMessageView.setVisibility(View.VISIBLE);
            } else {
                mMessageView.setVisibility(View.GONE);
                mMessageView.setText("");// set empty string
            }
        } else {
            Log.w(TAG, "setMessage: " + message);
            // super.setMessage(message);
        }
    }

    public static QProgressDialog show(Context context, CharSequence message)
    {
        QProgressDialog dialog = new QProgressDialog(context);
        // dialog.setCancelable(cancelable);
        // dialog.setOnCancelListener(cancelListener);
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        dialog.setMessage(message);// set after onCreate(), inflate the layout
        return dialog;
    }

    public static QProgressDialog show(Context context, CharSequence message,
            boolean cancelable, OnCancelListener cancelListener)
    {
        QProgressDialog dialog = new QProgressDialog(context);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        dialog.setMessage(message);// set after onCreate(), inflate the layout
        return dialog;
    }
}
